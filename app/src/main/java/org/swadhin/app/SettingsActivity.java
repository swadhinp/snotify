package org.swadhin.app;

import java.util.Calendar;
import java.util.GregorianCalendar;

import org.swadhin.app.utils.Utils;
import org.swadhin.sensors.appsensor.AppSensorService;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.widget.Toast;

public class SettingsActivity extends Activity implements OnSharedPreferenceChangeListener{

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Display the fragment as the main content.
		getFragmentManager().beginTransaction()
		.replace(android.R.id.content, new SettingsFragment())
		.commit();
	}

	public static class SettingsFragment extends PreferenceFragment {
		@Override
		public void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);

			// Load the preferences from an XML resource
			addPreferencesFromResource(R.xml.preferences);

			final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
			final Resources rcs = getResources();

			Preference appTrackPreference = (Preference) findPreference(rcs.getString(R.string.pref_permission_appsensor));
			appTrackPreference.setOnPreferenceClickListener(new OnPreferenceClickListener() {

				@SuppressLint("InlinedApi") @Override
				public boolean onPreferenceClick(Preference arg0) {
					if(Build.VERSION.SDK_INT>Build.VERSION_CODES.KITKAT)
					{
						Intent intent = new Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS);
						startActivity(intent);
					} else {
						Toast.makeText(getActivity(), "Thank you for granting the permissions", Toast.LENGTH_SHORT).show();
					}
					return true;
				}
			});

			Preference notificationTrackPreference = (Preference) findPreference(rcs.getString(R.string.pref_permission_notification));
			notificationTrackPreference.setOnPreferenceClickListener(new OnPreferenceClickListener() {

				@Override
				public boolean onPreferenceClick(Preference arg0) {
					Intent intent = new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS");
					startActivity(intent);
					return true;
				}
			});

			Preference dataSharingPreference = (Preference) findPreference(rcs.getString(R.string.pref_enable_logging));
			dataSharingPreference.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {

				@Override
				public boolean onPreferenceChange(Preference arg0, Object newValue) {
					try{
						if(newValue==null) return false;
						Boolean enabled = (Boolean)newValue;
						if(enabled)
						{
							SharedPreferences.Editor editor = prefs.edit();
							editor.putBoolean(rcs.getString(R.string.pref_enable_logging),true);
							Calendar gc = GregorianCalendar.getInstance();
							editor.putLong(rcs.getString(R.string.pref_sharing_day_start), gc.getTimeInMillis());
							String sharingDays = prefs.getString(rcs.getString(R.string.pref_sharing_days), "14");
							editor.putString(rcs.getString(R.string.pref_sharing_days), sharingDays);
							editor.apply();
							arg0.setSummary("Thank you for sharing your data with us");
						} else {
							arg0.setSummary("Please consider helping research by sharing your data with us");
						}

						//Start app sensor service so that it can start logging
						AppSensorService.startByIntent(getActivity());
						return true;
					} catch(Exception e) {
						Utils.d("Error in onPreferenceChange Settings for Enable Logging");
					}

					return false;
				}
			});

			Preference daysSharingPreference = (Preference) findPreference(rcs.getString(R.string.pref_sharing_days));
			daysSharingPreference.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {

				@Override
				public boolean onPreferenceChange(Preference arg0, Object newValue) {
					if(newValue==null) return false;
					try{
						String sharingDays = (String)newValue.toString();
						SharedPreferences.Editor editor = prefs.edit();
						editor.putBoolean(rcs.getString(R.string.pref_enable_logging),true);
						editor.putString(rcs.getString(R.string.pref_sharing_days), sharingDays);
						editor.apply();
						arg0.setSummary("Your data sharing duration is now set to "+sharingDays+" days");
						//Ensure that app sensor service reacts to the change
						AppSensorService.startByIntent(getActivity());
						return true;
					} catch(Exception e) {
						Utils.d("Error in onPreferenceChange Settings for Sharing Days");
					}

					return false;
				}
			});

			boolean enabledSharing = prefs.getBoolean(rcs.getString(R.string.pref_enable_logging), false);
			if(enabledSharing){
				String summary = rcs.getString(R.string.pref_sharing_days_summary);
				String sharingDays = prefs.getString(rcs.getString(R.string.pref_sharing_days), "14");
				summary += "Your data sharing period is currently set to "+sharingDays+" days";
				daysSharingPreference.setSummary(summary);
			}
		}
	}

	@Override
	public void onSharedPreferenceChanged(SharedPreferences prefs, String key) {
		if(key.equals(getResources().getString(R.string.pref_enable_logging)))
		{
			//Ensure that app sensor service reacts to the change
			AppSensorService.startByIntent(getBaseContext());
		}

	}
}


