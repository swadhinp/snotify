package org.swadhin.app;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.swadhin.sensors.appsensor.AppSensorService;

public class IntroActivity extends Activity{

	private static boolean accessedPermission = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_intro);

		TextView desc = (TextView)findViewById(R.id.desc_view1);
		desc.setText(R.string.intro_desc1);

		TextView desc2 = (TextView)findViewById(R.id.desc_view2);
		desc2.setText(R.string.intro_desc2);

		Button permissionButton = (Button)findViewById(R.id.settings_button);
		permissionButton.setOnClickListener(new View.OnClickListener() {

			@SuppressLint("InlinedApi") @Override
			public void onClick(View arg0) {
				if(Build.VERSION.SDK_INT>Build.VERSION_CODES.KITKAT)
				{
					//Toast.makeText(getApplicationContext(), "Please check the permission to monitor your app usage", Toast.LENGTH_LONG).show();
					Intent intent = new Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS);
					startActivity(intent);
					accessedPermission = true;
				} else {
					Toast.makeText(getApplicationContext(), "Thank you for granting the permissions", Toast.LENGTH_SHORT).show();
				}

			}
		});

		Button nextButton = (Button)findViewById(R.id.next_button);
		nextButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(getApplicationContext(),IntroNotificationActivity.class);
				startActivity(intent);

			}
		});

		final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
		final Resources rcs = getResources();
		SharedPreferences.Editor editor = prefs.edit();
		editor.putBoolean(rcs.getString(R.string.pref_appsensor_active),true);
		editor.apply();
		AppSensorService.startByIntent(getBaseContext());
	}

	@Override
	protected void onResume () {
		super.onResume();
		if(accessedPermission)
		{
			Intent intent = new Intent(getApplicationContext(),IntroNotificationActivity.class);
			startActivity(intent);
			accessedPermission = false;
		}
	}
}
