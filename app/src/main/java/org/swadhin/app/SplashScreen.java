/**
 * 
 */
package org.swadhin.app;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;

/**
 * An activity for the introductory and sign-in screen.
 * 
 * 
 * @author Abhinav Parate
 * 
 */
public class SplashScreen extends Activity implements View.OnClickListener {

	/** Logcat tag */
//	private static final String TAG = "SplashScreen";

	
	
	/** Google + Sign-In Button */
	private Button btnSignIn;


	/** Context instance */
	Context context = null;

	/**
	 * Animation Drawable to show Sarayu Information.
	 */
	//private AnimationDrawable infoAnimation;
	
	/** 
	 * Method to rotate images displaying information about Sarayu.
	 * @see android.app.Activity#onWindowFocusChanged(boolean)
	 */
	@Override
	public void onWindowFocusChanged (boolean hasFocus){
		//infoAnimation.start();
	}
	/** 
	 * Initialization upon activity creation
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);
		
		context = getBaseContext();

		btnSignIn = (Button) findViewById(R.id.launch_button);
		
		/*ImageView rocketImage = (ImageView) findViewById(R.id.imgLogo);
		rocketImage.setBackgroundResource(R.drawable.bg_sarayu);
		infoAnimation = (AnimationDrawable) rocketImage.getBackground();*/
		
		// Button click listeners
		btnSignIn.setOnClickListener(this);
		
	}

	/** 
	 * Initialization upon Activity Start.
	 * @see android.support.v4.app.FragmentActivity#onStart()
	 */
	@Override
	protected void onStart() {
		super.onStart();
		
		//We must check if user already logged in
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		Resources rcs = getResources();
		boolean loggedIn = prefs.getBoolean(rcs.getString(R.string.pref_userloggedin), false);
		//if logged in skip, everything and move to home screen
		if(loggedIn){
			signIn();
			return;
		}		
	}

	/** 
	 * De-init when activity stops
	 * @see android.support.v4.app.FragmentActivity#onStop()
	 */
	@Override
	protected void onStop() {
		super.onStop();

	}
	



	/** 
	 * Listener for sign-in button click
	 * @see android.view.View.OnClickListener#onClick(android.view.View)
	 */
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.launch_button:
			//Store the user information in preferences
			SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
			Resources rcs = getResources();
			SharedPreferences.Editor editor = prefs.edit();
			editor.putBoolean(rcs.getString(R.string.pref_userloggedin), true);
			editor.apply();
			break;
		}
		
		signIn();

	}

	

	
	
	/**
	 * This takes the user to home activity if the user is already
	 * logged in. 
	 */
	private void signIn(){
		
		
		
		Intent i = new Intent(SplashScreen.this, HomeActivity.class);
		
		startActivity(i);

		// close this activity
		finish();
	}

}