package org.swadhin.app;

import java.util.Calendar;
import java.util.GregorianCalendar;

import org.swadhin.app.db.GeneralDAO;
import org.swadhin.app.utils.Utils;
import org.swadhin.sensors.appsensor.db.AppUsageDAO;
import org.swadhin.sensors.appsensor.db.AppUsageObject;
import org.swadhin.sensors.appsensor.db.NotificationDAO;
import org.swadhin.sensors.appsensor.db.NotificationObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.TextView;

public class WeeklyStatsActivity extends Activity{

	public static final String KEY_IS_APP_INFO = "IS_APP_INFO";
	public static final String KEY_PACKAGE_NAME = "PACKAGE_NAME";
	public static final String KEY_APP_LABEL = "APP_LABEL";
	private static final String LINE_CHART = "LineChart";
	private static final String COLUMN_CHART = "ColumnChart";

	private AppUsageObject aus[] = null;
	private NotificationObject nos[] = null;
	private boolean isApps = true;
	private String packageName = null;
	private String appLabel = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_weekly_stats);
		
		Bundle extras = getIntent().getExtras();
		if(extras!=null)
		{
			isApps = extras.getBoolean(KEY_IS_APP_INFO);
			packageName = extras.getString(KEY_PACKAGE_NAME);
			appLabel = extras.getString(KEY_APP_LABEL);
			
			/**************************************************
			 * Push back DB operations to background
			 **************************************************/
			new LoadDataFromDB(this).execute((Void)null);
			
			TextView tv = (TextView) findViewById(R.id.item_count_title);
			tv.setText(appLabel+" Usage Trend (Count)");
			tv = (TextView) findViewById(R.id.item_duration_title);
			tv.setText(appLabel+" Usage Trend (Duration)");
		}
	}


	private void prepareChart(String title, boolean plotCount, int viewId, String chartType)
	{
		WebView chart = (WebView)findViewById(viewId);
		String preData = "<html>"
				+ "  <head>"
				+ "    <script type=\"text/javascript\" src=\"jsapi.js\"></script>"
				+ "    <script type=\"text/javascript\">"
				+ "      google.load(\"visualization\", \"1\", {packages:[\"corechart\"]});"
				+ "      google.setOnLoadCallback(drawChart);"
				+ "      function drawChart() {"
				+ "        var data = google.visualization.arrayToDataTable([";




		String data = "          ['Day', '"+(plotCount?"Count":"Duration")+"'],";
		Calendar gc = GregorianCalendar.getInstance();
		int DAYS_IN_WEEK = 7;
		gc.add(Calendar.DAY_OF_YEAR, -1*DAYS_IN_WEEK);
		if(isApps)
		{
			for(int i=0; aus!=null && i<aus.length;i++)
			{
				gc.add(Calendar.DAY_OF_YEAR,1);
				String dayOfWeek = getDay(gc.get(Calendar.DAY_OF_WEEK));
				data	+= "          ['"+(i!=aus.length-1?dayOfWeek:"TODAY")+"',"+(plotCount?aus[i].usageCount: aus[i].usageTime*1.0/(60000))+"]"+(i!=aus.length-1?",":"");
			}
		} else {
			for(int i=0; nos!=null && i<nos.length;i++)
			{
				gc.add(Calendar.DAY_OF_YEAR,1);
				String dayOfWeek = getDay(gc.get(Calendar.DAY_OF_WEEK));
				data	+= "          ['"+(i!=nos.length-1?dayOfWeek:"TODAY")+"',"+nos[i].count+"]"+(i!=nos.length-1?",":"");
			}
		}

		String postData	=  "        ]);"
				+ "        var options = {"
				+ "          title: '"+title+"',"
				+ "          legend: { position: \"none\" }"
				+ "        };"
				+ "        var chart = new google.visualization."+chartType+"(document.getElementById('chart_div'));"
				+ "        chart.draw(data, options);"
				+ "      }"
				+ "    </script>"
				+ "  </head>"
				+ "  <body>"
				+ "    <div id=\"chart_div\" style=\"width: 0px; height: 0px;\"></div>"
				+ "  </body>" + "</html>";
		String content = preData+data+postData;
		//		chart.setAlwaysDrawnWithCacheEnabled(true);
		//		chart.setDrawingCacheEnabled(true);
		String cachePath = getBaseContext().getCacheDir().getAbsolutePath();
		WebSettings webSettings = chart.getSettings();
		webSettings.setJavaScriptEnabled(true);
		webSettings.setAllowFileAccess(true);
		webSettings.setAppCacheEnabled(true);
		webSettings.setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
		webSettings.setAppCachePath(cachePath);
		chart.requestFocusFromTouch();
		chart.loadDataWithBaseURL( "file:///android_asset/", content, "text/html", "utf-8", null );
	}


	private String getDay(int day)
	{
		switch(day)
		{
		case Calendar.SUNDAY: return "SUNDAY";
		case Calendar.MONDAY: return "MONDAY";
		case Calendar.TUESDAY: return "TUESDAY";
		case Calendar.WEDNESDAY: return "WEDNESDAY";
		case Calendar.THURSDAY: return "THURSDAY";
		case Calendar.FRIDAY: return "FRIDAY";
		case Calendar.SATURDAY: return "SATURDAY";
		default: return "TODAY";
		}
	}

	/**
	 * Background Async task to load app data from db
	 **/
	private class LoadDataFromDB extends AsyncTask<Void, Void, Void> {

		private ProgressDialog dialog = null;
		private Context context;
		public LoadDataFromDB(Context context) {
			this.context = context;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog = new ProgressDialog(context);
			dialog.setMessage("Loading your data...");
			dialog.show();
		}

		protected Void doInBackground(Void... args) {

			if(isApps)
			{
				try{
					synchronized(GeneralDAO.semaphore)
					{
						AppUsageDAO dao = new AppUsageDAO(AppUsageDAO.TABLE_TYPE_DAILY, getBaseContext());
						dao.openRead();
						aus = dao.getAppUsageObjectsForLastWeek(packageName);
						dao.close();
					}
				} catch(Exception e) {
					Utils.d("Exception in reading app stats at WeeklyStats: "+e.getLocalizedMessage());
				}
			} else {
				try{
					synchronized(GeneralDAO.semaphore)
					{
						NotificationDAO dao = new NotificationDAO(getBaseContext());
						dao.openRead();
						nos = dao.getNotificationObjectsForLastWeek(packageName);
						dao.close();
					}
				} catch(Exception e) {
					Utils.d("Exception in reading notification stats at WeeklyStats: "+e.getLocalizedMessage());
				}
			}


			return null;
		}

		protected void onPostExecute(Void arg) {
			if(isApps){
				prepareChart("Usage Count", true, R.id.item_count_view, COLUMN_CHART);
				prepareChart("Usage Duration (in min)", false, R.id.item_duration_view, LINE_CHART);
			} else {
				prepareChart("Usage Count", true, R.id.item_count_view, COLUMN_CHART);
				WebView wv = (WebView)findViewById(R.id.item_duration_view);
				((ViewGroup)(wv.getParent())).removeView(wv);
				TextView tv = (TextView) findViewById(R.id.item_duration_title);
				((ViewGroup)(tv.getParent())).removeView(tv);
			}
			if(dialog!=null)
				dialog.dismiss();

		}
	}
}
