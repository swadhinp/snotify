package org.swadhin.app.fragments;

import android.support.v4.app.Fragment;

import common.view.ColorTabItem;

public class NotificationsTabItem extends ColorTabItem{

	private int tableType = -1;
	private int periodOffset = -1;
	
	public NotificationsTabItem(int tableType, int periodOffset)
	{
		this.tableType = tableType;
		this.periodOffset = periodOffset;
	}
	
	@Override
	public Fragment createFragment() {
		NotificationsFragment fragment = NotificationsFragment.newInstance(tableType, periodOffset);
		return fragment;
	}

}
