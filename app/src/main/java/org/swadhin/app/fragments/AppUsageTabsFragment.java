package org.swadhin.app.fragments;

import common.view.SlidingTabFragment;

public class AppUsageTabsFragment extends SlidingTabFragment{

	public AppUsageTabsFragment() {
		super(new AppUsageFragmentProvider());
	}

}
