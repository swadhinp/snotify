package org.swadhin.app.fragments;

import android.support.v4.app.Fragment;
import common.view.ColorTabItem;

public class AppUsageTabItem extends ColorTabItem{

	private int tableType = -1;
	private int periodOffset = -1;
	
	public AppUsageTabItem(int tableType, int periodOffset)
	{
		this.tableType = tableType;
		this.periodOffset = periodOffset;
	}
	
	@Override
	public Fragment createFragment() {
		AppUsageFragment fragment = AppUsageFragment.newInstance(tableType, periodOffset);
		return fragment;
	}

}
