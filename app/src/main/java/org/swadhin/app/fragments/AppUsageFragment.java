package org.swadhin.app.fragments;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

import org.swadhin.app.R;
import org.swadhin.app.db.GeneralDAO;
import org.swadhin.app.uiitems.AppInfoItem;
import org.swadhin.app.uiitems.AppInfoListAdapter;
import org.swadhin.app.utils.Utils;
import org.swadhin.sensors.appsensor.AppSensorService;
import org.swadhin.sensors.appsensor.db.AppCategoryDAO;
import org.swadhin.sensors.appsensor.db.AppUsageDAO;
import org.swadhin.sensors.appsensor.db.AppUsageObject;
import org.swadhin.sensors.appsensor.db.CategoryUsageObject;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ListView;

public class AppUsageFragment extends Fragment{

	private static final String KEY_TABLE_TYPE = "table_type";
	private static final String KEY_PERIOD_OFFSET = "period_offset";

	public static AppUsageFragment newInstance(int tableType, int periodOffset) {
		Bundle bundle = new Bundle();
		bundle.putInt(KEY_TABLE_TYPE, tableType);
		bundle.putInt(KEY_PERIOD_OFFSET, periodOffset);

		AppUsageFragment fragment = new AppUsageFragment();
		fragment.setArguments(bundle);

		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_app_usage_item, container, false);
	}


	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		Bundle args = getArguments();

		if (args != null) {
			int TABLE_TYPE = args.getInt(KEY_TABLE_TYPE);
			int offset = args.getInt(KEY_PERIOD_OFFSET);
			
			/**************************************************
			 * Push back DB operations to background
			 **************************************************/
			new LoadDataFromDB(getActivity(), TABLE_TYPE, view, offset).execute((Void)null);
		}


	}
	/**
	 * Background Async task to load app data from db
	 **/
	private class LoadDataFromDB extends AsyncTask<Void, Void, Void> {

//		private ProgressDialog dialog = null;
		private Context context;
		private int TABLE_TYPE = 0;
		private View view;
		private int offset = 0;


		/* Private objects needed to populate view onPostExecute()*/
		private ArrayList<AppInfoItem> appInfoItems = null;
		private CategoryUsageObject categories[] = null;

		public LoadDataFromDB(Context context, int TABLE_TYPE, View view, int offset) {
			this.context = context;
			this.TABLE_TYPE = TABLE_TYPE;
			this.view = view;
			this.offset = offset;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
//			dialog = new ProgressDialog(context);
//			dialog.setMessage("Loading your data...");
//			dialog.show();
		}

		protected Void doInBackground(Void... args) {


			/**************************************************************************************
			 * READ DB TO GET APP USAGE INFO
			 **************************************************************************************/
			int PERIOD = 0;
			String tableName = AppUsageDAO.TABLE_NAME_DAILY;

			Calendar gc = GregorianCalendar.getInstance();
			if(TABLE_TYPE==AppUsageDAO.TABLE_TYPE_DAILY){
				gc.add(Calendar.DAY_OF_YEAR, offset);
				PERIOD = gc.get(Calendar.DAY_OF_YEAR);
			} else {
				gc.add(Calendar.DAY_OF_YEAR, offset*7);
				tableName = AppUsageDAO.TABLE_NAME_WEEKLY;
				PERIOD = gc.get(Calendar.WEEK_OF_YEAR);
			}

			AppUsageObject apps[] = null;
			try{
				synchronized (GeneralDAO.semaphore) {

					AppUsageDAO dao = new AppUsageDAO(TABLE_TYPE, getActivity());
					dao.openRead();
					apps = dao.getAllAppUsageObjects(PERIOD);
					dao.close();
				}
			} catch(Exception e) {
				Utils.d("Error in reading app usage DB at AppUsageFragment: "+e.getLocalizedMessage());
			}

			/**************************************************************************************
			 * NOW POPULATE APPINFOITEMS ARRAY TO BE USED IN LISTVIEWADAPTER
			 **************************************************************************************/
			appInfoItems = new ArrayList<AppInfoItem>();
			PackageManager pm = context.getPackageManager();

			long maxDuration = 0;
			for(int i=0;apps!=null && i<apps.length;i++)
			{
				try{
					ApplicationInfo aInfo = pm.getApplicationInfo(apps[i].app,PackageManager.GET_META_DATA);
					Drawable icon = pm.getApplicationIcon(apps[i].app);
					String label = pm.getApplicationLabel(aInfo).toString();
					if(label!=null)// && !label.equals("Google Search") && !label.equals("Android System"))
					{
						if(apps[i].app.equals(AppSensorService.launcherPackage)) label = "Home";
						if(maxDuration==0) maxDuration = apps[i].usageTime;
						double percDuration = apps[i].usageTime*1.0/maxDuration;
						appInfoItems.add(new AppInfoItem(label, apps[i].app, apps[i].usageTime, percDuration, apps[i].usageCount, icon));
					}
				}catch(Exception e)
				{
					//e.printStackTrace();
				}
			}
			
			/**************************************************************************
			 * READ DB FOR CATEGORY USAGE
			 **************************************************************************/

			try{
				synchronized (GeneralDAO.semaphore) {

					AppCategoryDAO catDao = new AppCategoryDAO(context);
					catDao.openRead();
					categories = catDao.getCategoryUsage(tableName, PERIOD);
					catDao.close();

				}
			} catch(Exception e) {
				Utils.d("Error in reading category usage DB at AppUsageFragment: "+e.getLocalizedMessage());
			}

			return null;
		}

		protected void onPostExecute(Void arg) {

			//App Usage DB has been read, now populate view to show app usage
			ListView mAppInfoList = (ListView) view.findViewById(R.id.item_app_info_list);

			// setting the nav drawer list adapter
			if(appInfoItems!=null)
			{
				AppInfoListAdapter adapter = new AppInfoListAdapter(context, appInfoItems, true);
				mAppInfoList.setAdapter(adapter);
			}

			//Category Usage DB has been read, now populate webview to show category usage
			if(categories!=null)
				createWebView();

//			if(dialog!=null)
//				dialog.dismiss();

		}

		private void createWebView()
		{
			WebView chart = (WebView)view.findViewById(R.id.item_chart_view);
			String preData = "<html>"
					+ "  <head>"
					+ "    <script type=\"text/javascript\" src=\"jsapi.js\"></script>"
					+ "    <script type=\"text/javascript\">"
					+ "      google.load(\"visualization\", \"1\", {packages:[\"corechart\"]});"
					+ "      google.setOnLoadCallback(drawChart);"
					+ "      function drawChart() {"
					+ "        var data = google.visualization.arrayToDataTable([";

			int totalTime = 0;
			String data = "          ['Category', 'Minutes'],";
			for(int i=0;i<categories.length;i++)
			{
				data	+= "          ['"+categories[i].category+"',"+categories[i].usageTime/(60*1000)+"]"+(i!=categories.length-1?",":"");
				totalTime += categories[i].usageTime/(60*1000);
			}
			String postData	=  "        ]);"
					+ "        var options = {"
					+ "          title: 'App Usage by Category (in min) Total: "+totalTime+" min'"
					+ "        };"
					+ "        var chart = new google.visualization.PieChart(document.getElementById('chart_div'));"
					+ "        chart.draw(data, options);"
					+ "      }"
					+ "    </script>"
					+ "  </head>"
					+ "  <body>"
					+ "    <div id=\"chart_div\" style=\"width: 0px; height: 0px;\"></div>"
					+ "  </body>" + "</html>";
			String content = preData+data+postData;
			String cachePath = context.getCacheDir().getAbsolutePath();
			WebSettings webSettings = chart.getSettings();
			webSettings.setJavaScriptEnabled(true);
			webSettings.setAllowFileAccess(true);
			webSettings.setAppCacheEnabled(true);
			webSettings.setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
			webSettings.setAppCachePath(cachePath);
			chart.requestFocusFromTouch();
			chart.loadDataWithBaseURL( "file:///android_asset/", content, "text/html", "utf-8", null );
		}

	}
}
