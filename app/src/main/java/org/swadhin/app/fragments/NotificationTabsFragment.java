package org.swadhin.app.fragments;

import common.view.SlidingTabFragment;

public class NotificationTabsFragment extends SlidingTabFragment{

	public NotificationTabsFragment() {
		super(new NotificationFragmentProvider());
	}
}
