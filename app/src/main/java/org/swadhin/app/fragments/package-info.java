/**
 * Provides core UI fragments used in the app. Each sensor supported in
 * the app has its own UI fragment.
 *
 *@author Abhinav Parate
 *
 */
package org.swadhin.app.fragments;