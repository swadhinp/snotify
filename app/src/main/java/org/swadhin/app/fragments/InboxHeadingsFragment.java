package org.swadhin.app.fragments;

import android.app.Activity;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.service.notification.StatusBarNotification;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import org.swadhin.app.R;
import org.swadhin.app.uiitems.InboxItem;
import org.swadhin.app.uiitems.InboxListAdapter;
import org.swadhin.app.uiitems.InboxListAdapter.OnHeadingSelectedListener;
import org.swadhin.sensors.appsensor.NotificationObserver;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

public class InboxHeadingsFragment extends Fragment implements OnItemClickListener {

	private PackageManager pm = null;
	public static ArrayList<InboxItem> inboxItems;
	private InboxHeadingsFragment fm = null;
	private InboxListAdapter adapter = null;
	
	 // The listener we are to notify when a headline is selected
    OnHeadingSelectedListener mHeadingSelectedListener = null;
	
    
    /**
     * Sets the listener that should be notified of headline selection events.
     * @param listener the listener to notify.
     */
    public void setOnHeadlineSelectedListener(OnHeadingSelectedListener listener) {
        mHeadingSelectedListener = listener;
    }

	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		pm = getActivity().getPackageManager();
		return inflater.inflate(R.layout.fragment_inbox_apps, container, false);
	}

	
	@Override
	public void onResume() {
		super.onResume();
		if(adapter!=null)
		{
			adapter.notifyDataSetChanged();
		}
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		if(pm==null)
			pm = view.getContext().getPackageManager();
		fm = this;
		/**************************************************
		 * Push back DB operations to background
		 **************************************************/
		new LoadDataFromDB(view).execute((Void)null);
		
	}

		
	/**
	 * Background Async task to load app category data from db
	 **/
	private class LoadDataFromDB extends AsyncTask<Void, Void, Void> {

		private View view;
		//		private ProgressDialog dialog = null;
		
		public LoadDataFromDB(View view) {
			this.view = view;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			//			dialog = new ProgressDialog(getActivity());
			//			dialog.setMessage("Loading your data...");
			//			dialog.show();
		}

		protected Void doInBackground(Void... args) {

			//Populating the Home screen of Snotify for new notifications
			
			HashMap<String,ArrayList<StatusBarNotification>> nmap = NotificationObserver.nmaps;
			Set<String> keys = nmap.keySet();
			Iterator<String> it = keys.iterator();
			
			PackageManager pm;
            pm = view.getContext().getPackageManager();
            inboxItems = new ArrayList<InboxItem>();
			while(it.hasNext())
			{
				try{
					String key = it.next();
					ArrayList<StatusBarNotification> list = nmap.get(key);
					if(list==null || list.size()==0)
						continue;
					String packageName = key;
					ApplicationInfo aInfo = pm.getApplicationInfo(packageName,PackageManager.GET_META_DATA);
					Drawable icon = pm.getApplicationIcon(packageName);
					String label = pm.getApplicationLabel(aInfo).toString();
					inboxItems.add(new InboxItem(label, icon, packageName, list));
					
				 } catch(Exception e){}
			}
			
			return null;
		}

		protected void onPostExecute(Void arg) {
            //			if(dialog!=null)
            //				dialog.dismiss();
            Log.d("swadhin", "On Post Execute AsyncTask");
            Activity act = getActivity();

            if (null != act) {
                act.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ListView mAppInfoList = (ListView) view.findViewById(R.id.inbox_headings_list);
                        // setting the nav drawer list adapter
                        adapter = new InboxListAdapter(getActivity(), inboxItems);
                        mAppInfoList.setAdapter(adapter);
                        adapter.setOnHeadlineSelectedListener(mHeadingSelectedListener);
                        mAppInfoList.setOnItemClickListener(fm);
                    }
                });
            }
        }
	}


	 @Override
	    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
	        if (null != mHeadingSelectedListener && inboxItems!=null) {
	            mHeadingSelectedListener.onHeadingSelected(position);
	        }
	    }

}
