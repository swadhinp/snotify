package org.swadhin.app.fragments;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.PendingIntent;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.service.notification.StatusBarNotification;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.RemoteViews;
import android.widget.TextView;

import org.swadhin.app.R;
import org.swadhin.app.uiitems.InboxItem;
import org.swadhin.app.uiitems.InboxListAdapter;
import org.swadhin.app.uiitems.NotificationListAdapter;
import org.swadhin.sensors.appsensor.AppSensorService;
import org.swadhin.sensors.appsensor.AppUsageEvent;
import org.swadhin.sensors.appsensor.HardwareObserver;
import org.swadhin.sensors.appsensor.LocationObserver;
import org.swadhin.sensors.appsensor.NotificationObserver;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class InboxDetailFragment extends Fragment{


	private View mView = null;
	private int index = -1;
	public void showItem(int index)
	{
		this.index = index;
		Log.d("swadhin","Inside show item "+mView);
		if(mView!=null)
		{
			populateView();
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		Log.d("swadhin","Inside create view");
		mView = inflater.inflate(R.layout.inbox_detail, container, false);
		return mView;
	}


	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		Log.d("swadhin","Inside view created");
		mView = view;
		populateView();

	}

	private void populateView()
	{
		ImageView appIcon = (ImageView) mView.findViewById(R.id.inbox_app_icon);
		TextView appTitle = (TextView) mView.findViewById(R.id.inbox_app_title);
		TextView numNotifications = (TextView) mView.findViewById(R.id.inbox_msg_cnt);

		if(index==-1) return;
		final InboxItem item = InboxListAdapter.inboxItems.get(index);
		appIcon.setImageDrawable(item.getIcon());
		appTitle.setText(item.getTitle());

		final ArrayList<StatusBarNotification> nList = item.getNotifications();
		numNotifications.setText(nList.size()+"");

		final ListView listView = (ListView)mView.findViewById(R.id.inbox_msg_list);

		final NotificationListAdapter adapter = new NotificationListAdapter(getActivity(),
				item.getNotifications(), numNotifications);
		listView.setAdapter(adapter);

		ImageButton clearAllButton = (ImageButton) mView.findViewById(R.id.inbox_clear_all_button);
        final RadioGroup radgrp = (RadioGroup)mView.findViewById(R.id.notif_radio_grp);

		clearAllButton.setOnClickListener(new View.OnClickListener() {

			@TargetApi(Build.VERSION_CODES.LOLLIPOP)
			@Override
			public void onClick(View v) {
				NotificationObserver observer = NotificationObserver.getInstance();
				ArrayList<StatusBarNotification> list = item.getNotifications();
				for(StatusBarNotification sn: list)
				{
					Notification n = sn.getNotification();
                    Log.d("swadhin", "Start Fragment Getting Notif Info");

                    AppUsageEvent aue = new AppUsageEvent();
                    aue.taskID = sn.getId();
                    aue.packageName = sn.getPackageName();
                    aue.eventtype = "event_feedback";
                    aue.starttime = sn.getPostTime(); //Time when it was posted
                    aue.runtime = 0; // since we don't know when it ends
                    aue.longitude = LocationObserver.longitude;
                    aue.latitude = LocationObserver.latitude;
                    aue.accuracy = LocationObserver.accuracy;
                    aue.powerstate = HardwareObserver.powerstate;
                    aue.wifistate = HardwareObserver.wifistate;
                    aue.bluetoothstate = HardwareObserver.bluetoothstate;
                    aue.headphones = HardwareObserver.headphones;
                    aue.orientation = HardwareObserver.orientation;
                    aue.timestampOfLastScreenOn = HardwareObserver.timestampOfLastScreenOn;

                    Notification not = sn.getNotification();

                    List<String> str_list = getTextString(not);
                    int not_word_count = 0;

                    for (String s : str_list)
                    {
                        not_word_count += 1;
                    }

                    aue.notif_style= extractStyleInfo(not.extras.getString(Notification.EXTRA_TEMPLATE));
                    aue.notif_word_count = not_word_count;

                    aue.notif_tag = sn.getTag();
                    if(null != not.tickerText) {
                        aue.notif_tickerText = not.tickerText.toString();
                    }else{
                        aue.notif_tickerText = "None";
                    }
                    aue.notif_id = Integer.toString(sn.getId());
                    aue.notif_ledRGB = Integer.toString(not.ledARGB);
                    aue.notif_ledOff = Integer.toString(not.ledOffMS);
                    aue.notif_ledOn = Integer.toString(not.ledOnMS);
                    if( null != not.sound) {
                        aue.notif_sound = not.sound.toString();
                    }else{
                        aue.notif_sound = "None";
                    }

                    if( null != not.vibrate){
                        for (long l : not.vibrate) {
                            aue.notif_vibrate = Long.toString(l) + ":";
                        }
                    }else{
                        aue.notif_vibrate = "None";
                    }

                    aue.notif_priority = Integer.toString(not.priority);
                    aue.notif_icon = Integer.toString(not.icon);
                    aue.notif_clearable = Boolean.toString(sn.isClearable());
                    aue.notif_ongoing = Boolean.toString(sn.isOngoing());

                    aue.feedback = "None";

                    AppSensorService.logEvent(aue);

                    Log.d("swadhin", "Start Fragment Getting Notif Info");

					PendingIntent intent = n.deleteIntent;
					if(intent!=null)
					{
						try {
							intent.send();
						} catch (PendingIntent.CanceledException e) {
							
						}
					}

					//if(observer!=null)
					//	observer.cancelNotification(sn.getKey());
				}



                ArrayList<StatusBarNotification> thToRemoved = new ArrayList<StatusBarNotification>();

                for(Iterator<StatusBarNotification> it = list.iterator(); it.hasNext();) {
                    StatusBarNotification sn = it.next();

                    String app = sn.getPackageName();
                    int id = sn.getId();

                    ArrayList<StatusBarNotification> listn = NotificationObserver.nmaps.get(app);

                    if(listn==null)
                    {
                        listn = new ArrayList<StatusBarNotification>();
                        NotificationObserver.nmaps.put(app, listn);
                    }

                    for(Iterator<StatusBarNotification> itn = listn.iterator(); itn.hasNext();)
                    {
                        StatusBarNotification snn = itn.next();

                        if(snn.getId()==id)
                        {
                            //itn.remove();
                            //thToRemoved.add(snn);
                            break;
                        }
                    }

                    //removeFromMap(it.next()); // Swadhin : removing notification from Actual Map
                }

                list.clear();
				populateView();
			}
		});
	}

    private static void removeFromMap(StatusBarNotification notification)
    {
        String app = notification.getPackageName();
        int id = notification.getId();

        ArrayList<StatusBarNotification> list = getNotificationList(app);

        for(Iterator<StatusBarNotification> it = list.iterator(); it.hasNext();)
        {
            StatusBarNotification sn = it.next();

            if(sn.getId()==id)
            {
                it.remove();
                break;
            }
        }
    }

    private static ArrayList<StatusBarNotification> getNotificationList(String app)
    {
        ArrayList<StatusBarNotification> list = NotificationObserver.nmaps.get(app);
        if(list==null)
        {
            list = new ArrayList<StatusBarNotification>();
            NotificationObserver.nmaps.put(app, list);
        }
        return list;
    }


    public static List<String> getTextString(Notification notification)
    {
        // We have to extract the information from the view
        RemoteViews views = (RemoteViews)notification.bigContentView;
        if (views == null) views = (RemoteViews)notification.contentView;
        if (views == null) views = (RemoteViews)notification.tickerView;
        if (views == null) return null;

        // Use reflection to examine the m_actions member of the given RemoteViews object.
        // It's not pretty, but it works.
        List<String> text = new ArrayList<String>();
        try
        {
            Field field = null;

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                //Log.e("Obs", views.getClass().getSuperclass().getSimpleName());
                field = views.getClass().getSuperclass().getDeclaredField("mActions");
            }else{
                //Log.e("Obs", views.getClass().getSimpleName());
                field = views.getClass().getDeclaredField("mActions");
            }

            field.setAccessible(true);

            @SuppressWarnings("unchecked")
            ArrayList<Parcelable> actions = (ArrayList<Parcelable>) field.get(views);

            // Find the setText() and setTime() reflection actions
            for (Parcelable p : actions)
            {
                Parcel parcel = Parcel.obtain();

                if (parcel == null) continue;
                if (p == null) continue;

                p.writeToParcel(parcel, 0);
                parcel.setDataPosition(0);

                // The tag tells which type of action it is (2 is ReflectionAction, from the source)
                int tag = parcel.readInt();
                if (tag != 2) continue;
                // View ID
                parcel.readInt();

                String methodName = parcel.readString();
                if (methodName != null)
                    Log.e("Obs", methodName);

                if (methodName == null) continue;
                    // Save strings
                else if (methodName.equals("setText"))
                {
                    // Parameter type (10 = Character Sequence)
                    parcel.readInt();

                    // Store the actual string
                    if( null != parcel) {
                        CharSequence cs = TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
                        if (null != cs) {

                            String t = cs.toString();

                            if (null != t) {
                                text.add(t.trim());
                            }
                        }
                    }
                }

                // Save times. Comment this section out if the notification time isn't important
                else if (methodName.equals("setTime"))
                {
                    // Parameter type (5 = Long)
                    parcel.readInt();

                    if( null != parcel) {
                        String t = new SimpleDateFormat("h:mm a").format(new Date(parcel.readLong()));
                        text.add(t);
                    }
                }

                parcel.recycle();

            }
        }
        // It's not usually good style to do this, but then again, neither is the use of reflection...
        catch (Exception e)
        {
            Log.e("NotificationObserver", e.toString());
        }

        return text;
    }

    private String extractStyleInfo(String styleString)
    {
        String style;

        if(styleString!=null)
        {
            if(styleString.equals("android.app.Notification$BigTextStyle"))
            {
                style = "BigText";
            } else if(styleString.equals("android.app.Notification$BigPictureStyle"))
            {
                style = "BigPic";
            } else if(styleString.equals("android.app.Notification$InboxStyle"))
            {
                style = "Inbox";
            } else if(styleString.equals("android.app.Notification$MediaStyle"))
            {
                style = "Media";
            } else
            {
                style = "Simple";
            }
        } else {
            style = "None";
        }

        return style;
    }

}
