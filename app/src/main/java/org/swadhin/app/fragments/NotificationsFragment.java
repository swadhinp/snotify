package org.swadhin.app.fragments;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.LinkedList;

import org.swadhin.app.R;
import org.swadhin.app.db.GeneralDAO;
import org.swadhin.app.uiitems.AppInfoItem;
import org.swadhin.app.uiitems.AppInfoListAdapter;
import org.swadhin.app.utils.Utils;
import org.swadhin.sensors.appsensor.db.AppUsageDAO;
import org.swadhin.sensors.appsensor.db.NotificationDAO;
import org.swadhin.sensors.appsensor.db.NotificationObject;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ListView;

public class NotificationsFragment extends Fragment{

	private static final String KEY_TABLE_TYPE = "table_type";
	private static final String KEY_PERIOD_OFFSET = "period_offset";

	public static NotificationsFragment newInstance(int tableType, int periodOffset) {
		Bundle bundle = new Bundle();
		bundle.putInt(KEY_TABLE_TYPE, tableType);
		bundle.putInt(KEY_PERIOD_OFFSET, periodOffset);

		NotificationsFragment fragment = new NotificationsFragment();
		fragment.setArguments(bundle);

		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_notification_item, container, false);
	}


	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		Bundle args = getArguments();

		if (args != null) {
			int TABLE_TYPE = args.getInt(KEY_TABLE_TYPE);
			int offset = args.getInt(KEY_PERIOD_OFFSET);
			
			/**************************************************
			 * Push back DB operations to background
			 **************************************************/
			new LoadDataFromDB(getActivity(), TABLE_TYPE, view, offset).execute((Void)null);
		}

	}

	
	/**
	 * Background Async task to load app data from db
	 **/
	private class LoadDataFromDB extends AsyncTask<Void, Void, Void> {

//		private ProgressDialog dialog = null;
		private Context context;
		private int TABLE_TYPE = 0;
		private View view;
		private int offset = 0;


		/* Private objects needed to populate view onPostExecute()*/
		private ArrayList<AppInfoItem> appInfoItems = null;
		private LinkedList<String> appList = null;
		private LinkedList<Integer> countList = null;

		public LoadDataFromDB(Context context, int TABLE_TYPE, View view, int offset) {
			this.context = context;
			this.TABLE_TYPE = TABLE_TYPE;
			this.view = view;
			this.offset = offset;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
//			dialog = new ProgressDialog(context);
//			dialog.setMessage("Loading your data...");
//			dialog.show();
		}

		protected Void doInBackground(Void... args) {


			/**************************************************************************************
			 * READ DB TO GET APP USAGE INFO
			 **************************************************************************************/
			int PERIOD = 0;
			Calendar gc = GregorianCalendar.getInstance();
			if(TABLE_TYPE==AppUsageDAO.TABLE_TYPE_DAILY){
				gc.add(Calendar.DAY_OF_YEAR, offset);
				PERIOD = gc.get(Calendar.DAY_OF_YEAR);
			} else {
				gc.add(Calendar.DAY_OF_YEAR, offset*7);
				PERIOD = gc.get(Calendar.WEEK_OF_YEAR);
			}
			
			NotificationObject notifs[] = null;
			try{
				synchronized (GeneralDAO.semaphore) {

					NotificationDAO dao = new NotificationDAO(getActivity());
					dao.openRead();
					if(TABLE_TYPE ==  AppUsageDAO.TABLE_TYPE_DAILY)
						notifs = dao.getDayNotifications(PERIOD);
					else 
						notifs = dao.getWeekNotifications(PERIOD);
					dao.close();
				}
			} catch(Exception e) {
				Utils.d("Error in reading notifications DB at NotificationsFragment: "+e.getLocalizedMessage());
			}

			/**************************************************************************************
			 * NOW POPULATE APPINFOITEMS ARRAY TO BE USED IN LISTVIEWADAPTER
			 **************************************************************************************/
			appInfoItems = new ArrayList<AppInfoItem>();
			PackageManager pm = context.getPackageManager();

			long maxDuration = 0;
			appList = new LinkedList<String>();
			countList = new LinkedList<Integer>();
			for(int i=0;notifs!=null && i<notifs.length;i++)
			{
				try{
					ApplicationInfo aInfo = pm.getApplicationInfo(notifs[i].app,PackageManager.GET_META_DATA);
					Drawable icon = pm.getApplicationIcon(notifs[i].app);
					String label = pm.getApplicationLabel(aInfo).toString();
					if(maxDuration==0) maxDuration = notifs[i].count;//apps[i].usageTime;
					double percDuration = notifs[i].count*1.0/maxDuration;
					appInfoItems.add(new AppInfoItem(label, notifs[i].app, notifs[i].count, percDuration, notifs[i].count, icon));
					appList.add(label);
					countList.add(notifs[i].count);
				}catch(Exception e)
				{
					//e.printStackTrace();
				}
			}
			
			return null;
		}

		protected void onPostExecute(Void arg) {

			//Notification DB has been read, now populate view to show notifications
			ListView mAppInfoList = (ListView) view.findViewById(R.id.item_app_info_list);

			// setting the nav drawer list adapter
			if(appInfoItems!=null)
			{
				AppInfoListAdapter adapter = new AppInfoListAdapter(context, appInfoItems, false);
				mAppInfoList.setAdapter(adapter);
			}

			//Category Usage DB has been read, now populate webview to show category usage
			if(appList!=null)
				createWebView();

//			if(dialog!=null)
//				dialog.dismiss();

		}

		private void createWebView()
		{
			WebView chart = (WebView)view.findViewById(R.id.item_chart_view);
			String preData = "<html>"
					+ "  <head>"
					+ "    <script type=\"text/javascript\" src=\"jsapi.js\"></script>"
					+ "    <script type=\"text/javascript\">"
					+ "      google.load(\"visualization\", \"1\", {packages:[\"corechart\"]});"
					+ "      google.setOnLoadCallback(drawChart);"
					+ "      function drawChart() {"
					+ "        var data = google.visualization.arrayToDataTable([";

			


			String data = "          ['App', 'Notification Count'],";
			int size = appList.size();
			int totalCount = 0;
			for(int i=0;i<size;i++)
			{
				data	+= "          ['"+appList.get(i)+"',"+countList.get(i)+"]"+(i!=size-1?",":"");
				totalCount += countList.get(i);
			}
			if(size == 0)
				data	+= "          ['None',0 ]";
			String postData	=  "        ]);"
					+ "        var options = {"
					+ "          title: 'Notification Counts by App. Total: "+totalCount+"',"
					+ "          legend: { position: \"none\" }"
					+ "        };"
					+ "        var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));"
					+ "        chart.draw(data, options);"
					+ "      }"
					+ "    </script>"
					+ "  </head>"
					+ "  <body>"
					+ "    <div id=\"chart_div\" style=\"width: 0px; height: 0px;\"></div>"
					+ "  </body>" + "</html>";
			String content = preData+data+postData;
			//chart.setAlwaysDrawnWithCacheEnabled(true);
			//chart.setDrawingCacheEnabled(true);
			String cachePath = context.getCacheDir().getAbsolutePath();
			WebSettings webSettings = chart.getSettings();
			webSettings.setJavaScriptEnabled(true);
			webSettings.setAllowFileAccess(true);
			webSettings.setAppCacheEnabled(true);
			webSettings.setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
			webSettings.setAppCachePath(cachePath);
			chart.requestFocusFromTouch();
			chart.loadDataWithBaseURL( "file:///android_asset/", content, "text/html", "utf-8", null );
		}

	}

}
