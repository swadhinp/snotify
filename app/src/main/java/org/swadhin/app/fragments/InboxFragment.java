package org.swadhin.app.fragments;

//import android.app.FragmentManager;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.swadhin.app.R;
import org.swadhin.app.uiitems.InboxListAdapter.OnHeadingSelectedListener;

public class InboxFragment extends Fragment implements OnHeadingSelectedListener{

	private InboxHeadingsFragment masterFragment;
	private InboxDetailFragment detailFragment;
	private boolean mIsDualPane = false;
    private static View view;


   // @Override
    //public void onCreate(Bundle savedInstanceState) {
      //  setRetainInstance(true);
       // super.onCreate(savedInstanceState);
    //}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

       if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        else {
            view = inflater.inflate(R.layout.inbox_layout, container, false);
        }

		masterFragment = (InboxHeadingsFragment)getActivity().getSupportFragmentManager().
                findFragmentById(R.id.master_fragment);
		detailFragment = (InboxDetailFragment)getActivity().getSupportFragmentManager().
                findFragmentById(R.id.detail_fragment);

		View detailView = view.findViewById(R.id.detail_fragment);
		mIsDualPane = detailView != null && detailView.getVisibility() == View.VISIBLE;

		
		if(masterFragment==null) masterFragment = new InboxHeadingsFragment();
		Fragment fragment = masterFragment;//new InboxHeadingsFragment();

		if (fragment != null) {
			FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
			transaction.replace(R.id.master_fragment, fragment);
			//transaction.addToBackStack(null);
			transaction.commit();

		}
		masterFragment.setOnHeadlineSelectedListener(this);
        return view;
    }


    @Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
	}

	@Override
	public void onHeadingSelected(int index) {

		/*if(mIsDualPane)
		{
            detailFragment = (InboxDetailFragment)getActivity().getSupportFragmentManager().
                    findFragmentById(R.id.detail_fragment);
            if(detailFragment==null) detailFragment = new InboxDetailFragment();

			detailFragment.showItem(index);
		} else {*/
			Intent i = new Intent(this.getActivity(), InboxDetailFragmentActivity.class);
			i.putExtra("index", index);
			startActivity(i);
		//}
		
	}


    @Override
    public void onDestroyView() {
       super.onDestroyView();
       try {
            InboxHeadingsFragment fragment = (InboxHeadingsFragment) getActivity().getSupportFragmentManager().
                    findFragmentById(R.id.master_fragment);

            if (fragment != null) {
                FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                ft.remove(fragment);
                ft.commit();
                //getFragmentManager().beginTransaction().remove(fragment).commit();
            }


        } catch (IllegalStateException e) {
            Log.d("Exception", e.toString());
        }
    }

}
