package org.swadhin.app.fragments;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import org.swadhin.app.R;
import org.swadhin.app.db.GeneralDAO;
import org.swadhin.app.uiitems.AppCategoryItem;
import org.swadhin.app.uiitems.AppCategoryListAdapter;
import org.swadhin.app.utils.Utils;
import org.swadhin.sensors.appsensor.AppSensorService;
import org.swadhin.sensors.appsensor.db.AppCategoryDAO;

import android.app.ProgressDialog;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

public class AppCategoryFragment extends Fragment{

	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_all_apps, container, false);
	}


	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		/**************************************************
		 * Push back DB operations to background
		 **************************************************/
		new LoadDataFromDB(view).execute((Void)null);
	}
	
	/**
	 * Background Async task to load app category data from db
	 **/
	private class LoadDataFromDB extends AsyncTask<Void, Void, Void> {
		
		private View view;
		private ProgressDialog dialog = null;
		private ArrayList<AppCategoryItem> appInfoItems;
		public LoadDataFromDB(View view) {
			this.view = view;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog = new ProgressDialog(getActivity());
			dialog.setMessage("Loading your data...");
			dialog.show();
		}
		
		protected Void doInBackground(Void... args) {

			HashMap<String, String> categoryMap = AppSensorService.categoryMap;
			if(categoryMap==null || categoryMap.size()<=5)
			{
				try{
					synchronized (GeneralDAO.semaphore) {

						AppCategoryDAO dao = new AppCategoryDAO(view.getContext());
						dao.openRead();
						categoryMap = dao.getAppCategoryPairs();
						dao.close();
					}
				} catch(Exception e) {
					Utils.d("Error in reading category DB at AppCategoryFragment: "+e.getLocalizedMessage());
				}
				//If still null due to exception
				if(categoryMap == null) categoryMap = new HashMap<String,String>();

			}
			//Fetch categories
			PackageManager pm = view.getContext().getPackageManager();
			//get a list of installed apps.
			List<ApplicationInfo> packages = pm.getInstalledApplications(PackageManager.GET_META_DATA);
			appInfoItems = new ArrayList<AppCategoryItem>();
			for(ApplicationInfo aInfo: packages)
			{
				Drawable icon = aInfo.loadIcon(pm);//pm.getApplicationIcon(apps[i].app);
				String label = pm.getApplicationLabel(aInfo).toString();
				String category = (categoryMap!=null?categoryMap.get(aInfo.packageName):"Unknown");
				
				appInfoItems.add(new AppCategoryItem(label, category, icon, aInfo.packageName));
			}
			Collections.sort(appInfoItems, new Comparator<AppCategoryItem>(){

				@Override
				public int compare(AppCategoryItem arg0, AppCategoryItem arg1) {
					boolean a0null =  arg0.getCategory()==null;
					boolean a1null = arg1.getCategory()==null;
					if(a0null || a1null)
					{
						if(a0null && a1null)
							return 0;
						if(a0null) return -1;
						if(a1null) return 1;
					}
					int ret = (arg0.getCategory().compareTo(arg1.getCategory()));
					if(ret==0) ret = (arg0.getTitle().compareTo(arg1.getTitle()));
					return ret;
				}});

			
			return null;
		}

		protected void onPostExecute(Void arg) {
			if(dialog!=null)
				dialog.dismiss();
			ListView mAppInfoList = (ListView) view.findViewById(R.id.item_app_info_list);
			// setting the nav drawer list adapter
			AppCategoryListAdapter adapter = new AppCategoryListAdapter(view.getContext(),
					appInfoItems);
			mAppInfoList.setAdapter(adapter);
		}
	}

}
