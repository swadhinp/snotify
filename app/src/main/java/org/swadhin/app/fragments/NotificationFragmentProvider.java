package org.swadhin.app.fragments;

import org.swadhin.sensors.appsensor.db.AppUsageDAO;

import common.view.ColorTabItem;
import common.view.FragmentProvider;

public class NotificationFragmentProvider extends FragmentProvider{

	@Override
	public int getNumFragments() {
		return 4;
	}

	@Override
	public String getTitle(int position) {

		switch(position)
		{
		case 0: return "Today";
		case 1: return "Yesterday";
		case 2: return "Week";
		case 3: return "Last Week";
		}
		return null;
	}

	@Override
	public ColorTabItem getColorTabItem(int position) {

		switch(position)
		{
		case 0: return new NotificationsTabItem(AppUsageDAO.TABLE_TYPE_DAILY, 0);
		case 1: return new NotificationsTabItem(AppUsageDAO.TABLE_TYPE_DAILY, -1);
		case 2: return new NotificationsTabItem(AppUsageDAO.TABLE_TYPE_WEEKLY, 0);
		case 3: return new NotificationsTabItem(AppUsageDAO.TABLE_TYPE_WEEKLY, -1);
		}
		return null;
	}

}
