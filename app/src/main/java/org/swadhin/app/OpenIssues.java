/**
 * 
 */
package org.swadhin.app;

/**
 * Lists open issues to address in the app.
 * 
 * <ul>
 * <li><strike>Log location events in app sensor</strike></li>
 * <li><strike>Log notifications in app sensor</strike></li>
 * <li><strike>Activity Recognition.</strike></li>
 * <li><strike>Step counting and detection.</strike></li>
 * <li>Provide abstract service to all sensors that should be implemented.
 * <ul><li>should notify when bluetooth state changes.</li>
 * <li>should list multiple devices to start logging from.</li>
 * </ul>
 * </li>
 * <li>Everything related to tasks: Logic, Sensor/task Execution, UI implementation.</li>
 * <li>How should sensor services engage with UI?</li>
 * <li>Logfile logic should be separated completely from the utility service.</li>
 * <li></li>
 * <li></li>
 * <li></li>
 * <li></li>
 * <li></li>
 * <li></li>
 * <li></li>
 * <li></li>
 * <li></li>
 * <li></li>
 * <li></li>
 * <li></li>
 * <li></li>
 * <li></li>
 * <li></li>
 * <li></li>
 * <li></li>
 * <li></li>
 * </ul>
 * @author Abhinav Parate
 * 
 */
public class OpenIssues {

}
