/**
 * 
 */
package org.swadhin.app;

/**
 * Constants required to identify UI fragments/Actions in a navigation drawer and
 * to use consistent string constants throughout the app.
 * 
 * @author Abhinav Parate
 * 
 */
public final class UiConstants {

	/* Global constants to refer UI fragments/Actions in Navigation Drawer */
	
	/** Identifier for Home UI fragment.*/
	public static final int UI_HOME = 0;
	

	/** Identifier for Notification UI fragment.*/
	public static final int UI_NOTIFICATION = 1;
	/** Identifier for App Sensor UI fragment.*/
	public static final int UI_APPSENSOR = 2;
	/** Identifier for List of app UI fragment.*/
	public static final int UI_MYAPPS = 3;
	/** Identifier for Settings UI fragment.*/
	public static final int UI_SETTINGS = 4;
	/** Identifier for Signout action.*/
	public static final int UI_SIGNOUT = 5;
	/** Identifier for delete account action.*/
	//public static final int UI_DELETE_ACCOUNT = 6;
	
	
	public static final String sUser = "user";
	
}
