package org.swadhin.app.service;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.LinkedList;

import org.swadhin.app.utils.Utils;

/**
 * This class implements the log writer to be used by sensors to write data.
 * 
 * <p/><tt>LogWriter</tt> instance can only be obtained through {@link UtilityService}. Please
 * refer to {@link UtilityService#getLogWriter} for more details.
 * 
 * 
 * @author Abhinav Parate
 * 
 */
public class LogWriter{


	private BufferedWriter out = null;
	private boolean closed = false;

	protected LogWriter(BufferedWriter out){
		this.out = out;
		closed = false;
	}

	public void  writeLog(String log){
		try{
			if(!closed)
				out.write(log+"\n");
		} catch(IOException e){
			Utils.d("Error writing log "+log);
			e.printStackTrace();
		}
	}

	public synchronized void writeLog(LinkedList<String> logs){
		String log = null;
		while((log=logs.poll())!=null){
			try{
				if(!closed)
					out.write(log+"\n");
			} catch(IOException e){
				Utils.d("Error writing log "+log);
				e.printStackTrace();
			}
		}
	}

	protected synchronized void close(){
		try{
			closed = true;
			out.flush();
			out.close();
		}catch(IOException e){
			e.printStackTrace();
		}
	}
	
	public synchronized boolean isClosed(){
		return closed;
	}




}
