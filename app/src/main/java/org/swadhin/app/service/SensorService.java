/**
 * 
 */
package org.swadhin.app.service;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;

/**
 * An abstract service that must be extended by each sensor supported
 * by the Sarayu app.
 * 
 * @author Abhinav Parate
 * 
 */
public abstract class SensorService extends Service{

	
	/**
	 * Class used for the client Binder. For interaction with service
	 * without IPC
	 */
	public class SensorBinder extends Binder {
		public SensorService getService() {
			// Return this instance of LocalService so clients can call public methods
			return SensorService.this;
		}
	}
	
	
	private final IBinder mBinder = new SensorBinder();
	

	/* (non-Javadoc)
	 * @see android.app.Service#onBind(android.content.Intent)
	 */
	@Override
	public IBinder onBind(Intent intent) {
		return mBinder;
	}


	@Override
	public void onCreate() {
		super.onCreate();

	}

	@Override
	public void onDestroy() {
		super.onDestroy();

	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		return START_STICKY; // run until explicitly stopped.
	}


	
	/**
	 * Starts Logging if not started already
	 * @param activate start/stop logging
	 */
	public abstract void startLogging(boolean activate);
	


	
	//------------------------------------------------------------------------
	

	

}
