/**
 * Provides core services required for sensor data acquisition and to provide logging
 * services to various sensors. 
 * <p>  
 *
 *@author Abhinav Parate
 *
 */
package org.sarayu.app.service;