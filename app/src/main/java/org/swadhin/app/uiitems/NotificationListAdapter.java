/**
 * 
 */
package org.swadhin.app.uiitems;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Notification;
import android.app.Notification.Action;
import android.app.PendingIntent;
import android.app.PendingIntent.CanceledException;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.service.notification.StatusBarNotification;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.RemoteViews;
import android.widget.TextView;

import org.swadhin.app.R;
import org.swadhin.sensors.appsensor.AppSensorService;
import org.swadhin.sensors.appsensor.AppUsageEvent;
import org.swadhin.sensors.appsensor.HardwareObserver;
import org.swadhin.sensors.appsensor.LocationObserver;
import org.swadhin.sensors.appsensor.NotificationObserver;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Adapter to be used for Navigation Drawer List items.
 * 
 * @author Abhinav Parate
 * 
 * 
 */
public class NotificationListAdapter extends BaseAdapter {

	private Context context;
	private ArrayList<StatusBarNotification> snList;
	private int VIEW_ID = 0x750;
	private PackageManager pm;
	private TextView numNotificationsView;

	public NotificationListAdapter(Context context, ArrayList<StatusBarNotification> snList, TextView numNotificationsView){
		this.context = context;
		this.snList = snList;
		pm = context.getPackageManager();
		this.numNotificationsView = numNotificationsView;
	}

	@Override
	public int getCount() {
		return snList.size();
	}

	@Override
	public Object getItem(int position) {       
		return snList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	private static final int STYLE_BIG_TEXT = 0;
	private static final int STYLE_BIG_PICTURE = 1;
	private static final int STYLE_INBOX = 2;
	private static final int STYLE_MEDIA = 3;
	private static final int STYLE_NONE = 4;
	
	@SuppressLint("InflateParams") @Override
	public View getView(int position, View convertView, ViewGroup parent) {

		final StatusBarNotification item = snList.get(position);

		//Extract Style from the notification
		Notification n = item.getNotification();
		int style = extractStyle(n.extras.getString(Notification.EXTRA_TEMPLATE));

		//Inflate view appropriate to the style
		if (convertView == null) {
			LayoutInflater mInflater = (LayoutInflater)
					context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
			convertView = getInflatedConvertView(style, mInflater);
		}

		populateView(convertView, item, n, style, item.getPackageName());

		return convertView;
	}

	private int extractStyle(String styleString)
	{
		int style = STYLE_NONE;
		if(styleString!=null)
		{
			if(styleString.equals("android.app.Notification$BigTextStyle"))
			{
				style = STYLE_BIG_TEXT;
			} else if(styleString.equals("android.app.Notification$BigPictureStyle"))
			{
				style = STYLE_BIG_PICTURE;
			} else if(styleString.equals("android.app.Notification$InboxStyle"))
			{
				style = STYLE_INBOX;
			} else if(styleString.equals("android.app.Notification$MediaStyle"))
			{
				style = STYLE_BIG_TEXT;
			} else
			{
				style = STYLE_NONE;
			}
		} else {
			style = STYLE_NONE;
		}
		return style;
	}

	private View getInflatedConvertView(int style, LayoutInflater mInflater)
	{
		switch(style)
		{
		case STYLE_BIG_PICTURE:
			//return mInflater.inflate(R.layout.notification_bigpicture_list_item, null);
		case STYLE_BIG_TEXT:
			//return mInflater.inflate(R.layout.notification_bigtext_list_item, null);
		case STYLE_INBOX:
			//return mInflater.inflate(R.layout.notification_inbox_list_item, null);
		case STYLE_MEDIA:
		case STYLE_NONE:
		default:
			return mInflater.inflate(R.layout.notification_simple_list_item, null);
		}
	}

	private void populateView(View view, StatusBarNotification sn, Notification n, int style, String packageName)
	{
		try{
			switch(style)
			{
			case STYLE_BIG_PICTURE:
				//prepareBigPictureView(view, sn, n, packageName);
				//break;
			case STYLE_BIG_TEXT:
				//prepareBigTextView(view, sn, n, packageName);
				//break;
			case STYLE_INBOX:
				//prepareInboxView(view, sn, n, packageName);
				//break;
			case STYLE_MEDIA:
			case STYLE_NONE:
			default:
				prepareSimpleView(view, sn, n, packageName);
				break;
			}
		}catch(Exception e){}
	}

	/*
	private void prepareBigTextView(View view, StatusBarNotification sn, Notification n, String packageName)
	{


		ImageView nIcon = (ImageView)view.findViewById(R.id.notification_icon);
		TextView titleView = (TextView)view.findViewById(R.id.notification_title);
		TextView detailView = (TextView)view.findViewById(R.id.notification_detail);
		TextView summaryView = (TextView)view.findViewById(R.id.notification_summary);
		RelativeLayout actionLayout = (RelativeLayout)view.findViewById(R.id.notification_actions_layout);
		View dismissButton = view.findViewById(R.id.notification_dismiss_button);

		setNotificationIcon(nIcon, n, packageName, STYLE_BIG_TEXT);

		Bundle extras = n.extras;
		setNotificationTitle(titleView, extras);

		setNotificationDetail(detailView, extras, STYLE_BIG_TEXT);

		setSummary(summaryView, extras, STYLE_BIG_TEXT);

		if(n.actions!=null)
			setActions(actionLayout, sn, n.actions, packageName);
		else actionLayout.removeAllViewsInLayout();

		setDismissButton(dismissButton, sn, n);

		setOnClickIntentListener(view, sn, n);
	}

	private void prepareBigPictureView(View view, StatusBarNotification sn, Notification n, String packageName)
	{
		ImageView nIcon = (ImageView)view.findViewById(R.id.notification_icon);
		TextView titleView = (TextView)view.findViewById(R.id.notification_title);
		TextView detailView = (TextView)view.findViewById(R.id.notification_detail);
		ImageView pictureView = (ImageView)view.findViewById(R.id.notification_picture);
		TextView summaryView = (TextView)view.findViewById(R.id.notification_summary);
		RelativeLayout actionLayout = (RelativeLayout)view.findViewById(R.id.notification_actions_layout);
		View dismissButton = view.findViewById(R.id.notification_dismiss_button);

		setNotificationIcon(nIcon, n, packageName, STYLE_BIG_PICTURE);

		Bundle extras = n.extras;
		setNotificationTitle(titleView, extras);

		setNotificationDetail(detailView, extras, STYLE_BIG_PICTURE);

		setSummary(summaryView, extras, STYLE_BIG_PICTURE);

		setPicture(pictureView, extras);

		if(n.actions!=null)
			setActions(actionLayout, sn, n.actions, packageName);
		else actionLayout.removeAllViewsInLayout();

		setDismissButton(dismissButton, sn, n);

		setOnClickIntentListener(view, sn, n);
	}

    @TargetApi(Build.VERSION_CODES.KITKAT)
	private void prepareInboxView(View view, StatusBarNotification sn, Notification n, String packageName)
	{
		ImageView nIcon = (ImageView)view.findViewById(R.id.notification_icon);
		TextView titleView = (TextView)view.findViewById(R.id.notification_title);
		TextView detailView = (TextView)view.findViewById(R.id.notification_detail);
		TextView summaryView = (TextView)view.findViewById(R.id.notification_summary);
		RelativeLayout actionLayout = (RelativeLayout)view.findViewById(R.id.notification_actions_layout);
		//View dismissButton = view.findViewById(R.id.notification_dismiss_button);

		setNotificationIcon(nIcon, n, packageName, STYLE_INBOX);

		Bundle extras = n.extras;
		setNotificationTitle(titleView, extras);

		setNotificationDetail(detailView, extras, STYLE_INBOX);

		setSummary(summaryView, extras, STYLE_INBOX);

		if(n.actions!=null)
			setActions(actionLayout, sn, n.actions, packageName);
		else actionLayout.removeAllViewsInLayout();

		//setDismissButton(dismissButton, sn, n);

		setOnClickIntentListener(view, sn, n);

	}*/

    @TargetApi(Build.VERSION_CODES.KITKAT)
	private void prepareSimpleView(View view, StatusBarNotification sn, Notification n, String packageName)
	{

		ImageView nIcon = (ImageView)view.findViewById(R.id.notification_icon);
		TextView titleView = (TextView)view.findViewById(R.id.notification_title);
		TextView detailView = (TextView)view.findViewById(R.id.notification_detail);
		TextView summaryView = (TextView)view.findViewById(R.id.notification_summary);
		//RelativeLayout actionLayout = (RelativeLayout)view.findViewById(R.id.notification_actions_layout);
		Button dismissButton = (Button)view.findViewById(R.id.notification_dismiss_button);
        RadioGroup rad_grp = (RadioGroup)view.findViewById(R.id.notif_radio_grp);

		setNotificationIcon(nIcon, n, packageName, STYLE_NONE);

		Bundle extras = n.extras;
		setNotificationTitle(titleView, extras);

		setNotificationDetail(detailView, extras, STYLE_NONE);

		setSummary(summaryView, extras, STYLE_NONE);

		//if(n.actions!=null)
		//	setActions(actionLayout, sn, n.actions, packageName);
		//else actionLayout.removeAllViewsInLayout();

		setDismissButton(dismissButton, rad_grp, sn, n);

		setOnClickIntentListener(view, sn, n);
	}

    @TargetApi(Build.VERSION_CODES.KITKAT)
	private void setNotificationIcon(ImageView nIcon, Notification n, String packageName, int style)
	{
		/*Bitmap large = n.largeIcon;
		if(style == STYLE_BIG_PICTURE)
		{
			large = (Bitmap)n.extras.get(Notification.EXTRA_LARGE_ICON_BIG);
			if(large == null)
				large = n.largeIcon;
		}
		if(large!=null)
		{
			nIcon.setImageBitmap(large);
			nIcon.setPadding(0, 0, 0, 0);
		}
		else {*/

        try {
            Drawable icon = pm.getResourcesForApplication(packageName).getDrawable(n.icon);
                /*Bitmap bitmap = ((BitmapDrawable) icon).getBitmap();
				icon = new BitmapDrawable(context.getResources(), Bitmap.createScaledBitmap(bitmap, 20, 20, true));*/
            DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
            int pxy = Math.round(20 * (displayMetrics.ydpi / DisplayMetrics.DENSITY_DEFAULT));
            int pxx = Math.round(20 * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
            nIcon.setImageDrawable(icon);
            nIcon.setPadding(pxx, pxy, pxx, pxy);
        } catch (PackageManager.NameNotFoundException | Resources.NotFoundException e) {
            Log.d("setNotIcon", e.toString());
        }
        //}
	}

	private void setNotificationTitle(TextView view, Bundle extras)
	{
		String title = extras.getString(Notification.EXTRA_TITLE_BIG,null);
		if(title == null || title.trim().equals("")) title = extras.getString(Notification.EXTRA_TITLE);
		Log.d("SWADHIN","TITLE "+title);
		if(title == null)
		{
			view.setVisibility(View.GONE);
			title = "";
		}
		view.setText(title);
	}

	@TargetApi(Build.VERSION_CODES.KITKAT)
    private void setNotificationDetail(TextView view, Bundle extras, int style)
	{
		//Sets Big Text | Inbox Lines | Content
		String text = null;
		if(style == STYLE_BIG_TEXT)
			text = extras.getString(Notification.EXTRA_BIG_TEXT, null);
		else if(style == STYLE_INBOX)
		{
			CharSequence[] lines = extras.getCharSequenceArray(Notification.EXTRA_TEXT_LINES);
			if(lines!=null)
			{
				text = "";
				for(CharSequence seq: lines)
				{
					text+=(seq+"\n");
				}
			}
		}
		if(text == null || text.trim().equals("")) text = extras.getString(Notification.EXTRA_TEXT);
		if(text == null)
		{
			view.setVisibility(View.GONE);
			text = "";
		}

		view.setText(text);
	}

	private void setSummary(TextView view, Bundle extras, int style)
	{
		String summary = null;
		if(style == STYLE_NONE)
			summary = extras.getString(Notification.EXTRA_SUB_TEXT, null);
		else
			summary = extras.getString(Notification.EXTRA_SUMMARY_TEXT, null);
		if(summary == null)
		{
			view.setVisibility(View.GONE);
			summary = "";
		}
		view.setText(summary);
	}

	private void setPicture(ImageView view, Bundle extras)
	{
		Bitmap bm = (Bitmap)extras.get(Notification.EXTRA_PICTURE);
		if(bm!=null)
		{
			view.setImageBitmap(bm);
		} else view.setVisibility(View.GONE);
	}

	@TargetApi(Build.VERSION_CODES.KITKAT)
    @SuppressWarnings("unused")
	private void setActionsOld(RelativeLayout rl, Action[] actions, String packageName)
	{
		int aboveView = -1;
		for(int i=0;i<actions.length;i++)
		{
			final Action action = actions[i];
			Drawable actIcon = null;
			try{
				actIcon = pm.getResourcesForApplication(packageName).getDrawable(action.icon);
			} catch(Exception e){}
			Bitmap bitmap = ((BitmapDrawable) actIcon).getBitmap();
			// Scale it to 50 x 50
			actIcon = new BitmapDrawable(context.getResources(), Bitmap.createScaledBitmap(bitmap, 48, 48, true));
			//actIcon = new ScaleDrawable(actIcon, 0, 50, 50).getDrawable();
			//actIcon.setBounds(0, 0, 50, 50);
			//actIcon = d;
			Button button = new Button(context);
			button.setBackgroundResource(R.drawable.custom_btn_beige_flat);
			button.setCompoundDrawablesWithIntrinsicBounds(actIcon, null, null, null);
			button.setText(action.title);
			button.setTextAppearance(context, R.style.SplashFontTitleBlack);
			button.setId(VIEW_ID++);

			//button.setPadding(10, 10, 10, 10);
			rl.addView(button);

			RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)button.getLayoutParams();
			if(aboveView!=-1)
				params.addRule(RelativeLayout.BELOW,aboveView);
			if(i%2==0)
				params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
			else 
				params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
			params.setMargins(5, 5, 5, 5);
			button.setLayoutParams(params);

			OnClickListener listener = new OnClickListener() {

				@Override
				public void onClick(View v) {
					PendingIntent intent = action.actionIntent;
					try {
						if(intent!=null)
							intent.send();
					} catch (Exception e) {
					}
					numNotificationsView.setText(snList.size()+"");
				}
			};
			button.setOnClickListener(listener);
			if(i%2==1) aboveView = button.getId();
		}
		//End for loop
	}


	//@SuppressWarnings("unused")
	@TargetApi(Build.VERSION_CODES.KITKAT)
	private void setActions(RelativeLayout rl, final StatusBarNotification sn, Action[] actions, String packageName)
	{
		rl.removeAllViewsInLayout();
		LinearLayout ll = new LinearLayout(context);
		ll.setId(VIEW_ID++);
		ll.setOrientation(LinearLayout.HORIZONTAL);
		rl.addView(ll);
		int prevLL = ll.getId();

		for(int i=0;i<actions.length;i++)
		{
			final Action action = actions[i];
			Drawable actIcon = null;
			try{
				actIcon = pm.getResourcesForApplication(packageName).getDrawable(action.icon);
			} catch(Exception e){}
			Bitmap bitmap = ((BitmapDrawable) actIcon).getBitmap();
			// Scale it to 50 x 50
			actIcon = new BitmapDrawable(context.getResources(), Bitmap.createScaledBitmap(bitmap, 48, 48, true));


			Button button = new Button(context,null,0);
			button.setBackgroundResource(R.drawable.custom_btn_beige_flat);
			DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
			int px = Math.round(50 * (displayMetrics.ydpi / DisplayMetrics.DENSITY_DEFAULT)); 
			button.setMaxHeight(px);
			button.setCompoundDrawablesWithIntrinsicBounds(actIcon, null, null, null);
			button.setText(action.title);
			button.setTextAppearance(context, R.style.SplashFontTitleBlack);

			ll.addView(button);

			LinearLayout.LayoutParams lparams = (LinearLayout.LayoutParams)button.getLayoutParams();
			lparams.setMargins(5, 5, 5, 5);
			button.setLayoutParams(lparams);

			final NotificationListAdapter adapter = this;
			OnClickListener listener = new OnClickListener() {

				@Override
				public void onClick(View v) {
					PendingIntent intent = action.actionIntent;
					try {
						if(intent!=null)
							intent.send();
					} catch (Exception e) {
					}
					if((sn.getNotification().flags&Notification.FLAG_AUTO_CANCEL)!=0) {
                        snList.remove(sn);
                        removeFromMap(sn); // Swadhin : Removing from the notification

                    }
					//snList.remove(sn);
					numNotificationsView.setText(snList.size()+"");
					adapter.notifyDataSetChanged();
//					NotificationObserver observer = NotificationObserver.getInstance();
//					if(observer!=null && (sn.getNotification().flags&Notification.FLAG_AUTO_CANCEL)!=0)
//						observer.cancelNotification(sn.getKey());
					//TODO: REMOVE VIEW ON CANCELLATION
				}
			};
			button.setOnClickListener(listener);
			if(i%3==2)
			{
				ll = new LinearLayout(context);
				ll.setId(VIEW_ID++);
				ll.setOrientation(LinearLayout.HORIZONTAL);
				rl.addView(ll);
				RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)ll.getLayoutParams();
				params.addRule(RelativeLayout.BELOW, prevLL);
				prevLL = ll.getId();
			}
		}
		//End for loop
	}

	private void setDismissButton(View button, final RadioGroup radgrp, final StatusBarNotification sn, Notification n)
	{
		final PendingIntent intent = n.deleteIntent;
		final NotificationListAdapter adapter = this;
		//final StatusBarNotification sbn = sn;
		OnClickListener listener = new OnClickListener() {

			@TargetApi(Build.VERSION_CODES.LOLLIPOP)
			@Override
			public void onClick(View v) {


                Log.d("swadhin", "Start Getting Notif Info");

                AppUsageEvent aue = new AppUsageEvent();
                aue.taskID = sn.getId();
                aue.packageName = sn.getPackageName();
                aue.eventtype = "event_feedback";
                aue.starttime = sn.getPostTime(); //Time when it was posted
                aue.runtime = 0; // since we don't know when it ends
                aue.longitude = LocationObserver.longitude;
                aue.latitude = LocationObserver.latitude;
                aue.accuracy = LocationObserver.accuracy;
                aue.powerstate = HardwareObserver.powerstate;
                aue.wifistate = HardwareObserver.wifistate;
                aue.bluetoothstate = HardwareObserver.bluetoothstate;
                aue.headphones = HardwareObserver.headphones;
                aue.orientation = HardwareObserver.orientation;
                aue.timestampOfLastScreenOn = HardwareObserver.timestampOfLastScreenOn;

                Notification not = sn.getNotification();

                List<String> str_list = getTextString(not);
                int not_word_count = 0;

                for (String s : str_list)
                {
                    not_word_count += 1;
                }

                aue.notif_style= extractStyleInfo(not.extras.getString(Notification.EXTRA_TEMPLATE));
                aue.notif_word_count = not_word_count;

                aue.notif_tag = sn.getTag();
                if(null != not.tickerText) {
                    aue.notif_tickerText = not.tickerText.toString();
                }else{
                    aue.notif_tickerText = "None";
                }
                aue.notif_id = Integer.toString(sn.getId());
                aue.notif_ledRGB = Integer.toString(not.ledARGB);
                aue.notif_ledOff = Integer.toString(not.ledOffMS);
                aue.notif_ledOn = Integer.toString(not.ledOnMS);
                if( null != not.sound) {
                    aue.notif_sound = not.sound.toString();
                }else{
                    aue.notif_sound = "None";
                }

                if( null != not.vibrate){
                    for (long l : not.vibrate) {
                        aue.notif_vibrate = Long.toString(l) + ":";
                    }
                }else{
                    aue.notif_vibrate = "None";
                }

                aue.notif_priority = Integer.toString(not.priority);
                aue.notif_icon = Integer.toString(not.icon);
                aue.notif_clearable = Boolean.toString(sn.isClearable());
                aue.notif_ongoing = Boolean.toString(sn.isOngoing());

                if( radgrp.getCheckedRadioButtonId() == R.id.radioButton1 ) {
                    aue.feedback = "Important";
                }else if( radgrp.getCheckedRadioButtonId() == R.id.radioButton2){
                    aue.feedback = "Useless";
                }else if( radgrp.getCheckedRadioButtonId() == R.id.radioButton3){
                    aue.feedback = "Neutral";
                }else{
                    aue.feedback = "None";
                }



                AppSensorService.logEvent(aue);
                //Toast.makeText(context,"R: " + aue.feedback, Toast.LENGTH_SHORT).show();

                snList.remove(sn);
                removeFromMap(sn); // Swadhin : Removing from the notification

                if(intent!=null)
                {
                    try {
                        intent.send();
                    } catch (CanceledException e) {

                    }
                }

				numNotificationsView.setText(snList.size() + "");
				adapter.notifyDataSetChanged();
                Log.d("swadhin", "Stop Getting Notif Info");

				NotificationObserver observer = NotificationObserver.getInstance();

				//if(observer!=null && sn.isClearable())
				//	observer.cancelNotification(sn.getKey());
			}
		};
		button.setOnClickListener(listener);
	}

    private static void removeFromMap(StatusBarNotification notification)
    {
        String app = notification.getPackageName();
        int id = notification.getId();

        ArrayList<StatusBarNotification> list = getNotificationList(app);
        for(StatusBarNotification sn: list)
        {
            if(sn.getId()==id)
            {
                list.remove(sn);
                break;
            }
        }
    }

    private static ArrayList<StatusBarNotification> getNotificationList(String app)
    {
        ArrayList<StatusBarNotification> list = NotificationObserver.nmaps.get(app);
        if(list==null)
        {
            list = new ArrayList<StatusBarNotification>();
            NotificationObserver.nmaps.put(app, list);
        }
        return list;
    }

	private void setOnClickIntentListener(View view, final StatusBarNotification sn, Notification n)
	{

		final PendingIntent intent =n.contentIntent; 
		final NotificationListAdapter adapter = this;

		view.setOnClickListener(new OnClickListener(){

			@TargetApi(Build.VERSION_CODES.KITKAT)
            @Override
			public void onClick(View arg0) {

				try {
					if(intent!=null)
						intent.send();
				} catch (CanceledException e) {
					e.printStackTrace();
				}

				if((sn.getNotification().flags&Notification.FLAG_AUTO_CANCEL)!=0) {
                    snList.remove(sn);
                    removeFromMap(sn); // Swadhin : Removing from the notification
                }

				numNotificationsView.setText(snList.size()+"");
				adapter.notifyDataSetChanged();
				//NotificationObserver observer = NotificationObserver.getInstance();
				//if(observer!=null && (sn.getNotification().flags&Notification.FLAG_AUTO_CANCEL)!=0)
				//	observer.cancelNotification(sn.getKey());

			}
		});
	}

    public static List<String> getTextString(Notification notification)
    {
        // We have to extract the information from the view
        RemoteViews views = (RemoteViews)notification.bigContentView;
        if (views == null) views = (RemoteViews)notification.contentView;
        if (views == null) views = (RemoteViews)notification.tickerView;
        if (views == null) return null;

        // Use reflection to examine the m_actions member of the given RemoteViews object.
        // It's not pretty, but it works.
        List<String> text = new ArrayList<String>();
        try
        {
            Field field = null;

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                //Log.e("Obs", views.getClass().getSuperclass().getSimpleName());
                field = views.getClass().getSuperclass().getDeclaredField("mActions");
            }else{
                //Log.e("Obs", views.getClass().getSimpleName());
                field = views.getClass().getDeclaredField("mActions");
            }

            field.setAccessible(true);

            @SuppressWarnings("unchecked")
            ArrayList<Parcelable> actions = (ArrayList<Parcelable>) field.get(views);

            // Find the setText() and setTime() reflection actions
            for (Parcelable p : actions)
            {
                Parcel parcel = Parcel.obtain();

                if (parcel == null) continue;
                if (p == null) continue;

                p.writeToParcel(parcel, 0);
                parcel.setDataPosition(0);

                // The tag tells which type of action it is (2 is ReflectionAction, from the source)
                int tag = parcel.readInt();
                if (tag != 2) continue;
                // View ID
                parcel.readInt();

                String methodName = parcel.readString();
                if (methodName != null)
                    Log.e("Obs", methodName);

                if (methodName == null) continue;
                    // Save strings
                else if (methodName.equals("setText"))
                {
                    // Parameter type (10 = Character Sequence)
                    parcel.readInt();

                    // Store the actual string
                    if( null != parcel) {
                        CharSequence cs = TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
                        if (null != cs) {

                            String t = cs.toString();

                            if (null != t) {
                                text.add(t.trim());
                            }
                        }
                    }
                }

                // Save times. Comment this section out if the notification time isn't important
                else if (methodName.equals("setTime"))
                {
                    // Parameter type (5 = Long)
                    parcel.readInt();

                    if( null != parcel) {
                        String t = new SimpleDateFormat("h:mm a").format(new Date(parcel.readLong()));
                        text.add(t);
                    }
                }

                parcel.recycle();

            }
        }
        // It's not usually good style to do this, but then again, neither is the use of reflection...
        catch (Exception e)
        {
            Log.e("NotificationObserver", e.toString());
        }

        return text;
    }

    private String extractStyleInfo(String styleString)
    {
        String style;

        if(styleString!=null)
        {
            if(styleString.equals("android.app.Notification$BigTextStyle"))
            {
                style = "BigText";
            } else if(styleString.equals("android.app.Notification$BigPictureStyle"))
            {
                style = "BigPic";
            } else if(styleString.equals("android.app.Notification$InboxStyle"))
            {
                style = "Inbox";
            } else if(styleString.equals("android.app.Notification$MediaStyle"))
            {
                style = "Media";
            } else
            {
                style = "Simple";
            }
        } else {
            style = "None";
        }

        return style;
    }
}
