/**
 * 
 */
package org.swadhin.app.uiitems;

import java.util.ArrayList;

import org.swadhin.app.R;
import org.swadhin.app.WeeklyStatsActivity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Adapter to be used for Navigation Drawer List items.
 * 
 * @author Abhinav Parate
 * 
 * 
 */
public class AppInfoListAdapter extends BaseAdapter {

	private Context context;
	private ArrayList<AppInfoItem> appInfoItems;
	private boolean isAppList = true;

	public AppInfoListAdapter(Context context, ArrayList<AppInfoItem> AppInfoItems, boolean isAppList){
		this.context = context;
		this.appInfoItems = AppInfoItems;
		this.isAppList = isAppList;
	}

	@Override
	public int getCount() {
		return appInfoItems.size();
	}

	@Override
	public Object getItem(int position) {       
		return appInfoItems.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@SuppressLint("InflateParams") @Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			LayoutInflater mInflater = (LayoutInflater)
					context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
			if(isAppList)
				convertView = mInflater.inflate(R.layout.app_info_list_item, null);
			else
				convertView = mInflater.inflate(R.layout.notification_info_list_item, null);
		}
		final View mainView = convertView;
		ImageView imgIcon = (ImageView) convertView.findViewById(R.id.app_item_icon);
		final TextView txtTitle = (TextView) convertView.findViewById(R.id.app_item_title);
		TextView txtCount = (TextView) convertView.findViewById(R.id.app_item_count);
		TextView txtDuration = (TextView) convertView.findViewById(R.id.app_item_duration);
		final View fullDurationBar = convertView.findViewById(R.id.app_item_full_duration);
		final View durationBar = convertView.findViewById(R.id.app_item_duration_bar);

		final AppInfoItem item = appInfoItems.get(position);
		imgIcon.setImageDrawable(item.getIcon());
		txtTitle.setText(item.getTitle());
		txtCount.setText(item.getUsageCount()+"");
		if(txtDuration!=null)
			txtDuration.setText(item.getDuration()+"");

		mainView.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(mainView.getContext(), WeeklyStatsActivity.class);
				Bundle b = new Bundle();
				b.putBoolean(WeeklyStatsActivity.KEY_IS_APP_INFO, isAppList);
				b.putString(WeeklyStatsActivity.KEY_PACKAGE_NAME, item.getPackageName());
				b.putString(WeeklyStatsActivity.KEY_APP_LABEL, item.getTitle());
				intent.putExtras(b);
				mainView.getContext().startActivity(intent);
			}
		});


		ViewTreeObserver vto = fullDurationBar.getViewTreeObserver();
		vto.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
			public boolean onPreDraw() {
				fullDurationBar.getViewTreeObserver().removeOnPreDrawListener(this);
				double percent = item.getPercentDuration();
				int width = (int)(fullDurationBar.getMeasuredWidth()*percent);
				durationBar.getLayoutParams().width=width;
				txtTitle.setText(item.getTitle());
				return true;
			}
		});



		return convertView;
	}

}
