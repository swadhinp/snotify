/**
 * 
 */
package org.swadhin.app.uiitems;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.swadhin.app.R;

import java.util.ArrayList;

/**
 * Adapter to be used for Navigation Drawer List items.
 * 
 * @author Abhinav Parate
 * 
 * 
 */
public class NavigationDrawerListAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<NavigationDrawerItem> NavigationDrawerItems;

    public NavigationDrawerListAdapter(Context context, ArrayList<NavigationDrawerItem> NavigationDrawerItems){
        this.context = context;
        this.NavigationDrawerItems = NavigationDrawerItems;
    }

    @Override
    public int getCount() {
        return NavigationDrawerItems.size();
    }

    @Override
    public Object getItem(int position) {       
        return NavigationDrawerItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.navigation_drawer_list_item, null);
        }

        ImageView imgIcon = (ImageView) convertView.findViewById(R.id.nav_item_icon);
        TextView txtTitle = (TextView) convertView.findViewById(R.id.nav_item_title);
        //TextView txtTag = (TextView) convertView.findViewById(R.id.nav_item_tag);

        imgIcon.setImageResource(NavigationDrawerItems.get(position).getIcon());        
        txtTitle.setText(NavigationDrawerItems.get(position).getTitle());
        //txtTag.setText(NavigationDrawerItems.get(position).getTag());

        return convertView;
    }

}
