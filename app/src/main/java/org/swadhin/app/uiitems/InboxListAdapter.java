/**
 * 
 */
package org.swadhin.app.uiitems;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.service.notification.StatusBarNotification;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.swadhin.app.R;

import java.util.ArrayList;

/**
 * Adapter to be used for Navigation Drawer List items.
 * 
 * @author Abhinav Parate
 * 
 * 
 */
public class InboxListAdapter extends BaseAdapter {

	private Context context;
	public static ArrayList<InboxItem> inboxItems;

	// The listener we are to notify when a headline is selected
	OnHeadingSelectedListener mHeadingSelectedListener = null;

	/**
	 * Represents a listener that will be notified of headline selections.
	 */
	public interface OnHeadingSelectedListener {
		/**
		 * Called when a given headline is selected.
		 * @param index the index of the selected headline.
		 */
		public void onHeadingSelected(int index);
	}

	/**
	 * Sets the listener that should be notified of headline selection events.
	 * @param listener the listener to notify.
	 */
	public void setOnHeadlineSelectedListener(OnHeadingSelectedListener listener) {
		mHeadingSelectedListener = listener;
	}

	public InboxListAdapter(Context context, ArrayList<InboxItem> InboxItems){
		this.context = context;
		inboxItems = InboxItems;
	}

	@Override
	public int getCount() {
		return inboxItems.size();
	}

	@Override
	public Object getItem(int position) {       
		return inboxItems.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@SuppressLint("InflateParams") @Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final InboxListAdapter adapter = this;

		if (convertView == null) {
			LayoutInflater mInflater = (LayoutInflater)
					context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
			convertView = mInflater.inflate(R.layout.inbox_header_list_item, null);
		}

		ImageView appIcon = (ImageView) convertView.findViewById(R.id.inbox_app_icon);
		TextView appTitle = (TextView) convertView.findViewById(R.id.inbox_app_title);
		TextView numNotifications = (TextView) convertView.findViewById(R.id.inbox_msg_cnt);

		final InboxItem item = inboxItems.get(position);

		appIcon.setImageDrawable(item.getIcon());
        adapter.notifyDataSetChanged();
		appTitle.setText(item.getTitle());
        adapter.notifyDataSetChanged();

		final ArrayList<StatusBarNotification> nList = item.getNotifications();
		numNotifications.setText(nList.size()+"");
		adapter.notifyDataSetChanged();

		/*ImageButton clearAllButton = (ImageButton) convertView.findViewById(R.id.inbox_clear_all_button);
		clearAllButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//TODO Add logic to clear all notifications for this app
			}
		});*/
		final int pos = position;
		convertView.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				mHeadingSelectedListener.onHeadingSelected(pos);
				
			}
		});
        adapter.notifyDataSetChanged();
		

		return convertView;
	}

}
