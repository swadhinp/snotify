/**
 * 
 */
package org.swadhin.app.uiitems;

import android.graphics.drawable.Drawable;

import java.util.Locale;

/**
 * @author Abhinav Parate
 * 
 * Creates an item in the view to show app usage details
 * 
 * <p/>This implementation followed the guidelines from <a href="http://stackoverflow.com/questions/16874826/how-to-add-icons-adjacent-to-titles-for-android-navigation-drawer">Stack Overflow</a>. 
 */
public class AppInfoItem {

    private String title;
    private long duration;
    private double percentDuration;
    private int usageCount;
    private Drawable icon;
    private String packageName;
    
    public AppInfoItem(){}

    /**
     * Creates a AppInfo Item
     * @param title Title
     * @param duration Duration
     * @param percentDuration Duration as percent of Max duration observed
     * @param usageCount Count of app usage
     * @param icon Icon
     */
    public AppInfoItem(String title, String packageName, long duration, double percentDuration, int usageCount, Drawable icon){
        this.title = title;
        this.packageName = packageName;
        this.duration = duration;
        this.percentDuration = percentDuration;
        this.usageCount = usageCount;
        this.icon = icon;
    }

    

    public String getTitle(){
        return this.title;
    }

    public String getDuration(){
    	long durationInSecs = duration/1000;
    	int secs = (int)(durationInSecs%60);
		int min = (int)(durationInSecs/60);
		int hour = (int)(min/60);
		min = min%60;
		
        return String.format(Locale.ENGLISH,"%02dh %02dm %02ds", hour,min,secs);
    }
    
    public Drawable getIcon(){
        return this.icon;
    }

    public void setTitle(String title){
        this.title = title;
    }

    public void setDuration(long duration){
        this.duration = duration;
    }

    public void setIcon(Drawable icon){
        this.icon = icon;
    }
    
    public int getUsageCount() {
		return usageCount;
	}

	public void setUsageCount(int usageCount) {
		this.usageCount = usageCount;
	}
	
	public double getPercentDuration() {
		return percentDuration;
	}

	public void setPercentDuration(double percentDuration) {
		this.percentDuration = percentDuration;
	}
	
	public String getPackageName() {
		return this.packageName;
	}

}