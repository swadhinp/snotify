/**
 * 
 */
package org.swadhin.app.uiitems;

import android.graphics.drawable.Drawable;

/**
 * @author Abhinav Parate
 * 
 * Creates an item in the view to show app usage details
 * 
 * <p/>This implementation followed the guidelines from <a href="http://stackoverflow.com/questions/16874826/how-to-add-icons-adjacent-to-titles-for-android-navigation-drawer">Stack Overflow</a>. 
 */
public class AppCategoryItem {

    private String title;
    private String category;
    private Drawable icon;
    private String packageName;
    
    public AppCategoryItem(){}

    /**
     * Creates a AppInfo Item
     * @param title Title
     * @param category category
     * @param icon Icon
     */
    public AppCategoryItem(String title, String category, Drawable icon, String packageName){
        this.title = title;
        this.category = category;
        this.icon = icon;
        this.packageName = packageName;
    }

    

    public String getTitle(){
        return this.title;
    }

    public String getCategory(){
    	return category;
    }
    
    public Drawable getIcon(){
        return this.icon;
    }

    public String getPackageName(){
    	return this.packageName;
    }
    

}