/**
 * 
 */
package org.swadhin.app.uiitems;

/**
 * @author Abhinav Parate
 * 
 * Encapsulates the functionalities of a custom navigation drawer item.
 * 
 * <p/>This implementation followed the guidelines from <a href="http://stackoverflow.com/questions/16874826/how-to-add-icons-adjacent-to-titles-for-android-navigation-drawer">Stack Overflow</a>. 
 */
public class NavigationDrawerItem {

    private String title;
    private String tag;
    private int icon;
    
    public NavigationDrawerItem(){}

    /**
     * Creates a Navigation Item in the drawer
     * @param title Title
     * @param tag Tag
     * @param icon Icon
     */
    public NavigationDrawerItem(String title, String tag, int icon){
        this.title = title;
        this.tag = tag;
        this.icon = icon;
    }

    

    public String getTitle(){
        return this.title;
    }

    public String getTag(){
        return this.tag;
    }

    public int getIcon(){
        return this.icon;
    }

    public void setTitle(String title){
        this.title = title;
    }

    public void setTag(String tag){
        this.tag = tag;
    }

    public void setIcon(int icon){
        this.icon = icon;
    }
}