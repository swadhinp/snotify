/**
 * 
 */
package org.swadhin.app.uiitems;

import java.util.ArrayList;

import android.graphics.drawable.Drawable;
import android.service.notification.StatusBarNotification;

/**
 * @author Abhinav Parate
 * 
 * Creates an item in the view to show app usage details
 * 
 * <p/>This implementation followed the guidelines from <a href="http://stackoverflow.com/questions/16874826/how-to-add-icons-adjacent-to-titles-for-android-navigation-drawer">Stack Overflow</a>. 
 */
public class InboxItem {

    private String title;
    private Drawable icon;
    private String packageName;
    private ArrayList<StatusBarNotification> list;
    
    public InboxItem(){}

    /**
     * Creates a AppInfo Item
     * @param title Title
     * @param category category
     * @param icon Icon
     */
    public InboxItem(String title, Drawable icon, String packageName, ArrayList<StatusBarNotification> list){
        this.title = title;
        this.icon = icon;
        this.packageName = packageName;
        this.list = list;
    }

    

    public String getTitle(){
        return this.title;
    }
    
    public Drawable getIcon(){
        return this.icon;
    }

    public String getPackageName(){
    	return this.packageName;
    }
    
    public ArrayList<StatusBarNotification> getNotifications(){
    	return this.list;
    }
    

}