/**
 * Implements small building block UI components and fragments used by the main application. 
 * <p>  
 *
 *@author Abhinav Parate
 *
 */
package org.swadhin.app.uiitems;