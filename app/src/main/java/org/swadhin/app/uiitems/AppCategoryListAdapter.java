/**
 * 
 */
package org.swadhin.app.uiitems;

import java.util.ArrayList;

import org.swadhin.app.R;
import org.swadhin.app.WeeklyStatsActivity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Adapter to be used for Navigation Drawer List items.
 * 
 * @author Abhinav Parate
 * 
 * 
 */
public class AppCategoryListAdapter extends BaseAdapter {

	private Context context;
	private ArrayList<AppCategoryItem> appCategoryItems;

	public AppCategoryListAdapter(Context context, ArrayList<AppCategoryItem> AppCategoryItems){
		this.context = context;
		this.appCategoryItems = AppCategoryItems;
	}

	@Override
	public int getCount() {
		return appCategoryItems.size();
	}

	@Override
	public Object getItem(int position) {       
		return appCategoryItems.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@SuppressLint("InflateParams") @Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			LayoutInflater mInflater = (LayoutInflater)
					context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
			convertView = mInflater.inflate(R.layout.app_category_list_item, null);
		}
		
		final View mainView = convertView;
		ImageView imgIcon = (ImageView) convertView.findViewById(R.id.app_item_icon);
		TextView txtTitle = (TextView) convertView.findViewById(R.id.app_item_title);
		TextView txtCategory = (TextView) convertView.findViewById(R.id.app_item_category);

		final AppCategoryItem item = appCategoryItems.get(position);
		imgIcon.setImageDrawable(item.getIcon());
		txtTitle.setText(item.getTitle());
		String category = item.getCategory();
		if(category==null) category = "Unknown";
		txtCategory.setText(category);

		mainView.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(mainView.getContext(), WeeklyStatsActivity.class);
				Bundle b = new Bundle();
				b.putBoolean(WeeklyStatsActivity.KEY_IS_APP_INFO, true);
				b.putString(WeeklyStatsActivity.KEY_PACKAGE_NAME, item.getPackageName());
				b.putString(WeeklyStatsActivity.KEY_APP_LABEL, item.getTitle());
				intent.putExtras(b);
				mainView.getContext().startActivity(intent);
			}
		});



		return convertView;
	}

}
