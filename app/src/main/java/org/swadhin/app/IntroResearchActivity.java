package org.swadhin.app;

import java.util.Calendar;
import java.util.GregorianCalendar;

import org.swadhin.sensors.appsensor.AppSensorService;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class IntroResearchActivity extends Activity implements OnItemSelectedListener{

	private static int days = 0;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_research);

		TextView desc = (TextView)findViewById(R.id.desc_view1);
		desc.setText(R.string.research_desc1);

		TextView desc2 = (TextView)findViewById(R.id.desc_view2);
		desc2.setText(R.string.research_desc2);

		Spinner spinner = (Spinner) findViewById(R.id.days_spinner);
		// Create an ArrayAdapter using the string array and a default spinner layout
		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
				R.array.days_array, android.R.layout.simple_spinner_item);
		// Specify the layout to use when the list of choices appears
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		// Apply the adapter to the spinner
		spinner.setAdapter(adapter);
		spinner.setSelection(1);
		spinner.setOnItemSelectedListener(this);
		
		/*CheckBox cbox = (CheckBox) findViewById(R.id.check_hash_app);
		cbox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton arg0, boolean checked) {
				final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
				final Resources rcs = getResources();
				SharedPreferences.Editor editor = prefs.edit();
				editor.putBoolean(rcs.getString(R.string.pref_hash_apps),checked);
				editor.apply();
			}
		});*/
		
		Button acceptButton = (Button)findViewById(R.id.accept_button);
		acceptButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if(days==0)
				{
					Toast.makeText(getApplicationContext(), "Please select non-zero value for number of days.", Toast.LENGTH_LONG).show();
					return;
				}
				//Toast.makeText(getApplicationContext(), "Thank you for participating in our research!", Toast.LENGTH_SHORT).show();
				final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
				final Resources rcs = getResources();
				
				SharedPreferences.Editor editor = prefs.edit();
				editor.putBoolean(rcs.getString(R.string.pref_enable_logging),true);
				editor.putBoolean(rcs.getString(R.string.pref_firsttimeuser),false);
				
				Calendar gc = GregorianCalendar.getInstance();
				//gc.add(Calendar.DAY_OF_YEAR, days);
				editor.putLong(rcs.getString(R.string.pref_sharing_day_start), gc.getTimeInMillis());
				editor.putString(rcs.getString(R.string.pref_sharing_days), days+"");
				Toast.makeText(getApplicationContext(), "Thank you for participating in our research!", Toast.LENGTH_SHORT).show();
				editor.apply();
				
				//Start app sensor service so that it can start logging
				AppSensorService.startByIntent(getBaseContext());
				Intent intent = new Intent(getApplicationContext(),HomeActivity.class);
				startActivity(intent);
			}
		});

		Button declineButton = (Button)findViewById(R.id.decline_button);
		declineButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
				final Resources rcs = getResources();
				SharedPreferences.Editor editor = prefs.edit();
				editor.putBoolean(rcs.getString(R.string.pref_enable_logging),false);
				editor.putBoolean(rcs.getString(R.string.pref_firsttimeuser),false);
				editor.apply();
				Toast.makeText(getApplicationContext(), "Thank you for using our app!", Toast.LENGTH_SHORT).show();
				Intent intent = new Intent(getApplicationContext(),HomeActivity.class);
				startActivity(intent);

			}
		});
	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int pos,
			long id) {
		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
				R.array.days_array, android.R.layout.simple_spinner_item);
		String dayStringSelected = adapter.getItem(pos).toString();
		days = Integer.parseInt(dayStringSelected);

	}

	@Override
	public void onNothingSelected(AdapterView<?> parent) {
		// TODO Auto-generated method stub

	}
}
