/**
 * Provides main UI implementation of the Sarayu App. 
 * <p>  
 *
 *@author Abhinav Parate
 *
 */
package org.swadhin.app;