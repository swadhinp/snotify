/**
 * 
 */
package org.swadhin.app.utils;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.util.Log;

import org.apache.http.Header;
import org.apache.http.HeaderElement;
import org.apache.http.HttpEntity;
import org.apache.http.HttpRequest;
import org.apache.http.HttpRequestInterceptor;
import org.apache.http.HttpResponse;
import org.apache.http.HttpResponseInterceptor;
import org.apache.http.client.HttpClient;
import org.apache.http.entity.HttpEntityWrapper;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HttpContext;

import java.io.IOException;
import java.io.InputStream;
import java.util.zip.GZIPInputStream;

/**
 * Implements basic utilities required by the app source such as logging, creating http client, etc.
 * 
 * @author Abhinav Parate
 * 
 */
public class Utils {

	/**
	 * Global Tag to identify the swadhin application.
	 */
	public static final String TAG = "swadhin";
	/**
	 * Enable debugging comments
	 */
	public static final boolean DEBUG = false;

	/**
	 * Logs debug messages.
	 * @param tag tag to identify the message.
	 * @param msg message string to print.
	 */
	public static void d(String tag, String msg) {
		if(DEBUG)
			Log.d(tag, msg);
	}

	/**
	 * Logs debug messages with global tag {@link org.swadhin.app.utils.Utils#TAG}.
	 * @param msg message string to print
	 */
	public static void d(String msg) {
		if(DEBUG)
			Log.d(TAG,msg);
	}

	/**
	 * Logs info messages.
	 * @param tag tag to identify the message.
	 * @param msg message string to print.
	 */
	public static void i(String tag, String msg) {
		Log.d(tag, msg);
	}

	/**
	 * Logs info messages with global tag {@link org.swadhin.app.utils.Utils#TAG}
	 * @param msg message string to print.
	 */
	public static void i(String msg) {
		Log.d(TAG,msg);
	}
	
	private static final int SECOND_IN_MILLIS = 1000;

	private static final String HEADER_ACCEPT_ENCODING = "Accept-Encoding";
	private static final String ENCODING_GZIP = "gzip";

	private static final int HTTP_SOCKET_BUFFERSIZE = 8192;

	/* default buffersize = 8kb */
	public static final int BUFFER_SIZE = 8 * 1024;
	/** charset for communication with the server */
	public static final String CHARSET = "UTF-8";
	
	/**
	 * Generate and return a {@link HttpClient} configured for general use,
	 * including setting an application-specific user-agent string.
	 */
	public static HttpClient createHttpClient(Context context) {
		
		final HttpParams params = new BasicHttpParams();

		// Use generous timeouts for slow mobile networks
		HttpConnectionParams.setConnectionTimeout(params, 30 * SECOND_IN_MILLIS);
		HttpConnectionParams.setSoTimeout(params, 30 * SECOND_IN_MILLIS);

		HttpConnectionParams.setSocketBufferSize(params, HTTP_SOCKET_BUFFERSIZE);
		HttpProtocolParams.setContentCharset(params, CHARSET);
		HttpProtocolParams.setUserAgent(params, buildUserAgent(context));

		final DefaultHttpClient client = new DefaultHttpClient(params);

		client.addRequestInterceptor(new HttpRequestInterceptor() {
			public void process(HttpRequest request, HttpContext context) {
				// Add header to accept gzip content
				if (!request.containsHeader(HEADER_ACCEPT_ENCODING)) {
					request.addHeader(HEADER_ACCEPT_ENCODING, ENCODING_GZIP);
				}
			}
		});

		client.addResponseInterceptor(new HttpResponseInterceptor() {
			public void process(HttpResponse response, HttpContext context) {
				// Inflate any responses compressed with gzip
				final HttpEntity entity = response.getEntity();
				final Header encoding = entity.getContentEncoding();
				if (encoding != null) {
					for (HeaderElement element : encoding.getElements()) {
						if (element.getName().equalsIgnoreCase(ENCODING_GZIP)) {
							response.setEntity(new InflatingEntity(response.getEntity()));
							break;
						}
					}
				}
			}
		});

		Log.d(TAG, "created a new http client in Snotify");
		return client;
	}
	
	/**
	 * Build and return a user-agent string that can identify this application
	 * to remote servers. Contains the package name and version code.
	 */
	private static String buildUserAgent(Context context) {
		try {
			final PackageManager manager = context.getPackageManager();
			final PackageInfo info = manager.getPackageInfo(context.getPackageName(), 0);

			// Some APIs require "(gzip)" in the user-agent string.
			return info.packageName + "/" + info.versionName + " (" + info.versionCode + ") (gzip)";
		} catch (NameNotFoundException e) {
			return null;
		}
	}
	
	/**
	 * Simple {@link HttpEntityWrapper} that inflates the wrapped
	 * {@link HttpEntity} by passing it through {@link GZIPInputStream}.
	 */
	private static class InflatingEntity extends HttpEntityWrapper {
		public InflatingEntity(HttpEntity wrapped) {
			super(wrapped);
		}

		@Override
		public InputStream getContent() throws IOException {
			return new GZIPInputStream(wrappedEntity.getContent());
		}

		@Override
		public long getContentLength() {
			return -1;
		}
	}
}
