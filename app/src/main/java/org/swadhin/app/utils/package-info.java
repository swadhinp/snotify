/**
 * Provides basic utility methods used throughout the source code. 
 * <p>  
 *
 *@author Abhinav Parate
 *
 */
package org.swadhin.app.utils;