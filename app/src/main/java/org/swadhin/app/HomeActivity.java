/**
 * 
 */
package org.swadhin.app;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.service.notification.StatusBarNotification;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NotificationCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import org.swadhin.app.fragments.AppCategoryFragment;
import org.swadhin.app.fragments.AppUsageTabsFragment;
import org.swadhin.app.fragments.InboxFragment;
import org.swadhin.app.fragments.NotificationTabsFragment;
import org.swadhin.app.service.UtilityService;
import org.swadhin.app.uiitems.NavigationDrawerItem;
import org.swadhin.app.uiitems.NavigationDrawerListAdapter;
import org.swadhin.sensors.appsensor.AppSensorService;
import org.swadhin.sensors.appsensor.NotificationObserver;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

/**
 * Main activity that manages various components/fragments displayed in the app.
 * 
 * <p/> This activity creates a navigation drawer and populates fragments 
 * based on the navigation item that is selected.
 * 
 * <p/>By default, fragment for the first navigation item will be displayed.
 * 
 * 0054 11 4781 3448
 * @author Abhinav Parate
 * 
 */

//http://www.iconarchive.com/show/pretty-office-8-icons-by-custom-icon-design/Bar-chart-icon.html
//http://devlup.com/designs/colorful-androids-iconset-for-download/685/
public class HomeActivity extends FragmentActivity {


	/* UI Components */
	private DrawerLayout mDrawerLayout;
	private LinearLayout mDrawerNavLayout;
	private ListView mDrawerList;
	private ActionBarDrawerToggle mDrawerToggle;
	private Switch mSensorSwitch;
    private Handler handler = null;

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
      /* do what you need to do */
            HashMap<String,ArrayList<StatusBarNotification>> nmap = NotificationObserver.nmaps;
            Set<String> keys = nmap.keySet();
            Iterator<String> it = keys.iterator();

            int num = 0;
            for ( ; it.hasNext() ; ++num ) {
                it.next();
            }

            if( num > 0 ){
                Intent intent = new Intent(getApplicationContext(), HomeActivity.class);

                PendingIntent pIntent = PendingIntent.getActivity(getApplicationContext(), 0, intent, 0);
                Uri uri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

                // Build notification
                // Actions are just fake
                Notification noti = new NotificationCompat.Builder(getApplicationContext())
                        .setContentTitle("Snotify")
                        .setContentText("Please Fill up Notification Survey").setSmallIcon(R.drawable.ic_launcher)
                        .setSound(uri)
                        .setTicker("Snotify")
                        .setContentIntent(pIntent).build();

                NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(getApplicationContext().NOTIFICATION_SERVICE);

                // hide the notification after its selected
                noti.flags |= Notification.FLAG_AUTO_CANCEL;

                notificationManager.notify(1, noti);

            }
      /* and here comes the "trick" */

                handler.postDelayed(this, 7200000); //Every 2 Hours

        }
    };


	// used to store app title
	//private CharSequence mTitle;

	// slide menu items
	private String[] navMenuTitles;
	private String[] navMenuTags;;
	private TypedArray navMenuIcons;

	private ArrayList<NavigationDrawerItem> navDrawerItems;
	private NavigationDrawerListAdapter adapter;
	private static final int VIEW_ID = 0x250;

    protected static boolean isMainShown = false;

	/**
	 * Initialization upon creation of the activity
	 * @see android.support.v4.app.FragmentActivity#onCreate(android.os.Bundle)
	 */
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_home);


		//mTitle =  getTitle();

		// load slide menu items
		navMenuTitles = getResources().getStringArray(R.array.navigation_drawer_items);

		// load slide menu tags
		navMenuTags = getResources().getStringArray(R.array.navigation_drawer_tags);

		// nav drawer icons from resources
		navMenuIcons = getResources()
				.obtainTypedArray(R.array.navigation_drawer_icons);

		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerList = (ListView) findViewById(R.id.left_drawer);
		mDrawerNavLayout = (LinearLayout) findViewById(R.id.nav_drawer_layout);


		TextView headerView = new TextView(getApplicationContext());
		headerView.setText("STATS AND SETTINGS");

		mDrawerList.addHeaderView(headerView);

		navDrawerItems = new ArrayList<NavigationDrawerItem>();

		for(int i=0;i<navMenuTitles.length;i++)
			navDrawerItems.add(new NavigationDrawerItem(navMenuTitles[i], navMenuTags[i], navMenuIcons.getResourceId(i, -1)));

		// Recycle the typed array
		navMenuIcons.recycle();

		mDrawerList.setOnItemClickListener(new SlideMenuClickListener());


		// set a custom shadow that overlays the main content when the drawer opens
		mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
		// setting the nav drawer list adapter
		adapter = new NavigationDrawerListAdapter(getApplicationContext(),
				navDrawerItems);
		mDrawerList.setAdapter(adapter);

		//Set up persistent Toolbar
		Toolbar toolbar = (Toolbar)findViewById(R.id.my_toolbar);

		//Create a layout view to add toolbar elements
		RelativeLayout layout = new RelativeLayout( this);
		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT,
				LayoutParams.WRAP_CONTENT);
		params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);

		//Set sensor switch in the toolbar
		mSensorSwitch = new Switch(getApplicationContext());
		mSensorSwitch.setId(VIEW_ID);
		layout.addView(mSensorSwitch,params);

		//Add logo to the toolbar
		ImageView logo = new ImageView(this);
		logo.setAdjustViewBounds(true);
		logo.setMaxHeight(72);
		logo.setImageDrawable(getResources().getDrawable(R.drawable.ic_apps));
		//view.setPadding(5, 5, 5, 5);
		logo.setId(VIEW_ID+1);
		layout.addView(logo);

		params = (RelativeLayout.LayoutParams)logo.getLayoutParams();
		params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);

		//Add title to the toolbar
		TextView title = new TextView(this);
		title.setText(R.string.title_appsensor);
		title.setTextColor(Color.WHITE);
		title.setTextAppearance(getApplicationContext(), R.style.SplashFontTitle);
		title.setPadding(5, 5, 5, 5);
		layout.addView(title);
		params = (RelativeLayout.LayoutParams)title.getLayoutParams();
		params.addRule(RelativeLayout.RIGHT_OF,logo.getId());

		toolbar.addView(layout);

		//Add navigation option in the toolbar
		toolbar.setNavigationIcon(R.drawable.ic_drawer);

		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
				//R.drawable.ic_drawer, //nav menu toggle icon
				toolbar,
				R.string.app_name, // nav drawer open - description for accessibility
				R.string.app_name // nav drawer close - description for accessibility
				) {
			public void onDrawerClosed(View view) {
				//getActionBar().setTitle(mTitle);
				//getActionBar().setDisplayShowHomeEnabled(true);
				//getActionBar().setDisplayShowTitleEnabled(true);
				// calling onPrepareOptionsMenu() to show action bar icons
				invalidateOptionsMenu();
			}

			public void onDrawerOpened(View drawerView) {
				//getActionBar().setTitle(mDrawerTitle);
				//getActionBar().setDisplayShowHomeEnabled(false);
				//getActionBar().setDisplayShowTitleEnabled(false);
				// calling onPrepareOptionsMenu() to hide action bar icons
				invalidateOptionsMenu();
			}
		};
		mDrawerLayout.setDrawerListener(mDrawerToggle);

		
		final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
		final Resources rcs = getResources();
		
		//Set switch on or off
		boolean isSensorEnabled = prefs.getBoolean(rcs.getString(R.string.pref_appsensor_active), true);
		mSensorSwitch.setChecked(isSensorEnabled);

		//Set listener
		mSensorSwitch.setOnCheckedChangeListener(new OnCheckedChangeListener(){

			@Override
			public void onCheckedChanged(CompoundButton arg0, boolean isChecked) {
				SharedPreferences.Editor editor = prefs.edit();
				editor.putBoolean(rcs.getString(R.string.pref_appsensor_active),isChecked);
				editor.apply();
				Toast.makeText(getApplicationContext(), "Your app usage tracking has been "+(isChecked?"enabled":"disabled")+" now", Toast.LENGTH_SHORT).show();
				AppSensorService.startByIntent(getBaseContext());
			}});	

		if (savedInstanceState == null) {
			// on first time display first statistics
			displayView(0);
		}

		/* Google Play Version
        if( null == handler) {
            handler = new Handler();
            handler.postDelayed(runnable, 7200000); //Every 2 Hours ask Users (Experience Sampling Method)
        }*/

		//Start utility service for the situation it was not booted up. 
		Intent serviceIntent = new Intent(this,UtilityService.class);
		startService(serviceIntent);

        Intent serviceIntent1 = new Intent(this,NotificationObserver.class);
        startService(serviceIntent1);

        //Intent serviceIntent2 = new Intent(this,AppSensorService.class);
        //startService(serviceIntent2);

	}

	@Override
	protected void onStart(){
		super.onStart();
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
		Resources rcs = getResources();

		//Set sensor switch on or off. May have changed its value in other activities.
		boolean isSensorEnabled = prefs.getBoolean(rcs.getString(R.string.pref_appsensor_active), false);
		mSensorSwitch.setChecked(isSensorEnabled);

		//Show preferences if launched for the first time
		boolean firstTimeUse = prefs.getBoolean(rcs.getString(R.string.pref_firsttimeuser), true);
		if(firstTimeUse)
		{
			Intent activityIntent = new Intent(this,IntroActivity.class);
			startActivity(activityIntent);
		}

		//Start utility service for the situation it was not booted up. 
		Intent serviceIntent = new Intent(this,UtilityService.class);
		startService(serviceIntent);

        Intent serviceIntent1 = new Intent(this,NotificationObserver.class);
        startService(serviceIntent1);

        //Intent serviceIntent2 = new Intent(this,AppSensorService.class);
        //startService(serviceIntent2);
	}




	/**
	 * Slide menu item click listener
	 * */
	private class SlideMenuClickListener implements
	ListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			// display view for selected nav drawer item
			displayView(position);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu items for use in the action bar
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main_menu, menu);
		return super.onCreateOptionsMenu(menu);
	}


	public boolean onOptionsItemSelected(MenuItem item) {
		if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}

		switch (item.getItemId()) {
		case R.id.action_settings:
		{
			Toast.makeText(getApplicationContext(), "Settings", Toast.LENGTH_SHORT).show();
			return true;
		}
		case R.id.action_help:
		{
			Toast.makeText(getApplicationContext(), "Help", Toast.LENGTH_SHORT).show();
			return true;
		}

		default:
			return super.onOptionsItemSelected(item);
		}
	}


	/* 
	 * Called when invalidateOptionsMenu() is triggered
	 **/

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		return super.onPrepareOptionsMenu(menu);
	}

	/* (non-Javadoc)
	 * @see android.support.v4.app.FragmentActivity#onResume()
	 */
	@Override
	protected void onResume() {
		super.onResume();

        //Start utility service for the situation it was not booted up.
        Intent serviceIntent = new Intent(this,UtilityService.class);
        startService(serviceIntent);

        Intent serviceIntent1 = new Intent(this,NotificationObserver.class);
        startService(serviceIntent1);

        //Intent serviceIntent2 = new Intent(this,AppSensorService.class);
        //startService(serviceIntent2);
	}


	/** 
	 * Deinit when activity stops
	 * @see android.support.v4.app.FragmentActivity#onStop()
	 */
	@Override
	protected void onStop() {

        Intent intent = new Intent(getBaseContext(),UtilityService.class);
        this.stopService(intent);

        intent = new Intent(getBaseContext(),AppSensorService.class);
        this.stopService(intent);

        //intent = new Intent(getBaseContext(),NotificationObserver.class);
        //stopService(intent);
        super.onStop();
	}

    @Override
    protected void onPause(){
        Intent intent = new Intent(getBaseContext(),UtilityService.class);
        this.stopService(intent);

        //intent = new Intent(getBaseContext(),AppSensorService.class);
        //this.stopService(intent);

        //intent = new Intent(getBaseContext(),NotificationObserver.class);
        //stopService(intent);
        super.onPause();


    }
	/** 
	 * Unbinds from sensor service on destruction.
	 * @see android.support.v4.app.FragmentActivity#onDestroy()
	 */
	@Override
	protected void onDestroy() {

        Intent intent = new Intent(getBaseContext(),UtilityService.class);
        this.stopService(intent);

        intent = new Intent(getBaseContext(),AppSensorService.class);
        this.stopService(intent);

        //intent = new Intent(getBaseContext(),NotificationObserver.class);
        //stopService(intent);
        super.onDestroy();
		//doUnbindService();
	}

	/**
	 * When using the ActionBarDrawerToggle, you must call it during
	 * onPostCreate() and onConfigurationChanged()...
	 */

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		//mDrawerToggle.syncState();
	}

	/** When using the ActionBarDrawerToggle, you must call it during  
	 * @see android.support.v4.app.FragmentActivity#onConfigurationChanged(android.content.res.Configuration)
	 */
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		// Pass any configuration change to the drawer toggls
		//mDrawerToggle.onConfigurationChanged(newConfig);
	}


	/**
	 * Diplaying fragment view for selected nav drawer list item
	 * */
	private void displayView(int position) {
		// update the main content by replacing fragments
		Fragment fragment = null;
        String tag_fragment = "main";
        isMainShown = false;
		//First position taken by header
		switch (position) {
		case UiConstants.UI_HOME:
			fragment = new InboxFragment();
            isMainShown = true;
            tag_fragment = "HOME";
			break;
		case UiConstants.UI_APPSENSOR:
			fragment = new AppUsageTabsFragment();
            //isMainShown = false;
            tag_fragment = "APP";
			break;
		case UiConstants.UI_NOTIFICATION:
			fragment = new NotificationTabsFragment();
            //isMainShown = false;
            tag_fragment = "NOTIF";
			break;
		case UiConstants.UI_MYAPPS:
			fragment = new AppCategoryFragment();
            //isMainShown = false;
            tag_fragment = "APPCAT";
			break;
		case UiConstants.UI_SETTINGS:
			Intent intent = new Intent(this,SettingsActivity.class);
            tag_fragment = "SETTING";
            //isMainShown = false;
			startActivity(intent);
			//Toast.makeText(getApplicationContext(), "Settings not supported yet", Toast.LENGTH_SHORT).show();
			break;
		//case UiConstants.UI_DELETE_ACCOUNT:
		//	deleteAccount();
		//	return;
		default:
            //isMainShown = false;
			break;
		}

		if (fragment != null) {

			FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
			//SlidingTabsColorsFragment fragment = new SlidingTabsColorsFragment();
			transaction.replace(R.id.stats_fragment, fragment, tag_fragment);
            transaction.addToBackStack(tag_fragment);
			transaction.commit();

			// update selected item and title, then close the drawer
			mDrawerList.setItemChecked(position, true);
			mDrawerList.setSelection(position);
			mDrawerLayout.closeDrawer(mDrawerNavLayout);
		} 
		//setTitle(navMenuTitles[position]);
		mDrawerLayout.closeDrawer(mDrawerNavLayout);
	}



	@Override
	public void onBackPressed(){

		android.support.v4.app.FragmentManager fm = getSupportFragmentManager();
		if (fm.getBackStackEntryCount() > 0) {
			Log.i("MainActivity", "popping backstack");
            if(isMainShown) {
                finish();
            }else
            {
                fm.popBackStack();
            }
		} else {
			Log.i("MainActivity", "nothing on backstack, calling super");
            if(isMainShown) {
                //Intent intent = new Intent(getBaseContext(),UtilityService.class);
                //this.stopService(intent);

                //intent = new Intent(getBaseContext(),AppSensorService.class);
                //this.stopService(intent);

                //intent = new Intent(getBaseContext(),NotificationObserver.class);
                //stopService(intent);
                finish();
            }else {
                super.onBackPressed();
            }
		}
	}



/*
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            recreate();
        }
        return super.onKeyDown(keyCode, event);
    }*/
	

	private void shutdownServices()
	{
		final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
		final Resources rcs = getResources();
		SharedPreferences.Editor editor = prefs.edit();
		editor.putBoolean(rcs.getString(R.string.pref_firsttimeuser), true);
		editor.putBoolean(rcs.getString(R.string.pref_appsensor_active), false);
		editor.putBoolean(rcs.getString(R.string.pref_enable_logging), false);
		editor.apply();
		AppSensorService.startByIntent(getBaseContext());

		Intent intent = new Intent(getBaseContext(),UtilityService.class);
		this.stopService(intent);

		intent = new Intent(getBaseContext(),AppSensorService.class);
		this.stopService(intent);

		//intent = new Intent(getBaseContext(),NotificationObserver.class);
		//stopService(intent);

	}


	/**
	 * Sign-out from google
	 * */
	private void deleteAccount() {
		shutdownServices();
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
		Resources rcs = getResources();
		SharedPreferences.Editor editor = prefs.edit();
		editor.putBoolean(rcs.getString(R.string.pref_userloggedin), false);
		editor.apply();
		
		Intent i = new Intent(HomeActivity.this, SplashScreen.class);
		startActivity(i);

		// close this activity
		finish();
	}







}
