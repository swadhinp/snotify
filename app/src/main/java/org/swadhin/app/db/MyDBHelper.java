package org.swadhin.app.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

import org.swadhin.app.utils.Utils;
import org.swadhin.sensors.appsensor.db.AppCategoryDAO;
import org.swadhin.sensors.appsensor.db.AppUsageDAO;
import org.swadhin.sensors.appsensor.db.NotificationDAO;

/**
 * This is the basic component to create and manage the database. 
 * This class should only be used in the {@link GeneralDAO}.
 * 
 * @author Matthias Boehmer, matthias.boehmer@dfki.de
 * @author Abhinav Parate
 * 
 */
public class MyDBHelper extends SQLiteOpenHelper {

	private static final int DATABASE_VERSION = 1;
	private static final String DATABASE_NAME = "utility";
	private Context context = null;
	
	public MyDBHelper(Context context) {
		this(context, DATABASE_NAME, null, DATABASE_VERSION);
		this.context = context;
	}

	public MyDBHelper(Context context, String name, CursorFactory factory, int version) {
		super(context, name, factory, version);
		//Utils.d("database created");
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(FilesDAO.TABLE_CREATE);
		Utils.d("table " + FilesDAO.TABLE_NAME + " was created");

		AppUsageDAO dao = new AppUsageDAO(AppUsageDAO.TABLE_TYPE_DAILY, context);
		db.execSQL(dao.TABLE_CREATE);
		Utils.d("table " + dao.TABLE_NAME + " was created");

		dao = new AppUsageDAO(AppUsageDAO.TABLE_TYPE_WEEKLY, context);
		db.execSQL(dao.TABLE_CREATE);
		Utils.d("table " + dao.TABLE_NAME + " was created");

		db.execSQL(AppCategoryDAO.TABLE_CREATE);
		Utils.d("table " + AppCategoryDAO.TABLE_NAME + " was created");
		
		db.execSQL(NotificationDAO.TABLE_CREATE);
		Utils.d("table " + NotificationDAO.TABLE_NAME+ " was created");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

		//==================UPGRADING DATABASE===========================
		int upgradeTo = oldVersion + 1;
		while(upgradeTo <= newVersion)
		{
			switch(upgradeTo)
			{
			case 2:{
				/*Utils.d("===============UPGRADING DATABASE====================");
				db.execSQL(AppCategoryDAO.TABLE_CREATE);
				Utils.d("table " + AppCategoryDAO.TABLE_NAME + " was created");
				
				db.execSQL(NotificationDAO.TABLE_CREATE);
				Utils.d("table " + NotificationDAO.TABLE_NAME+ " was created");
				break;*/
			}
			}
			upgradeTo++;
		}


	}

}
