/**
 * Provides the core sqlite database implementation used by the app. 
 * 
 * <p/>We thank Matthias Boehmer for the core DB access functions.  
 *
 *@author Abhinav Parate
 *
 */
package org.swadhin.app.db;