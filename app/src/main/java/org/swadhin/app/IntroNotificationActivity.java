package org.swadhin.app;

import org.swadhin.sensors.appsensor.AppSensorService;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class IntroNotificationActivity extends Activity{

	private static boolean accessedPermission = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_intro);

		TextView desc = (TextView)findViewById(R.id.desc_view1);
		desc.setText(R.string.notif_desc1);

		TextView desc2 = (TextView)findViewById(R.id.desc_view2);
		desc2.setText(R.string.notif_desc2);

		Button permissionButton = (Button)findViewById(R.id.settings_button);
		permissionButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
				final Resources rcs = getResources();
				SharedPreferences.Editor editor = prefs.edit();
				editor.putBoolean(rcs.getString(R.string.pref_appsensor_active),true);
				editor.apply();
				AppSensorService.startByIntent(getBaseContext());
				Toast.makeText(getApplicationContext(), "Please check the permission to monitor your notifications", Toast.LENGTH_LONG).show();
				// in this situation we know that the user has not granted the app the Notification access permission
				Intent intent = new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS");
				startActivity(intent);
				accessedPermission = true;

			}
		});

		Button nextButton = (Button)findViewById(R.id.next_button);
		nextButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(getApplicationContext(),IntroResearchActivity.class);
				startActivity(intent);

			}
		});
	}

	@Override
	protected void onResume () {
		super.onResume();
		if(accessedPermission)
		{
			Intent intent = new Intent(getApplicationContext(),IntroResearchActivity.class);
			startActivity(intent);
			accessedPermission = false;
		}
	}
}
