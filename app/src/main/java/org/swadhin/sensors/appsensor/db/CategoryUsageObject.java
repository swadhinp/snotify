package org.swadhin.sensors.appsensor.db;

public class CategoryUsageObject {

	public String category = null;
	public long usageTime = 0;
	
	public CategoryUsageObject(String category, long usageTime) {
		this.category = category;
		this.usageTime = usageTime;
	}
	
	public CategoryUsageObject() {
		
	}
}
