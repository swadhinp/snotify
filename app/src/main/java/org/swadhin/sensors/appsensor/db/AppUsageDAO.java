package org.swadhin.sensors.appsensor.db;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.LinkedList;

import org.swadhin.app.db.GeneralDAO;
import org.swadhin.app.utils.Utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

/**
 * Data access object for app usage events
 * 
 * <p/>This class provides access methods to the `App Usage' database that
 * tracks app usage for the week and for each day.
 * 
 * @author Abhinav Parate
 */
public class AppUsageDAO extends GeneralDAO {

	// --------------------------------------------
	// SCHEMA
	// --------------------------------------------

	public static final String TABLE_NAME_DAILY = "appusagedaily";
	public static final String TABLE_NAME_WEEKLY = "appusageweekly";
	public String TABLE_NAME = TABLE_NAME_DAILY;
	
	public static final int TABLE_TYPE_DAILY = 1;
	public static final int TABLE_TYPE_WEEKLY = 2;

	public static final String CNAME_APP = "app";
	public static final String CNAME_DAY_OR_WEEK = "period";
	public static final String CNAME_USAGE_COUNT = "usage_count";
	public static final String CNAME_USAGE_TIME = "usage_time";

	public static final String[] PROJECTION = {
		CNAME_APP,
		CNAME_DAY_OR_WEEK,
		CNAME_USAGE_COUNT,
		CNAME_USAGE_TIME
	};

	public final static int CNUM_APP = 0;
	public final static int CNUM_DAY_OR_WEEK = 1;
	public final static int CNUM_USAGE_COUNT = 2;
	public final static int CNUM_USAGE_TIME = 3;


	public String TABLE_CREATE = "CREATE TABLE " + TABLE_NAME + " (" +
			CNAME_APP + " TEXT, " +
			CNAME_DAY_OR_WEEK+" INTEGER, " +
			CNAME_USAGE_COUNT+" INTEGER, " +
			CNAME_USAGE_TIME + " LONG " +
			");";

	// --------------------------------------------
	// QUERIES
	// --------------------------------------------

	//private final static String WHERE_APP = CNAME_APP + "=?";
	private final static String WHERE_PERIOD = CNAME_DAY_OR_WEEK + "=?";
	private final static String WHERE_APP_AND_PERIOD = CNAME_APP + "=? AND "+CNAME_DAY_OR_WEEK + "=?";
	
	
	// --------------------------------------------
	// LIVECYCLE
	// --------------------------------------------

	public AppUsageDAO(int TABLE_TYPE, Context context) {
		super(context);
		if(TABLE_TYPE==TABLE_TYPE_DAILY)
			TABLE_NAME = TABLE_NAME_DAILY;
		if(TABLE_TYPE==TABLE_TYPE_WEEKLY)
			TABLE_NAME = TABLE_NAME_WEEKLY;
		TABLE_CREATE = "CREATE TABLE " + TABLE_NAME + " (" +
				CNAME_APP + " TEXT, " +
				CNAME_DAY_OR_WEEK+" INTEGER, " +
				CNAME_USAGE_COUNT+" INTEGER, " +
				CNAME_USAGE_TIME + " LONG " +
				");";
		
	}

	// --------------------------------------------
	// CRUD
	// --------------------------------------------

	public Cursor find(String app, int period) {
		Cursor c = db.query(
				TABLE_NAME, 
				PROJECTION, 
				WHERE_APP_AND_PERIOD, 
				new String[]{app, period+""}, 
				null, 
				null, 
				null);
		return c;
	}
	
	public AppUsageObject[] getAppUsageObjectsForLastWeek(String app)
	{
		if(TABLE_NAME.equals(TABLE_NAME_WEEKLY))
			return null;
		Calendar gc = GregorianCalendar.getInstance();
		int DAYS_IN_WEEK = 7;
		gc.add(Calendar.DAY_OF_YEAR, -1*(DAYS_IN_WEEK));
		AppUsageObject out[] = new AppUsageObject[DAYS_IN_WEEK];
		for(int i=0; i<DAYS_IN_WEEK; i++ )
		{
			gc.add(Calendar.DAY_OF_YEAR, 1);
			int DAY = gc.get(Calendar.DAY_OF_YEAR);
			AppUsageObject au = cursor2appusage(find(app,DAY));
			if(au==null) au = new AppUsageObject(app, DAY, 0, 0);
			out[i] = au;
		}
		return out;
	}
	
	
	public AppUsageObject[] getAllAppUsageObjects(int period) {
		Cursor c = db.query(
				TABLE_NAME, 
				PROJECTION, 
				WHERE_PERIOD, 
				new String[]{period+""}, 
				null, 
				null, 
				CNAME_USAGE_TIME+" DESC");
		return cursor2appusages(c);
	}
	
	public AppUsageObject[] getAllAppUsageObjects() {
		Cursor c = db.query(
				TABLE_NAME, 
				PROJECTION, 
				null, 
				null, 
				null, 
				null, 
				CNAME_USAGE_TIME+" DESC");
		return cursor2appusages(c);
	}

	private void insert(AppUsageObject r) {
		ContentValues cv = appusage2ContentValues(r);
		db.insert(TABLE_NAME, null, cv);
	}

	

	private void update(AppUsageObject r) {
		ContentValues values = appusage2ContentValues(r);
		db.update(TABLE_NAME, values , WHERE_APP_AND_PERIOD, new String[]{r.app,r.DAY_OR_WEEK+""});
	}

	public void delete(int period) {
		//Utils.d("delete file " + r.id);
		db.delete(TABLE_NAME, WHERE_PERIOD, new String[]{period+""});
	}

	public void deleteAll() {
		Utils.d("delete all from " + TABLE_NAME);
		db.delete(TABLE_NAME, null, null);
	}
	
	public void updateStats(AppUsageObject r)
	{
		AppUsageObject fromDB = cursor2appusage(find(r.app,r.DAY_OR_WEEK));
		if(fromDB==null)
		{
			insert(r);
		} else {
			r.usageCount += fromDB.usageCount;
			r.usageTime += fromDB.usageTime;
			update(r);
		}
	}

	// --------------------------------------------
	// TRANSFORMATION
	// --------------------------------------------

	

	public static AppUsageObject cursor2appusage(Cursor c) {
		c.moveToFirst();
		if(c.isAfterLast()) return null;
		AppUsageObject r = new AppUsageObject();
		r.app = c.getString(CNUM_APP);
		r.DAY_OR_WEEK = c.getInt(CNUM_DAY_OR_WEEK);
		r.usageCount = c.getInt(CNUM_USAGE_COUNT);
		r.usageTime = c.getLong(CNUM_USAGE_TIME);
		return r;
	}

	public static AppUsageObject[] cursor2appusages(Cursor c) {
		c.moveToFirst();
		LinkedList<AppUsageObject> appusages = new LinkedList<AppUsageObject>();
		while(!c.isAfterLast()){
			AppUsageObject r = new AppUsageObject();
			r.app = c.getString(CNUM_APP);
			r.DAY_OR_WEEK = c.getInt(CNUM_DAY_OR_WEEK);
			r.usageCount = c.getInt(CNUM_USAGE_COUNT);
			r.usageTime = c.getLong(CNUM_USAGE_TIME);
			appusages.add(r);
			c.moveToNext();
		}
		return appusages.toArray(new AppUsageObject[0]);
	}

	private static ContentValues appusage2ContentValues(AppUsageObject r) {
		ContentValues cv = new ContentValues();
		cv.put(CNAME_APP, r.app);
		cv.put(CNAME_DAY_OR_WEEK, r.DAY_OR_WEEK);
		cv.put(CNAME_USAGE_COUNT, r.usageCount);
		cv.put(CNAME_USAGE_TIME, r.usageTime);
		return cv;
	}

	


}
