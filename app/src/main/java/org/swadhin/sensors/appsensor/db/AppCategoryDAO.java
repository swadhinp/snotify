package org.swadhin.sensors.appsensor.db;

import java.util.HashMap;
import java.util.LinkedList;

import org.swadhin.app.db.GeneralDAO;
import org.swadhin.app.utils.Utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

/**
 * Data access object for app usage events
 * 
 * <p/>This class provides access methods to the `App Usage' database that
 * tracks app usage for the week and for each day.
 * 
 * @author Abhinav Parate
 */
public class AppCategoryDAO extends GeneralDAO {

	// --------------------------------------------
	// SCHEMA
	// --------------------------------------------

	public static final String TABLE_NAME = "appcategory";


	public static final String CNAME_APP = "app";
	public static final String CNAME_CATEGORY = "category";

	public static final String[] PROJECTION = {
		CNAME_APP,
		CNAME_CATEGORY
	};

	public final static int CNUM_APP = 0;
	public final static int CNUM_CATEGORY = 1;


	public final static String TABLE_CREATE = "CREATE TABLE " + TABLE_NAME + " (" +
			CNAME_APP + " TEXT PRIMARY KEY, " +
			CNAME_CATEGORY+" TEXT " +
			");";

	// --------------------------------------------
	// QUERIES
	// --------------------------------------------

	private final static String WHERE_APP = CNAME_APP + "=?";



	// --------------------------------------------
	// LIVECYCLE
	// --------------------------------------------

	public AppCategoryDAO(Context context) {
		super(context);
	}

	// --------------------------------------------
	// CRUD
	// --------------------------------------------

	public Cursor find(String app) {
		Cursor c = db.query(
				TABLE_NAME, 
				PROJECTION, 
				WHERE_APP, 
				new String[]{app}, 
				null, 
				null, 
				null);
		return c;
	}
	
	public HashMap<String,String> getAppCategoryPairs()
	{
		Cursor c = db.query(
				TABLE_NAME, 
				PROJECTION, 
				null, 
				null, 
				null, 
				null, 
				null);
		c.moveToFirst();
		HashMap<String,String> map = new HashMap<String,String>();
		while(!c.isAfterLast()){
			map.put(c.getString(CNUM_APP),c.getString(CNUM_CATEGORY));
			c.moveToNext();
		}
		return map;
	}


	public String getAppCategory(String app) {
		Cursor c = find(app);
		return cursor2category(c);
	}

	public CategoryUsageObject[] getCategoryUsage(String appUsageTable,int PERIOD) {
		String CATEGORY_USAGE_QUERY = "SELECT C."+CNAME_CATEGORY+", SUM(A."+AppUsageDAO.CNAME_USAGE_TIME+") AS duration"+
				" FROM "+TABLE_NAME+" C, "+appUsageTable+" A "+
				" WHERE C."+CNAME_APP+"=A."+AppUsageDAO.CNAME_APP+" "+
				" AND A."+AppUsageDAO.CNAME_DAY_OR_WEEK+"="+PERIOD+" "+
				" GROUP BY(C."+CNAME_CATEGORY+") "+
				" ORDER BY duration DESC";
		//Utils.d(CATEGORY_USAGE_QUERY);
		Cursor c = db.rawQuery(CATEGORY_USAGE_QUERY, null);
		return cursor2categoryusageobjects(c);
	}

	public boolean insert(String app, String category) {
		ContentValues cv = new ContentValues();
		cv.put(CNAME_APP, app);
		cv.put(CNAME_CATEGORY, category);
		try{
			long row = db.insert(TABLE_NAME, null, cv);
			return (row!=-1);
		} catch(Exception e)
		{
			return false;
		}
	}



	public void update(String app, String category) {
		ContentValues cv = new ContentValues();
		cv.put(CNAME_APP, app);
		cv.put(CNAME_CATEGORY, category);
		db.update(TABLE_NAME, cv , WHERE_APP, new String[]{app});
	}

	public void delete(String app) {
		//Utils.d("delete file " + r.id);
		db.delete(TABLE_NAME, WHERE_APP, new String[]{app});
	}

	public void deleteAll() {
		Utils.d("delete all from " + TABLE_NAME);
		db.delete(TABLE_NAME, null, null);
	}

	// --------------------------------------------
	// TRANSFORMATION
	// --------------------------------------------


	private static String cursor2category(Cursor c) {
		c.moveToFirst();
		while(!c.isAfterLast()){
			return c.getString(CNUM_CATEGORY);
		}
		return null;
	}

	private static CategoryUsageObject[] cursor2categoryusageobjects(Cursor c)
	{
		c.moveToFirst();
		LinkedList<CategoryUsageObject> categoryusages = new LinkedList<CategoryUsageObject>();
		while(!c.isAfterLast()){
			CategoryUsageObject r = new CategoryUsageObject();
			r.category = c.getString(0);
			r.usageTime = c.getLong(1);
			categoryusages.add(r);
			c.moveToNext();
		}
		return categoryusages.toArray(new CategoryUsageObject[0]);
	}

}
