/**
 * 
 */
package org.swadhin.sensors.appsensor;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import android.util.Log;
import android.widget.RemoteViews;

import org.swadhin.app.HomeActivity;
import org.swadhin.app.R;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/**
 * This component observes the various notifications received and cleared by the user. The
 * notification events are logged along with the posting/clearing time and the package/app that
 * posted the notification.
 * 
 * @author Abhinav Parate
 * 
 */
public class NotificationObserver extends NotificationListenerService{

	public static ArrayList<StatusBarNotification> notifications = new ArrayList<StatusBarNotification>();
	public static HashMap<String,ArrayList<StatusBarNotification>> nmaps = new HashMap<String,ArrayList<StatusBarNotification>>();
	public static ArrayList<Integer> clearedList = new ArrayList<Integer>();
	private static NotificationObserver context = null;
	private static final int NOTIFICATION_ID = 0x2376;

	@Override
	public int onStartCommand(Intent intent,int flags, int startId){
		super.onStartCommand(intent, flags, startId);
		context = this;
		return START_STICKY;
	}

	/* (non-Javadoc)
	 * @see android.service.notification.NotificationListenerService#onNotificationPosted(android.service.notification.StatusBarNotification)
	 */
	@TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
	public void onNotificationPosted(StatusBarNotification notification) {
		context = this;
		AppUsageEvent aue = new AppUsageEvent();
		aue.taskID = notification.getId();
		aue.packageName = notification.getPackageName();
		aue.eventtype = AppUsageEvent.EVENT_NOTIFICATION_ON;
		aue.starttime = notification.getPostTime(); //Time when it was posted
		aue.runtime = 0; // since we don't know when it ends
		aue.longitude = LocationObserver.longitude;
		aue.latitude = LocationObserver.latitude;
		aue.accuracy = LocationObserver.accuracy;
		aue.powerstate = HardwareObserver.powerstate;
		aue.wifistate = HardwareObserver.wifistate;
		aue.bluetoothstate = HardwareObserver.bluetoothstate;
		aue.headphones = HardwareObserver.headphones;
		aue.orientation = HardwareObserver.orientation;
		aue.timestampOfLastScreenOn = HardwareObserver.timestampOfLastScreenOn;

        Notification not = notification.getNotification();

        List<String> str_list = getTextString(not);
        int not_word_count = 0;

        for (String s : str_list)
        {
            not_word_count += 1;
        }

        aue.notif_style= extractStyle(not.extras.getString(Notification.EXTRA_TEMPLATE));
        aue.notif_word_count = not_word_count;

        aue.notif_tag = notification.getTag();
        if( null != not.tickerText) {
            aue.notif_tickerText = not.tickerText.toString();
        }else{
            aue.notif_tickerText = "None";
        }
        aue.notif_id = Integer.toString(notification.getId());
        aue.notif_ledRGB = Integer.toString(not.ledARGB);
        aue.notif_ledOff = Integer.toString(not.ledOffMS);
        aue.notif_ledOn = Integer.toString(not.ledOnMS);
        if( null != not.sound) {
            aue.notif_sound = not.sound.toString();
        }else{
            aue.notif_sound = "None";
        }
        if( null != not.vibrate){
        for (long l : not.vibrate) {
            aue.notif_vibrate = Long.toString(l) + ":";
        }
        }else{
            aue.notif_vibrate = "None";
        }

        aue.notif_priority = Integer.toString(not.priority);
        aue.notif_icon = Integer.toString(not.icon);
        aue.notif_clearable = Boolean.toString(notification.isClearable());
        aue.notif_ongoing = Boolean.toString(notification.isOngoing());

		notifications.add(notification);

		updateMap(notification);
		AppSensorService.logEvent(aue); //Stopping App Logging in New Version : Swadhin
		if(notification.getId()!=NOTIFICATION_ID)
			updateNotification(notification);

        //cancelNotification(notification.getPackageName(), notification.getTag(), notification.getId());
        //cancel(notification.geId());

		//Clearing the Notification from the Notification Bar
		//NotificationManager notificationManager = (NotificationManager)getSystemService(getApplicationContext().NOTIFICATION_SERVICE);
		//notificationManager.cancel(notification.getId());
	}

	/* (non-Javadoc)
	 * @see android.service.notification.NotificationListenerService#onNotificationRemoved(android.service.notification.StatusBarNotification)
	 */
	@Override
	public void onNotificationRemoved(StatusBarNotification notification) {
		context = this;
		AppUsageEvent aue = new AppUsageEvent();
		aue.taskID = notification.getId();
		aue.packageName = notification.getPackageName();
		aue.eventtype = AppUsageEvent.EVENT_NOTIFICATION_OFF;
		aue.starttime = System.currentTimeMillis(); //Time when it was cleared
		aue.runtime = aue.starttime-notification.getPostTime(); // since we just cleared it.
		aue.longitude = LocationObserver.longitude;
		aue.latitude = LocationObserver.latitude;
		aue.accuracy = LocationObserver.accuracy;
		aue.powerstate = HardwareObserver.powerstate;
		aue.wifistate = HardwareObserver.wifistate;
		aue.bluetoothstate = HardwareObserver.bluetoothstate;
		aue.headphones = HardwareObserver.headphones;
		aue.orientation = HardwareObserver.orientation;
		aue.timestampOfLastScreenOn = HardwareObserver.timestampOfLastScreenOn;

        /*
		if(notifications.size()>20)
		{
			for(StatusBarNotification iNotification: notifications)
			{
				if(iNotification.getId() == notification.getId())
				{
					notifications.remove(iNotification);
					break;
				}
			}
		}*/

		// Stop this removal before any feedback

		//removeFromMap(notification); //Swadhin : Latest addition for removal of the notification

		AppSensorService.logEvent(aue); //Stopping App Logging in New Version : Swadhin
	}

	private static void updateMap(StatusBarNotification notification)
	{
		String app = notification.getPackageName();
		ArrayList<StatusBarNotification> list = getNotificationList(app);
		int id = notification.getId();

		/*for(StatusBarNotification sn: list)
		{
			if(sn.getId()==id)
			{
				list.remove(sn);
			}
		}*/

        //For Concurrentmodification error
        for(Iterator<StatusBarNotification> it = list.iterator(); it.hasNext();)
        {
            StatusBarNotification sn = it.next();

            if(sn.getId()==id)
            {
                it.remove();
                break;
            }
        }

		list.add(notification);
		//if(notification.isClearable()  && notification.getId()!=NOTIFICATION_ID)
		//{
            //clearedList.add(id);
            // context.cancelNotification(notification.getKey());

		//}
	}

	private static void removeFromMap(StatusBarNotification notification)
	{
		String app = notification.getPackageName();
		int id = notification.getId();
		for(Integer cId: clearedList)
		{
			if(cId==id)
			{
				//This notification was cleared by us
				//Keep it in memory
				clearedList.remove(id);
				return;
			}
		}
		ArrayList<StatusBarNotification> list = getNotificationList(app);
		for(StatusBarNotification sn: list)
		{
			if(sn.getId()==id)
			{
				list.remove(sn);
				break;
			}
		}
	}

	private static ArrayList<StatusBarNotification> getNotificationList(String app)
	{
		ArrayList<StatusBarNotification> list = nmaps.get(app);
		if(list==null)
		{
			list = new ArrayList<StatusBarNotification>();
			nmaps.put(app, list);
		}
		return list;
	}

	public static NotificationObserver getInstance()
	{
		return context;
	}

	@TargetApi(Build.VERSION_CODES.KITKAT)
	private void updateNotification(StatusBarNotification tn) {

		Bundle extras = tn.getNotification().extras;
		Bitmap bm = (Bitmap)extras.get(Notification.EXTRA_PICTURE);
		
		NotificationCompat.Builder mBuilder =
				new NotificationCompat.Builder(context);
		mBuilder.setSmallIcon(R.drawable.ic_apps);
		mBuilder.setLargeIcon(bm).setStyle(new NotificationCompat.BigPictureStyle());
		Intent resultIntent = new Intent(context, HomeActivity.class);
		PendingIntent resultPendingIntent =
				PendingIntent.getActivity(
						context,
						0,
						resultIntent,
						PendingIntent.FLAG_UPDATE_CURRENT
						);
		mBuilder.setContentIntent(resultPendingIntent);
		RemoteViews mContentView = new RemoteViews(context.getPackageName(), R.layout.layout_notification);
		mBuilder.setContent(mContentView);
		Log.d("swadhin", "Updating notification");
		/*Notification notif = mBuilder.build();
		NotificationManager mNotifyMgr = 
		        (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		
		
		// Builds the notification and issues it.
		//Log.d("swadhin", "Building notification");
		mNotifyMgr.notify(NOTIFICATION_ID, notif);
		/* 
		 */
	}

    public static List<String> getTextString(Notification notification)
    {
        // We have to extract the information from the view
        RemoteViews views = (RemoteViews)notification.bigContentView;
        if (views == null) views = (RemoteViews)notification.contentView;
        if (views == null) views = (RemoteViews)notification.tickerView;
        if (views == null) return null;

        // Use reflection to examine the m_actions member of the given RemoteViews object.
        // It's not pretty, but it works.
        List<String> text = new ArrayList<String>();
        try
        {
            Field field = null;

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                //Log.e("Obs", views.getClass().getSuperclass().getSimpleName());
                field = views.getClass().getSuperclass().getDeclaredField("mActions");
            }else{
                //Log.e("Obs", views.getClass().getSimpleName());
                field = views.getClass().getDeclaredField("mActions");
            }

            field.setAccessible(true);

            @SuppressWarnings("unchecked")
            ArrayList<Parcelable> actions = (ArrayList<Parcelable>) field.get(views);

            // Find the setText() and setTime() reflection actions
            for (Parcelable p : actions)
            {
                Parcel parcel = Parcel.obtain();

                if (parcel == null) continue;
                if (p == null) continue;

                p.writeToParcel(parcel, 0);
                parcel.setDataPosition(0);

                // The tag tells which type of action it is (2 is ReflectionAction, from the source)
                int tag = parcel.readInt();
                if (tag != 2) continue;
                // View ID
                parcel.readInt();

                String methodName = parcel.readString();
                if (methodName != null)
                    Log.e("Obs", methodName);

                if (methodName == null) continue;
                    // Save strings
                else if (methodName.equals("setText"))
                {
                    // Parameter type (10 = Character Sequence)
                    parcel.readInt();

                    // Store the actual string
                    if( null != parcel) {
                        CharSequence cs = TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
                        if (null != cs) {

                            String t = cs.toString();

                            if (null != t) {
                                text.add(t.trim());
                            }
                        }
                    }
                }

                // Save times. Comment this section out if the notification time isn't important
                else if (methodName.equals("setTime"))
                {
                    // Parameter type (5 = Long)
                    parcel.readInt();

                    if( null != parcel) {
                        String t = new SimpleDateFormat("h:mm a").format(new Date(parcel.readLong()));
                        text.add(t);
                    }
                }

                parcel.recycle();

            }
        }
        // It's not usually good style to do this, but then again, neither is the use of reflection...
        catch (Exception e)
        {
            Log.e("NotificationObserver", e.toString());
        }

        return text;
    }

    private String extractStyle(String styleString)
    {
        String style;

        if(styleString!=null)
        {
            if(styleString.equals("android.app.Notification$BigTextStyle"))
            {
                style = "BigText";
            } else if(styleString.equals("android.app.Notification$BigPictureStyle"))
            {
                style = "BigPic";
            } else if(styleString.equals("android.app.Notification$InboxStyle"))
            {
                style = "Inbox";
            } else if(styleString.equals("android.app.Notification$MediaStyle"))
            {
                style = "Media";
            } else
            {
                style = "Simple";
            }
        } else {
            style = "None";
        }

        return style;
    }

}
