package org.swadhin.sensors.appsensor.db;


/**
 * An app usage object used with {@link AppUsageDAO},{@link AppUsageWeeklyDAO}.
 * 
 * @author Abhinav Parate
 */
public class NotificationObject {

	/** App Name */
	public String app = null;
	
	/** Number of times notification generated */
	public int count = 0;
	
	/**
	 * Empty Constructor
	 */
	public NotificationObject() {
		
	}

}