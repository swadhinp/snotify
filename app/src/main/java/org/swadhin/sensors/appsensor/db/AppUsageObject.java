package org.swadhin.sensors.appsensor.db;

import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * An app usage object used with {@link AppUsageDAO},{@link AppUsageWeeklyDAO}.
 * 
 * @author Abhinav Parate
 */
public class AppUsageObject {

	/** App Name */
	public String app = null;
	
	/** Indicator of which day or week this stat is about */
	public int DAY_OR_WEEK = -1;

	/** Number of times used */
	public int usageCount;

	/** Total time spent on this app */
	public long usageTime;
	
	public AppUsageObject(String app, long eventTime, long usageTime, boolean daily) {
		this.app = app;
		Calendar gc = GregorianCalendar.getInstance();
		gc.setTimeInMillis(eventTime);
		if(daily)
		{
			int PERIOD = gc.get(Calendar.DAY_OF_YEAR);
			this.DAY_OR_WEEK = PERIOD;
		} else {
			int PERIOD = gc.get(Calendar.WEEK_OF_YEAR);
			this.DAY_OR_WEEK = PERIOD;
		}
		this.usageCount = 1;
		this.usageTime = usageTime;
	}
	
	
	public AppUsageObject(String app, int DAY_OR_WEEK, int usageCount, long usageTime) {
		this.app = app;
		this.DAY_OR_WEEK = DAY_OR_WEEK;
		this.usageCount = usageCount;
		this.usageTime = usageTime;
	}
	
	
	/**
	 * Empty Constructor
	 */
	public AppUsageObject() {
		
	}

}