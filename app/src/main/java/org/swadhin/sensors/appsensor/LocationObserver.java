package org.swadhin.sensors.appsensor;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.text.format.DateUtils;

/**
 * <p>
 * This component observes the users location and always provides the actual
 * location of the user for logging. It provided values like longitude,
 * latitude, altitude, speed, etc.
 * </p>
 *
 * <p>
 * As an observer, this component just traces the information and makes it
 * available through static variables. The component is running in its own
 * thread and makes all information available through static variables.
 * </p>
 *
 * @author Matthias Boehmer, matthias.boehmer@dfki.de
 */
public class LocationObserver extends Thread implements LocationListener {

	// ---------------------------------------------
	// constants
	// ---------------------------------------------

	
	private static final float GPS_MIN_TIME_SERVICE = DateUtils.SECOND_IN_MILLIS * 600;// 10 minutes
	// sec
	private static final long GPS_MIN_MOVEMENT_SERVICE = 50; // meters

	/** value of longitude if not available */
	public final static double LONGITUDE_UNKNOWN = 0.0;

	/** value of latitude if not available */
	public final static double LATITUDE_UNKNOWN = 0.0;

	/** value of altitude if not available */
	public final static double ALTITUDE_UNKNOWN = -9.9;

	/** value of speed if no accuracy is available */
	public static final double ACCURACY_UNKNOWN = -1;

	/** value of speed if not available */
	public final static double SPEED_UNKNOWN = -1;

	/* code if country code is unknown */
	private static final String COUNTRYCODE_UNKNOWN = "--";

	// ---------------------------------------------
	// observed public values of this observer
	// ---------------------------------------------

	/** the current longitude of the user */
	public static double longitude = LONGITUDE_UNKNOWN;

	/** the current latitude of the user */
	public static double latitude = LATITUDE_UNKNOWN;

	/** the current altitude of the user */
	public static double altitude = ALTITUDE_UNKNOWN;

	/** the current speed of the user */
	public static double speed = SPEED_UNKNOWN;

	/** the accuracy of the current location */
	public static double accuracy = ACCURACY_UNKNOWN;

	/** the country code of the last location */
	public static String countryCode = COUNTRYCODE_UNKNOWN;

	/**
	 * publish the given location as public static values of the component
	 *
	 * @param l
	 */
	private static void updateLocation(Location l) {
		if (l != null) {
			longitude = l.getLongitude();
			latitude = l.getLatitude();
			if(AppSensorService.context!=null)
				AppObserver.getAppUsageLogger(AppSensorService.context).logLocationChanged();

			if (l.hasAltitude()) {
				altitude = l.getAltitude();
			} else {
				altitude = ALTITUDE_UNKNOWN;
			}

			if (l.hasSpeed()) {
				speed = l.getSpeed();
			} else {
				speed = SPEED_UNKNOWN;
			}

			if (l.hasAccuracy()) {
				accuracy = l.getAccuracy();
			} else {
				accuracy = ACCURACY_UNKNOWN;
			}
		}
	}

	// ---------------------------------------------
	// attributes
	// ---------------------------------------------

	/** access to the application context */
	private Context context;

	/** manager that gives access to the location api */
	private LocationManager locationManager;

	
	private static LocationObserver instance = null;

	// ---------------------------------------------
	// livecycle
	// ---------------------------------------------

	public LocationObserver(Context context) {
		super();
		//Utils.d( "constructed");
		this.context = context;

		// create a location manager to access location api
		locationManager = (LocationManager) this.context.getSystemService(Context.LOCATION_SERVICE);
	}
	
	
	/**
	 * get the singleton
	 * 
	 * @param c
	 * @return singleton instance
	 */
	public static LocationObserver getLocationLogger(Context c) {
		//Utils.d("getLocationLogger");
			if (instance == null) {
				instance = new LocationObserver(c);
			}

		return instance;
	}

	

	/**
	 * pause this logger, i.e. stop the thread and unregister all listeners
	 */
	public void pauseLogging() {
		//Utils.d("pause location logging");

		// unregister location listener
		locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, GPS_MIN_MOVEMENT_SERVICE, GPS_MIN_TIME_SERVICE, this);
		locationManager.removeUpdates(this);

		// send message to the worker thread
		//mHandler.sendEmptyMessageDelayed(DO_PAUSE, TIME_IMMEDIATELY);
	}

	/**
	 * start this logger, i.e. start the thread and register listeners
	 */
	public void startLogging() {
		//Utils.d("start location logging");

		// register location listener
		locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, GPS_MIN_MOVEMENT_SERVICE, GPS_MIN_TIME_SERVICE, this);

		// send message to the worker thread
		//mHandler.sendEmptyMessageDelayed(DO_START, TIME_IMMEDIATELY);
	}

	// ---------------------------------------------
	// core thread functionality
	// ---------------------------------------------


	// ---------------------------------------------
	// methods
	// ---------------------------------------------

	

	/**
	 * This method is part of the location listener and is called -- of course
	 * ;-) -- when the location changes.
	 */
	public void onLocationChanged(Location location) {
		//Utils.d( "onLocationChanged | " + location.toString());
		updateLocation(location);

	}

	public void onProviderDisabled(String provider) {
		//Utils.d( "onProviderDisabled");
	}

	public void onProviderEnabled(String provider) {
		//Utils.d( "onProviderEnabled");
	}

	public void onStatusChanged(String provider, int status, Bundle extras) {
		//Utils.d( "onStatusChanged");
	}


}

