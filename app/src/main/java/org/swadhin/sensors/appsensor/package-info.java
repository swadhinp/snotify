/**
 *  Provides classes for App Sensor operation.
 * 
 * <p>AppSensor is based on open source project of the same name by Matthias Boehmer.
 * This is a virtual sensor for measuring mobile phone use on Android platform.</p>
 * The open source project is located at <a href="https://github.com/matboehmer/appsensor">https://github.com/matboehmer/appsensor</a></p>
 * 
 * <p>The main events observed by this sensor are: app usage, app installation/uninstallation and updates.
 * </p>
 * 
 * <p><b>NEW:</b> We now have extended this sensor to log:</p>
 * <ul><li> notification events (posted/cleared).</li>
 * <li> periodic location updates.</li>
 * <li> ringer mode change events (vibrate,sound,silent).</li>
 * </ul>
 * 
 *  <p>This sensor supports many more events from the list below. For supported events list, refer
 *  to {@link org.sarayu.sensors.appsensor.AppUsageEvent}.</p>
 *  <ul>
<li>Type of event</li>
<li>Package name of the application</li>
<li>Start time of event (UTC timestamp)</li>
<li>Offset from local time zone to UTC (in hours)</li>
<li>Usage time (milliseconds)</li>
<li>Information of location API: Longitude, Latitude, Accuracy, Altitude,Speed</li>
<li>Powerstate (connected / disconnected from the charger)</li>
<li>Powerlevel (battery load in percentage)</li>
<li>Time of last screen on for sessions (UTC timestamp)</li>
<li>State of headphones (plugged in: yes / no)</li>
<li>Orientation of device (portrait / landscape)</li>
<li>WiFi state (turned off / turned on / connected)</li>
<li>Bluetooth state (turned off / turned on / connected)</li>
<li>GPS state [implementation planned]</li>
</ul>
 *
 *@author Abhinav Parate
 *
 */
package org.sarayu.sensors.appsensor;