package org.swadhin.sensors.appsensor;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.swadhin.app.R;
import org.swadhin.app.db.GeneralDAO;
import org.swadhin.app.utils.Utils;
import org.swadhin.sensors.appsensor.db.AppUsageDAO;
import org.swadhin.sensors.appsensor.db.AppUsageObject;
import org.swadhin.sensors.appsensor.db.NotificationDAO;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.app.KeyguardManager;
import android.app.usage.UsageEvents;
import android.app.usage.UsageStatsManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.media.AudioManager;
import android.os.Build;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.HandlerThread;
import android.os.Message;
import android.preference.PreferenceManager;

/**
 * <p>
 * This runnable observes a users device interaction and makes it persistent to
 * the local database. It tracks the application usage and saves events in
 * memory (in a list). At the end of a session (i.e. when the device goes into
 * standby) the data is saved to a local database on the mobile device.
 * database.
 * </p>
 * 
 * <p>
 * This component is a thread that can be controlled through the start and pause
 * methods.
 * </p>
 * 
 * @author Matthias Boehmer, matthias.boehmer@dfki.de
 */
class AppObserver extends HandlerThread {

	/*
	 * informs all listeners about application change
	 * 
	 * @param previous
	 * @param current
	 */
	/*private void fireAppChange(String previous, String current) {

		Utils.d( "fire app change: " + previous + " -> " + current);

	}*/

	public static final int LOLLIPOP_TASKID = -104;

	/** number of recent tasks that will be tracked form the API */
	private static final int MAX_TASKS = 1;

	/** period of running app usage checks (in milliseconds) */
	private static long TIME_APPCHECKINTERVAL = 500;
	private static final long TIME_IMMEDIATELY = 5;
	private static final long TIME_PREPOPULATE =  60*30*1000;

	static {
		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
		{
			TIME_APPCHECKINTERVAL = 180*1000;
		}
	}
	private static final String USAGE_STATS_SERVICE = "usagestats";

	/** states of the thread */
	private static final int DO_OBSERVE = 0;
	private static final int DO_PAUSE = 1;
	private static final int DO_START = 2;

	/** task id for the app that was open before going into standby */
	private static final int LAST_BEFORE_STANDBY = -1;

	/** package name of the phone app */
	private static final Object PHONEPACKAGENAME = "com.android.phone";

	/** number of items in table that we cache on the mobile before sending data to the server */
	public static final int MIN_INTERACTIONS_TO_SEND = 2;

	/** history of apps that have been used in current session */
	public static ArrayList<AppUsageEvent> appHistory;

	/** whether the logger should be active or not */
	public static boolean ACTIVE = true;

	/** general history of other app events */
	private ArrayList<AppUsageEvent> generalInteractionHistory;

	/** reference to the activity manager for retrieving info on current apps */
	private ActivityManager activityManager;

	/** the previous app */
	private AppUsageEvent lastApp;

	/** list of recently running apps */
	private List<RunningTaskInfo> apps = null;

	/** handler for thread messages */
	private Handler mHandler = null;

	/** number of apps that are already in our list */
	private int lengthHistory;

	/** context of the app */
	private Context context;

	/** to check if keyboard is locked */
	private KeyguardManager keyManager;

	/** special package name for logging special events not related to app usage */
	private static final String SPECIALLOGGINGPACKAGE = "-event-";

	/** semaphore for synchronized blocks, since some other threads are using this component */
	private static final Object semaphore = new Object();

	/** singleton instance of the logger */
	private static AppObserver instance = null;

	/** default home/launcher package */
	@SuppressWarnings("unused")
	private String launcherPackage = null;


	/** Hashmap to store app events in Android Lollipop */
	private static HashMap<String,Long> startTimeMap = new HashMap<String,Long>();

	/** Tracking last event check time in Android Lollipop */
	private static long lastCheckTime = System.currentTimeMillis()-3600*1000;


	/**
	 * get the singleton
	 * 
	 * @param c
	 * @return singleton instance
	 */
	public static AppObserver getAppUsageLogger(Context c) {
		synchronized (semaphore) {
			if (instance == null) {
				instance = new AppObserver(c);
				instance.start();
				instance.waitUntilReady();
			}
		}

		return instance;
	}


	/**
	 * creates a new app logger that needs to be started afterwards
	 * @param context
	 */
	private AppObserver(Context context) {
		super("AppSensorThread");
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		final Resources rcs = context.getResources();
		lastCheckTime = prefs.getLong(rcs.getString(R.string.pref_appsensor_lastchecktime),-1);
		if(lastCheckTime==-1)
		{
			//Set time to the start of the day
			Calendar gc = GregorianCalendar.getInstance();
//			gc.set(Calendar.HOUR_OF_DAY, 0);
//			gc.set(Calendar.MINUTE, 0);
//			gc.set(Calendar.SECOND, 0);
//			//gc.add(Calendar.DAY_OF_YEAR, -1);
			lastCheckTime = gc.getTimeInMillis()-TIME_PREPOPULATE;
		}

		this.context = context;
		keyManager = (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);
		appHistory = new ArrayList<AppUsageEvent>();
		generalInteractionHistory = new ArrayList<AppUsageEvent>();
		activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);

		Intent intent = new Intent(Intent.ACTION_MAIN);
		intent.addCategory(Intent.CATEGORY_HOME);
		ResolveInfo resolveInfo = context.getPackageManager().resolveActivity(intent, PackageManager.MATCH_DEFAULT_ONLY);
		launcherPackage = resolveInfo.activityInfo.packageName;

	}



	/**
	 * Analyzes the runtime of the currently used application and adds the
	 * information to the history (list of recent apps) in memory.
	 * 
	 * @param forceClosingHistory
	 */
	@SuppressLint("NewApi") @SuppressWarnings("deprecation")
	private void observeCurrentApp(boolean forceClosingHistory) {
		synchronized (appHistory) {

			//Utils.d(appHistory);
			if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {


				apps = activityManager.getRunningTasks(MAX_TASKS);

				if (apps == null) {
					Utils.d("oops, we cannot look which apps are running; API ERROR");
					return;
				}

				/** fill list with running apps */
				for (RunningTaskInfo app : apps) {

					lengthHistory = appHistory.size();

					if (lengthHistory > 0) {

						lastApp = appHistory.get(appHistory.size() - 1);

						if (app.id == lastApp.taskID) {

							/** here the previous app is still running */
							lastApp.runtime = System.currentTimeMillis() - lastApp.starttime;
							lastApp.longitude = LocationObserver.longitude;
							lastApp.latitude = LocationObserver.latitude;
							lastApp.accuracy = LocationObserver.accuracy;
							lastApp.powerstate = HardwareObserver.powerstate;

							lastApp.wifistate = HardwareObserver.wifistate;
							lastApp.bluetoothstate = HardwareObserver.bluetoothstate;
							lastApp.headphones += HardwareObserver.headphones;

							// HardwareObserver.orientation is either +1 or -1
							HardwareObserver.updateOrientation(context);
							lastApp.orientation += HardwareObserver.orientation;

							if (forceClosingHistory) {
								lastApp.taskID = LAST_BEFORE_STANDBY;
							}
						} else {
							/** here the user has opened a different app */
							if (lastApp.taskID != LAST_BEFORE_STANDBY) {
								// add runtime to the old app
								lastApp.runtime = (System.currentTimeMillis() - lastApp.starttime);
							}

							//Utils.d( lastApp.packageName + "(taskID:" + lastApp.taskID + ") " + " was running " + lastApp.runtime);
							//Utils.d( lastApp.packageName + " ran " + Math.round(lastApp.runtime/1000) + " s");

							// inform listeners about change
							String currentPackageName = app.baseActivity.getPackageName();
							//fireAppChange(lastApp.packageName, currentPackageName);

							if (!forceClosingHistory) {

								// create a usage event and fill in all the context information
								AppUsageEvent aue = new AppUsageEvent();
								aue.taskID = app.id;
								aue.packageName = currentPackageName;
								aue.eventtype = AppUsageEvent.EVENT_APPUSE;
								aue.starttime = System.currentTimeMillis();
								aue.runtime = 0; // since we just started
								aue.longitude = LocationObserver.longitude;
								aue.latitude = LocationObserver.latitude;
								aue.accuracy = LocationObserver.accuracy;
								aue.powerstate = HardwareObserver.powerstate;
								aue.wifistate = HardwareObserver.wifistate;
								aue.bluetoothstate = HardwareObserver.bluetoothstate;
								aue.headphones = HardwareObserver.headphones;
								aue.orientation = HardwareObserver.orientation;
								aue.timestampOfLastScreenOn = HardwareObserver.timestampOfLastScreenOn;

								//appHistory.add(aue);
								logAppHistory(aue); //Stopping App Logging in New Veriosn : Swadhin
								//Utils.d( "added the following app to history: " + aue.packageName);

							}
						}

					} else {

						/** here the user starts his first app */
						AppUsageEvent aue = new AppUsageEvent();
						aue.taskID = app.id;
						aue.runtime = 0; // since we just started
						aue.packageName = app.baseActivity.getPackageName();
						aue.eventtype = AppUsageEvent.EVENT_APPUSE;
						aue.starttime = System.currentTimeMillis();
						aue.longitude = LocationObserver.longitude;
						aue.latitude = LocationObserver.latitude;
						aue.accuracy = LocationObserver.accuracy;
						aue.powerlevel = HardwareObserver.powerlevel;
						aue.powerstate = HardwareObserver.powerstate;
						aue.wifistate = HardwareObserver.wifistate;
						aue.bluetoothstate = HardwareObserver.bluetoothstate;
						aue.headphones = HardwareObserver.headphones;
						aue.orientation = HardwareObserver.orientation;
						aue.timestampOfLastScreenOn = HardwareObserver.timestampOfLastScreenOn;

						//appHistory.add(aue);
						logAppHistory(aue); //Stopping App Logging in New Veriosn : Swadhin
						//Utils.d("added the first app to history: " + aue.packageName);

						// inform listeners about change
						//String currentPackageName = app.baseActivity.getPackageName();
						//fireAppChange(null, currentPackageName);

					}


				}
			}
			else 
			{
				//Lollipop and higher fix
				long cTime = System.currentTimeMillis();
				//TODO: Get settings for first time use
				/*Intent intent = new Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS);
				startActivity(intent);*/

				UsageStatsManager manager = (UsageStatsManager)context.getSystemService(USAGE_STATS_SERVICE);
				UsageEvents events=manager.queryEvents(lastCheckTime, cTime);

				while(events.hasNextEvent())
				{
					UsageEvents.Event event = new UsageEvents.Event();
					events.getNextEvent(event);
					int eventType = event.getEventType();
					String app = event.getPackageName();
					
					switch(eventType)
					{
					case UsageEvents.Event.MOVE_TO_BACKGROUND:
					{
						lastCheckTime = event.getTimeStamp()+1;

						long startTime = getAppStartTime(app);
						startTime = (startTime==-1?lastCheckTime:startTime);

						/** Received app completion event */
						AppUsageEvent aue = new AppUsageEvent();
						aue.taskID = LOLLIPOP_TASKID; //Lollipop does not give us task id
						aue.runtime = lastCheckTime-startTime; // since we just started
						aue.packageName = app;
						aue.eventtype = AppUsageEvent.EVENT_APPUSE;
						aue.starttime = startTime;
						aue.longitude = LocationObserver.longitude;
						aue.latitude = LocationObserver.latitude;
						aue.accuracy = LocationObserver.accuracy;
						aue.powerlevel = HardwareObserver.powerlevel;
						aue.powerstate = HardwareObserver.powerstate;
						aue.wifistate = HardwareObserver.wifistate;
						aue.bluetoothstate = HardwareObserver.bluetoothstate;
						aue.headphones = HardwareObserver.headphones;
						aue.orientation = HardwareObserver.orientation;
						aue.timestampOfLastScreenOn = HardwareObserver.timestampOfLastScreenOn;

						logAppHistory(aue);
						clearAppStartTime(app);
						break;
					}
					case UsageEvents.Event.MOVE_TO_FOREGROUND:
					{
						lastCheckTime = event.getTimeStamp()+1;

						setAppStartTime(app, lastCheckTime);
						break;
					}
					case UsageEvents.Event.CONFIGURATION_CHANGE:
					case UsageEvents.Event.NONE:
						break;
					}
					//Log.d("swadhin"," LAST CHECK TIME:"+lastCheckTime);
				}
				
				SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
				final Resources rcs = context.getResources();

				SharedPreferences.Editor editor = prefs.edit();
				editor.putLong(rcs.getString(R.string.pref_appsensor_lastchecktime), lastCheckTime);
				editor.apply();

			}
		}
	}



	/**
	 * Get last app usage interaction
	 * @return last app usage event
	 */
	private AppUsageEvent getLastInteraction() {
		int s = appHistory.size();
		if (s > 0) {
			return appHistory.get(s-1); 
		} else {
			return null;
		}
	}

	/**
	 * Makes the history of app interaction, which is normally written to the
	 * list in memory, persistent to the local database. Usually, this is done
	 * when the device goes into standby mode.
	 */
	private void makeAppHistoryPersistent() {

		Utils.d( "making app history persistent from memory");
		synchronized (appHistory) {
			//Need to save last app interaction as it gets lost.
			if(appHistory.size()>0){
				AppUsageEvent aue = appHistory.get(appHistory.size()-1);
				aue.runtime = System.currentTimeMillis()-aue.starttime;
				AppSensorService.logEvent(aue); //Stopping App Logging in New Version : Swadhin
			}

			// we do not need to persist those app usages with zero runtime,
			// since they result from errors (e.g. logging when users klicks in
			// standby mode

			/*appHistory.addAll(generalInteractionHistory);
			Collections.sort(appHistory, new Comparator<AppUsageEvent>(){

				@Override
				public int compare(AppUsageEvent arg0, AppUsageEvent arg1) {
					if(arg0.starttime<arg1.starttime)
						return -1;
					return 1;
				}});*/
			generalInteractionHistory.clear();
			/*for(int i=0;i<appHistory.size();i++){
				AppUsageEvent aue = appHistory.get(i);
				if(aue.eventtype.equals(AppUsageEvent.EVENT_APPUSE) && aue.runtime==0)
					continue;
				AppSensorService.logEvent(aue);
			}*/
			appHistory.clear();
		}

	}

	private void logAppHistory(AppUsageEvent aue) {
		appHistory.add(aue);
		Calendar gc = GregorianCalendar.getInstance();
		int day = gc.get(Calendar.DAY_OF_YEAR);
		if(day!=AppSensorService.DAY_RESET){
			AppSensorService.DAY_RESET = day;
			//Force check app sensor service for log period expiry
			if(context!=null)
			{
				AppSensorService.startByIntent(context);
				//Remove old data from database
				removeDailyData();
				removeWeeklyData();
			}

		}
		//Since the currently observed app has not finished execution
		//we will log the last used application.
		AppUsageEvent li = (appHistory.size()>1?appHistory.get(appHistory.size() - 2):null);
		if(li!=null && li.runtime!=0){
			AppSensorService.logEvent(li);
		}
	}

	private void removeDailyData() {
		Calendar gc = GregorianCalendar.getInstance();
		int day = gc.get(Calendar.DAY_OF_YEAR);
		HashSet<Integer> validDays = new HashSet<Integer>();
		HashSet<Integer> invalidDays = new HashSet<Integer>();
		validDays.add(day);
		int DAYS_IN_WEEK = 7;
		for(int i=0;i< DAYS_IN_WEEK;i++)
		{
			gc.add(Calendar.DAY_OF_YEAR,-1);
			validDays.add(gc.get(Calendar.DAY_OF_YEAR));
		}
		try {
			AppUsageObject all[] = null;
			synchronized (GeneralDAO.semaphore) {
				AppUsageDAO dao = new AppUsageDAO(AppUsageDAO.TABLE_TYPE_DAILY, context);
				dao.openRead();
				all = dao.getAllAppUsageObjects();
				dao.close();
			}
			if(all!=null)
			{
				for(int i=0;i<all.length;i++)
				{
					int period = all[i].DAY_OR_WEEK;
					if(!validDays.contains(period))
						invalidDays.add(period);
				}
				Iterator<Integer> it = invalidDays.iterator();
				synchronized (GeneralDAO.semaphore) {
					AppUsageDAO dao = new AppUsageDAO(AppUsageDAO.TABLE_TYPE_DAILY, context);
					dao.openWrite();
					while(it.hasNext())
					{
						int period = it.next();
						dao.delete(period);
					}
					dao.close();
				}
			}

		} catch(Exception e) {
			Utils.d("Error removing old daily data:"+e.getMessage());
		}
	}

	private void removeWeeklyData() {
		Calendar gc = GregorianCalendar.getInstance();
		int week = gc.get(Calendar.WEEK_OF_YEAR);
		gc.add(Calendar.DAY_OF_YEAR, -7);
		int lastWeek = gc.get(Calendar.WEEK_OF_YEAR);

		HashSet<Integer> validWeek = new HashSet<Integer>();
		HashSet<Integer> invalidWeek = new HashSet<Integer>();
		validWeek.add(week);
		validWeek.add(lastWeek);
		try {
			AppUsageObject all[] = null;
			LinkedList<Integer> allWeeks = null;
			synchronized (GeneralDAO.semaphore) {
				AppUsageDAO dao = new AppUsageDAO(AppUsageDAO.TABLE_TYPE_WEEKLY, context);
				dao.openRead();
				all = dao.getAllAppUsageObjects();
				dao.close();
			}
			synchronized (GeneralDAO.semaphore) {
				NotificationDAO dao = new NotificationDAO(context);
				dao.openRead();
				allWeeks = dao.getAllWeeks();
				dao.close();
			}
			if(all!=null)
			{
				for(int i=0;i<all.length;i++)
				{
					int period = all[i].DAY_OR_WEEK;
					if(!validWeek.contains(period))
						invalidWeek.add(period);
				}
				Iterator<Integer> it = invalidWeek.iterator();
				synchronized (GeneralDAO.semaphore) {
					AppUsageDAO dao = new AppUsageDAO(AppUsageDAO.TABLE_TYPE_WEEKLY, context);
					dao.openWrite();
					while(it.hasNext())
					{
						int period = it.next();
						dao.delete(period);
					}
					dao.close();
				}
				invalidWeek.clear();
			}
			if(allWeeks!=null)
			{
				for(int i=0;i<allWeeks.size();i++)
				{
					int period = allWeeks.get(i);
					if(!validWeek.contains(period))
						invalidWeek.add(period);
				}
				Iterator<Integer> it = invalidWeek.iterator();
				synchronized (GeneralDAO.semaphore) {
					NotificationDAO dao = new NotificationDAO(context);
					dao.openWrite();
					while(it.hasNext())
					{
						int period = it.next();
						dao.deleteWeek(period);
					}
					dao.close();
				}
			}

		} catch(Exception e) {
			Utils.d("Error removing old weekly data:"+e.getMessage());
		}
	}



	public void logCustom(AppUsageEvent aue) {
		synchronized (generalInteractionHistory) {
			generalInteractionHistory.add(aue);
		}
		AppSensorService.logEvent(aue);
	}

	public void logAppInstalled(String packageName) {
		logCustom(new AppUsageEvent(packageName, System.currentTimeMillis(), 0,
				AppUsageEvent.EVENT_INSTALLED));
	}

	public void logAppRemoved(String packageName) {

		logCustom(new AppUsageEvent(packageName, System.currentTimeMillis(), 0,
				AppUsageEvent.EVENT_UNINSTALLED));

	}

	public void logAppUpdated(String packageName) {

		logCustom(new AppUsageEvent(packageName, System.currentTimeMillis(), 0,
				AppUsageEvent.EVENT_UPDATE));

	}

	public void logDeviceScreenOn() {

		logCustom(new AppUsageEvent(SPECIALLOGGINGPACKAGE, System.currentTimeMillis(), 0, AppUsageEvent.EVENT_SCREENON));
	}	

	public void logDeviceScreenOff() {

		logCustom(new AppUsageEvent(SPECIALLOGGINGPACKAGE, System.currentTimeMillis(), 0, AppUsageEvent.EVENT_SCREENOFF));
	}	

	public void logDeviceBooted() {

		HardwareObserver.timestampOfLastScreenOn = System.currentTimeMillis(); // obviously user has turned on screen
		logCustom(new AppUsageEvent(SPECIALLOGGINGPACKAGE, System.currentTimeMillis(), 0, AppUsageEvent.EVENT_BOOT));
	}

	public void logDeviceTurnedOff() {

		logCustom(new AppUsageEvent(SPECIALLOGGINGPACKAGE, System.currentTimeMillis(), 0, AppUsageEvent.EVENT_POWEROFF));

		// better make everything persistent when turning off ;)
		makeAppHistoryPersistent();
	}

	public void logWifiStateChanged(String wifiState) {
		if(wifiState!=null)
			logCustom(new AppUsageEvent(SPECIALLOGGINGPACKAGE, System.currentTimeMillis(), 0, wifiState));
	}

	public void logBluetoothStateChanged(String btState) {
		if(btState!=null)
			logCustom(new AppUsageEvent(SPECIALLOGGINGPACKAGE, System.currentTimeMillis(), 0, btState));
	}

	public void logRingerStateChanged(String ringerState) {
		if(ringerState!=null)
			logCustom(new AppUsageEvent(SPECIALLOGGINGPACKAGE, System.currentTimeMillis(), 0, ringerState));
	}

	public void logHeadphonesStateChanged(String headphoneState) {
		if(headphoneState!=null)
			logCustom(new AppUsageEvent(SPECIALLOGGINGPACKAGE, System.currentTimeMillis(), 0, headphoneState));
	}

	public void logLocationChanged() {
		logCustom(new AppUsageEvent(SPECIALLOGGINGPACKAGE, System.currentTimeMillis(), 0, AppUsageEvent.EVENT_LOCATION));
	}

	public void logBroadcastEvent(long time, String event) {
		if(time==-1) time = System.currentTimeMillis();
		logCustom(new AppUsageEvent(SPECIALLOGGINGPACKAGE, time, 0, event));
	}



	private synchronized void waitUntilReady() {
		//Utils.d("waitUntilReady: waiting...");

		if (mHandler == null) {

			Callback messageCallback = new Callback() {

				public boolean handleMessage(Message msg) {

					keyManager = (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);
					boolean rm = keyManager.inKeyguardRestrictedInputMode();
					boolean ph = isUserOnPhone();

					switch (msg.what) {

					case DO_OBSERVE:

						// check whether keys are locked or user is on phone
						rm = keyManager.inKeyguardRestrictedInputMode();
						ph = isUserOnPhone();

						final AppUsageEvent lasi = getLastInteraction();

						if (
								( // device is in restricted mode and user currently not in a phone call
										rm && !ph
										)
										||
										( // screen is locked and users is on phone
												(HardwareObserver.screenState == HardwareObserver.SCREEN_OFF)
												&& !PHONEPACKAGENAME.equals(lasi.packageName))
								) {
							// screen still to be locked
							mHandler.sendEmptyMessageDelayed(DO_START, TIME_APPCHECKINTERVAL);
						} else {
							observeCurrentApp(false);
							mHandler.sendEmptyMessageDelayed(DO_OBSERVE, TIME_APPCHECKINTERVAL);
						}
						break;

					case DO_START: // Utils.d(AppUsageLogger.this,
						// "case DO_START");

						rm = keyManager.inKeyguardRestrictedInputMode();
						ph = isUserOnPhone();

						if (rm && !ph) {
							// screen still seems to be locked and there is no
							// incoming call
							//Utils.d(
							//	"device is in restricted mode");
							mHandler.sendEmptyMessageDelayed(DO_START, TIME_APPCHECKINTERVAL);
						} else {
							// screen was unlocked by user
							//Utils.d( "device is not in restricted mode anymore");
							mHandler.sendEmptyMessageDelayed(DO_OBSERVE, TIME_IMMEDIATELY);
						}
						break;

					case DO_PAUSE: // Utils.d(AppUsageLogger.this,
						// "case DO_PAUSE");
						observeCurrentApp(true);
						mHandler.removeMessages(DO_START);
						mHandler.removeMessages(DO_OBSERVE);
						// tidy up everything and make history persistent
						makeAppHistoryPersistent();
						break;
					}

					return false;
				}
			};

			mHandler = new Handler(getLooper(), messageCallback);
		}

		//Utils.d("waitUntilReady: got it!");
	}




	public synchronized void pauseLogging() {
		waitUntilReady();
		// send message to the worker thread if initialized

		mHandler.sendEmptyMessageDelayed(DO_PAUSE, TIME_IMMEDIATELY);
	}

	public synchronized void startLogging() {
		waitUntilReady();

		// maybe timestamp is zero, e.g. if phone comes in after device was booted
		if (HardwareObserver.timestampOfLastScreenOn == 0) {
			HardwareObserver.timestampOfLastScreenOn = System.currentTimeMillis();
		}

		mHandler.sendEmptyMessageDelayed(DO_START, TIME_IMMEDIATELY);
	}



	private boolean isUserOnPhone() {
		AudioManager manager = (AudioManager)context.getSystemService(Context.AUDIO_SERVICE);
		if(manager.getMode()==AudioManager.MODE_IN_CALL){
			return true;
		}
		else{
			return false;
		}
		/*List<RunningTaskInfo> current = activityManager.getRunningTasks(1);
		if (current.size() > 0) {
			RunningTaskInfo rti = current.get(0);
			boolean userIsOnThePhone = rti.baseActivity.getPackageName().equals(PHONEPACKAGENAME);
			return userIsOnThePhone;
		}
		return false;*/

	}
	/**
	 * Call this method if device screen turns off to check whether user is
	 * currently on the phone or not (some phones turn the screen off when the
	 * user is coming close to the display to prevent unintended button
	 * pressing). If phone app is running, we will continue to log, otherwise we
	 * will pause the logging.
	 */
	@SuppressWarnings("deprecation")
	public void checkStandByOnSceenOff() {
		boolean userIsOnThePhone = false;

		List<RunningTaskInfo> current = activityManager.getRunningTasks(1);
		if (current.size() > 0) {
			RunningTaskInfo rti = current.get(0);
			userIsOnThePhone = rti.baseActivity.getPackageName().equals(PHONEPACKAGENAME);
		}

		// only pause if user is not on the phone, otherwise track phone usage
		if (!userIsOnThePhone) {
			pauseLogging();
		}
	}



	/**
	 * returns true if there is a launcher intent for the app, i.e. this app is shown in the app drawer
	 * @param packageName
	 * @param context
	 * @return false if app is not shown in launcher
	 */
	public static boolean appIsShownInLaucher(String packageName, Context context) {
		Intent intent = context.getPackageManager().getLaunchIntentForPackage(packageName);
		return (intent != null);
	}


	private static long getAppStartTime(String app)
	{
		Long st = startTimeMap.get(app);
		if(st==null)
			return -1;
		return st;
	}

	private static void setAppStartTime(String app, long st)
	{
		startTimeMap.put(app, st);
	}

	private static void clearAppStartTime(String app)
	{
		startTimeMap.remove(app);
	}





}
