package org.swadhin.sensors.appsensor;


/**
 * <p>
 * This is a value object for the information required to observe app
 * interaction such as usage and installations. Additionally it also keeps track
 * of the context, that this event took place in (mainly local time, location).
 * </p>
 * 
 * <p>Apart from app interactions, this object can record various events:</p>
 * <ul>
 * <li>Notification arrived</li>
 * <li>Notification cleared</li>
 * <li>Application used</li>
 * <li>Phone booted</li>
 * <li>Screen on/off</li>
 * <li>WiFi off/on/connected</li>
 * <li>Bluetooth off/on/connected</li>
 * <li>Headphones connected/disconnected</li>
 * </ul>
 * 
 * @author Matthias Boehmer, matthias.boehmer@dfki.de
 */
public class AppUsageEvent {

	// ---------------------------------------------
	// constants
	// ---------------------------------------------

	public static final String EVENT_UNDEFINED = "event_undefined";
	public static final String EVENT_APPUSE = "event_appuse";
	public static final String EVENT_INSTALLED = "event_installed";
	public static final String EVENT_UNINSTALLED = "event_uninstalled";
	public static final String EVENT_UPDATE = "event_update";
	
	public static final String EVENT_NOTIFICATION_ON = "event_notification";
	public static final String EVENT_NOTIFICATION_OFF = "event_notification_clear";
	
	public static final String EVENT_BOOT = "event_boot";
	public static final String EVENT_POWEROFF = "event_poweroff";
	
	public static final String EVENT_SCREENON = "event_screenon";
	public static final String EVENT_SCREENOFF = "event_screenoff";
	
	public static final String EVENT_WIFIOFF = "event_wifioff";
	public static final String EVENT_WIFION = "event_wifion";
	public static final String EVENT_WIFICONNECTED = "event_wificonnected";
	
	public static final String EVENT_BTOFF = "event_btoff";
	public static final String EVENT_BTON = "event_bton";
	public static final String EVENT_BTCONNECTED = "event_btconnected";
	
	public static final String EVENT_HEADPHONESON = "event_headphoneson";
	public static final String EVENT_HEADPHONESOFF = "event_headphonesoff";
	
	public static final String EVENT_RINGNORMAL = "event_ringnormal";
	public static final String EVENT_RINGSILENT = "event_ringsilent";
	public static final String EVENT_RINGVIBRATE = "event_ringvibrate";
	
	public static final String EVENT_LOCATION = "event_location";


	
	// ---------------------------------------------
	// public properties
	// ---------------------------------------------

	/** the app */
	public String packageName;

	// information on the interaction itself
	public String eventtype = EVENT_UNDEFINED;
	public long starttime = System.currentTimeMillis();
	
	/**
	 * This is the timespan in milliseconds that the app was used. 
	 */
	public long runtime;
	
	public int taskID;
	
	/**
	 * This is the UTC timestamp of the last screen on event, i.e. the start of
	 * the session where the current app interaction is embedded in. It might be
	 * zero, e.g. if the background logger starts within a session (e.g. when
	 * AppSensor is just being installed).
	 */
	public long timestampOfLastScreenOn = HardwareObserver.timestampOfLastScreenOn;

	/* location information */
	public double longitude = LocationObserver.longitude;
	public double latitude = LocationObserver.latitude;
	public double accuracy = LocationObserver.accuracy;
	
	/** battery level */
	public short powerstate = HardwareObserver.powerstate;
	public short powerlevel = HardwareObserver.powerlevel;

	/** whether wifi is on, off or connected */
	public short wifistate = HardwareObserver.wifistate;

	/** whether bluetooth is on, off or connected */
	public short bluetoothstate = HardwareObserver.bluetoothstate;

	/** the most often measured orientation of the device during this event (portrait or landscape) */
	public short orientation = HardwareObserver.orientation;

	/** state of the headphones of the device, i.e. connected or not */
	public short headphones = HardwareObserver.headphones;

    public int notif_word_count = 0;
    public String notif_tag = "None";
    public String notif_tickerText = "None";
    public String notif_id = "None";
    public String notif_ledRGB = "None";
    public String notif_ledOff = "None";
    public String notif_ledOn = "None";
    public String notif_sound  = "None";
    public String notif_vibrate = "None";
    public String notif_priority = "None";
    public String notif_icon = "None";
    public String notif_clearable = "None";
    public String notif_ongoing = "None";
    public String notif_style = "None";

    public String feedback = "None";
	
	

	// ---------------------------------------------
	// constructors
	// ---------------------------------------------

	public AppUsageEvent() {
	}

	public AppUsageEvent(int taskID, String packagename, long starttime, int runtime, String eventtype){
		this.taskID = taskID;
		this.packageName = packagename;
		this.starttime = starttime;
		this.runtime = runtime;
		this.eventtype = eventtype;
	}

	public AppUsageEvent(String packagename, long starttime, int runtime, String eventtype) {
		this.taskID = 0;
		this.packageName = packagename;
		this.starttime = starttime;
		this.runtime = runtime;
		this.eventtype = eventtype;
	}

	// ---------------------------------------------
	// technical methods
	// ---------------------------------------------

	@Override
	public String toString() {
		return 
				starttime+","+eventtype+","+packageName+","+runtime+","+bluetoothstate+","
				+wifistate+","+powerstate+","+powerlevel+","+String.valueOf(headphones)+","+latitude+","
				+longitude + "," + notif_word_count + "," + notif_vibrate + "," + notif_icon + ","
                + notif_id + "," + notif_clearable + "," + notif_ongoing + "," + notif_ledRGB + ","
                + notif_ledOn + "," + notif_ledOff + "," + notif_priority + ","
                + notif_sound + "," + notif_style + "," + notif_tag + "," + notif_tickerText + "," + feedback;
	}
	
	
	public short getOrientationSignum() {
		if (orientation < 0 ) return -1;
		if (orientation > 0 ) return +1;
		return 0;
	}

}