package org.swadhin.sensors.appsensor.db;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.LinkedList;

import org.swadhin.app.db.GeneralDAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

/**
 * Data access object for app usage events
 * 
 * <p/>This class provides access methods to the `App Usage' database that
 * tracks app usage for the week and for each day.
 * 
 * @author Abhinav Parate
 */
public class NotificationDAO extends GeneralDAO {

	// --------------------------------------------
	// SCHEMA
	// --------------------------------------------

	public static final String TABLE_NAME = "notifications";
	
	
	public static final String CNAME_APP = "app";
	public static final String CNAME_COUNT = "count";
	public static final String CNAME_DAY = "day";
	public static final String CNAME_WEEK = "week";
	
	public static final String[] PROJECTION = {
		CNAME_APP,
		CNAME_COUNT,
		CNAME_DAY,
		CNAME_WEEK
	};

	public final static int CNUM_APP = 0;
	public final static int CNUM_COUNT = 1;
	public final static int CNUM_DAY = 2;
	public final static int CNUM_WEEK = 13;


	public final static String TABLE_CREATE = "CREATE TABLE " + TABLE_NAME + " (" +
			CNAME_APP + " TEXT, " +
			CNAME_COUNT+" INTEGER, " +
			CNAME_DAY+" INTEGER, " +
			CNAME_WEEK+" INTEGER " +
			");";

	// --------------------------------------------
	// QUERIES
	// --------------------------------------------

	private final static String WHERE_APP_DAY = CNAME_APP + "=? AND " + CNAME_DAY + "=?";
	//private final static String WHERE_APP_WEEK = CNAME_APP + "=? AND " + CNAME_WEEK + "=?";
	private final static String WHERE_DAY =  CNAME_DAY + "=?";
	private final static String WHERE_WEEK = CNAME_WEEK + "=?";
	
	
	
	// --------------------------------------------
	// LIVECYCLE
	// --------------------------------------------

	public NotificationDAO(Context context) {
		super(context);
	}

	// --------------------------------------------
	// CRUD
	// --------------------------------------------

	
	public Cursor find(String app, int day, int week)
	{
		Cursor c = db.query(
				TABLE_NAME, 
				PROJECTION, 
				WHERE_APP_DAY, 
				new String[]{app,day+""}, 
				null, 
				null, 
				null);
		return c;
	}
	
	public NotificationObject[] getNotificationObjectsForLastWeek(String app)
	{
		Calendar gc = GregorianCalendar.getInstance();
		int DAYS_IN_WEEK = 7;
		gc.add(Calendar.DAY_OF_YEAR, -1*(DAYS_IN_WEEK));
		NotificationObject out[] = new NotificationObject[DAYS_IN_WEEK];
		for(int i=0; i<DAYS_IN_WEEK; i++ )
		{
			gc.add(Calendar.DAY_OF_YEAR, 1);
			int DAY = gc.get(Calendar.DAY_OF_YEAR);
			int WEEK = gc.get(Calendar.WEEK_OF_YEAR);
			NotificationObject no = cursor2notificationobject(find(app,DAY,WEEK));
			if(no==null) { 
				no = new NotificationObject();
				no.app = app;
				no.count = 0;
			}
			out[i] = no;
		}
		return out;
	}
	
	public NotificationObject[]  getDayNotifications(int DAY)
	{
		Cursor c = db.query(
				TABLE_NAME, 
				PROJECTION, 
				WHERE_DAY, 
				new String[]{DAY+""}, 
				null, 
				null, 
				CNAME_COUNT+" DESC");
		return cursor2notificationobjects(c);
	}
	
	public NotificationObject[]  getWeekNotifications(int WEEK)
	{
		Cursor c = db.query(
				TABLE_NAME, 
				new String[]{CNAME_APP,"SUM("+CNAME_COUNT+") AS "+CNAME_COUNT}, 
				WHERE_WEEK, 
				new String[]{WEEK+""}, 
				CNAME_APP, 
				null, 
				CNAME_COUNT+" DESC");
		return cursor2notificationobjects(c);
	}
	
	public LinkedList<Integer>  getAllWeeks()
	{
		LinkedList<Integer> list = new LinkedList<Integer>();
		Cursor c = db.query(
				TABLE_NAME, 
				new String[]{CNAME_WEEK}, 
				null, 
				null, 
				null, 
				null, 
				null);
		c.moveToFirst();
		while(!c.isAfterLast()){
			list.add(c.getInt(CNUM_WEEK));
			c.moveToNext();
		}
		
		return list;
	}
	
	
	private ContentValues getContentValues(String app, int day, int week, int count)
	{
		ContentValues cv = new ContentValues();
		cv.put(CNAME_APP, app);
		cv.put(CNAME_COUNT, count);
		cv.put(CNAME_DAY, day);
		cv.put(CNAME_WEEK, week);
		return cv;
	}
	
	private void insert(String app, int day, int week) {
		ContentValues cv = getContentValues(app, day, week, 1);
		db.insert(TABLE_NAME, null, cv);
	}

	private void update(String app, int day, int week, int count) {
		ContentValues values = getContentValues(app, day, week, count);
		db.update(TABLE_NAME, values , WHERE_APP_DAY, new String[]{app,day+""});
	}

	public void deleteDay(int day) {
		//Utils.d("delete file " + r.id);
		db.delete(TABLE_NAME, WHERE_DAY, new String[]{day+""});
	}
	
	public void deleteWeek(int week) {
		//Utils.d("delete file " + r.id);
		db.delete(TABLE_NAME, WHERE_WEEK, new String[]{week+""});
	}
	
	public void updateStats(String app, int day, int week)
	{
		Cursor c = find(app,day,week);
		c.moveToFirst();
		if(c.isAfterLast())
		{
			insert(app, day, week);
		} else {
			int count = c.getInt(CNUM_COUNT);
			update(app, day, week, count+1);
		}
	}
	
	
	// --------------------------------------------
	// TRANSFORMATION
	// --------------------------------------------


	public static NotificationObject cursor2notificationobject(Cursor c)
	{
		c.moveToFirst();
		while(!c.isAfterLast()){
			NotificationObject r = new NotificationObject();
			r.app = c.getString(CNUM_APP);
			r.count = c.getInt(CNUM_COUNT);
			return r;
		}
		return null;
	}
	
	public static NotificationObject[] cursor2notificationobjects(Cursor c)
	{
		c.moveToFirst();
		LinkedList<NotificationObject> notifications = new LinkedList<NotificationObject>();
		while(!c.isAfterLast()){
			NotificationObject r = new NotificationObject();
			r.app = c.getString(CNUM_APP);
			r.count = c.getInt(CNUM_COUNT);
			notifications.add(r);
			c.moveToNext();
		}
		return notifications.toArray(new NotificationObject[0]);
	}

}
