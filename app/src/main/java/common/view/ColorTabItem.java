package common.view;

import android.support.v4.app.Fragment;


public abstract class ColorTabItem{

	private CharSequence mTitle;


	private int mIndicatorColor;
	private int mDividerColor;

	/**
	 * @return the title which represents this tab. In this sample this is used directly by
	 * {@link android.support.v4.view.PagerAdapter#getPageTitle(int)}
	 */
	CharSequence getTitle() {
		return mTitle;
	}

	/**
	 * @return the color to be used for indicator on the {@link SlidingTabLayout}
	 */
	int getIndicatorColor() {
		return mIndicatorColor;
	}

	/**
	 * @return the color to be used for right divider on the {@link SlidingTabLayout}
	 */
	int getDividerColor() {
		return mDividerColor;
	}

	public void setIndicatorColor(int c)
	{
		this.mIndicatorColor = c;
	}

	public void setTitle(CharSequence mTitle) {
		this.mTitle = mTitle;
	}

	public void setDividerColor(int mDividerColor) {
		this.mDividerColor = mDividerColor;
	}
	
	public abstract Fragment createFragment();

}
