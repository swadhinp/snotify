package common.view;

public abstract class FragmentProvider {

	public abstract int getNumFragments();
	
	public abstract String getTitle(int position);
	
	public abstract ColorTabItem getColorTabItem(int position);
	
}
