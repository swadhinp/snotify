package common.view;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.swadhin.app.R;

import java.util.ArrayList;
import java.util.List;

public class SlidingTabFragment extends Fragment {

	private FragmentProvider fragmentProvider = null;

    public SlidingTabFragment(){
    }
    @SuppressLint("ValidFragment")
	public SlidingTabFragment(FragmentProvider fragmentProvider)
	{
        super();
		this.fragmentProvider = fragmentProvider;
	}
	 
    /**
     * A custom {@link ViewPager} title strip which looks much like Tabs present in Android v4.0 and
     * above, but is designed to give continuous feedback to the user when scrolling.
     */
    private SlidingTabLayout mSlidingTabLayout;
 
    /**
     * A {@link ViewPager} which will be used in conjunction with the {@link SlidingTabLayout} above.
     */
    private ViewPager mViewPager;
 
    /**
     * List of {@link ColorTabItem} which represent this sample's tabs.
     */
    private List<ColorTabItem> mTabs = new ArrayList<ColorTabItem>();
    
    private int colors[] = {Color.BLUE, Color.RED, Color.YELLOW, Color.GREEN};
 
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        /**
         * Populate our tab list with tabs. Each item contains a title, indicator color and divider
         * color, which are used by {@link SlidingTabLayout}.
         */
        for(int i=0;i<fragmentProvider.getNumFragments() && i<colors.length;i++)
        {
        	ColorTabItem item = fragmentProvider.getColorTabItem(i);
        	item.setIndicatorColor(colors[i]);
        	mTabs.add(item);
        }
       
    }
 
    /**
     * Inflates the {@link View} which will be displayed by this {@link Fragment}, from the app's
     * resources.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_color_tabs, container, false);
    }
 
    /**
     * This is called after the {@link #onCreateView(LayoutInflater, ViewGroup, Bundle)} has finished.
     * Here we can pick out the {@link View}s we need to configure from the content view.
     *
     * We set the {@link ViewPager}'s adapter to be an instance of
     * {@link ColorTabFragmentPagerAdapter}. The {@link SlidingTabLayout} is then given the
     * {@link ViewPager} so that it can populate itself.
     *
     * @param view View created in {@link #onCreateView(LayoutInflater, ViewGroup, Bundle)}
     */
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // Get the ViewPager and set it's PagerAdapter so that it can display items
        mViewPager = (ViewPager) view.findViewById(R.id.viewpager);
        mViewPager.setAdapter(new ColorTabFragmentPagerAdapter(getChildFragmentManager()));
 
        // Give the SlidingTabLayout the ViewPager, this must be done AFTER the ViewPager has had
        // it's PagerAdapter set.
        mSlidingTabLayout = (SlidingTabLayout) view.findViewById(R.id.sliding_tabs);
        mSlidingTabLayout.setViewPager(mViewPager);
 
        // Set a TabColorizer to customize the indicator and divider colors. Here we just retrieve
        // the tab at the position, and return it's set color
        mSlidingTabLayout.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
 
            @Override
            public int getIndicatorColor(int position) {
                return mTabs.get(position).getIndicatorColor();
            }
 
            @Override
            public int getDividerColor(int position) {
                return mTabs.get(position).getDividerColor();
            }
 
        });
    }
 
    /**
     * The {@link FragmentPagerAdapter} used to display tabs in this fragment. The individual fragment
     * instances are provided by {@link FragmentProvider}
     * as {@link ColorTabItem} for the requested position.
     * <p>
     */
    class ColorTabFragmentPagerAdapter extends FragmentPagerAdapter {
 
        ColorTabFragmentPagerAdapter(FragmentManager fm) {
            super(fm);
        }
 
        /**
         * Return the {@link android.support.v4.app.Fragment} to be displayed at {@code position}.
         * <p>
         * Here we return the value returned from {@link ColorTabItem#createFragment()}.
         */
        @Override
        public Fragment getItem(int i) {
            return mTabs.get(i).createFragment();
        }
 
        @Override
        public int getCount() {
            return mTabs.size();
        }
 
        /**
         * Return the title of the item at {@code position}. This is important as what this method
         * returns is what is displayed in the {@link SlidingTabLayout}.
         * <p>
         * Here we return the value returned from {@link ColorTabItem#getTitle()}.
         */
        @Override
        public CharSequence getPageTitle(int position) {
            return fragmentProvider.getTitle(position);
        }
 
    }
}
