-- Merging decision tree log ---
manifest
ADDED from AndroidManifest.xml:2:1
	package
		ADDED from AndroidManifest.xml:3:5
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
	android:versionName
		ADDED from AndroidManifest.xml:5:5
	android:versionCode
		ADDED from AndroidManifest.xml:4:5
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
	xmlns:android
		ADDED from AndroidManifest.xml:2:11
uses-sdk
ADDED from AndroidManifest.xml:8:5
MERGED from com.android.support:appcompat-v7:21.0.3:15:5
MERGED from com.android.support:support-v4:21.0.3:15:5
MERGED from com.google.android.gms:play-services:6.5.87:18:5
MERGED from com.android.support:support-v4:21.0.3:15:5
	android:targetSdkVersion
		ADDED from AndroidManifest.xml:10:9
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
	android:minSdkVersion
		ADDED from AndroidManifest.xml:9:9
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
uses-permission#android.permission.INTERNET
ADDED from AndroidManifest.xml:22:5
	android:name
		ADDED from AndroidManifest.xml:22:22
uses-permission#android.permission.ACCESS_NETWORK_STATE
ADDED from AndroidManifest.xml:23:5
	android:name
		ADDED from AndroidManifest.xml:23:22
uses-permission#android.permission.WRITE_EXTERNAL_STORAGE
ADDED from AndroidManifest.xml:24:5
	android:name
		ADDED from AndroidManifest.xml:24:22
uses-permission#android.permission.BLUETOOTH
ADDED from AndroidManifest.xml:25:5
	android:name
		ADDED from AndroidManifest.xml:25:22
uses-permission#android.permission.BLUETOOTH_ADMIN
ADDED from AndroidManifest.xml:26:5
	android:name
		ADDED from AndroidManifest.xml:26:22
uses-permission#android.permission.ACCESS_WIFI_STATE
ADDED from AndroidManifest.xml:28:5
	android:name
		ADDED from AndroidManifest.xml:28:22
uses-permission#android.permission.RECEIVE_BOOT_COMPLETED
ADDED from AndroidManifest.xml:29:5
	android:name
		ADDED from AndroidManifest.xml:29:22
uses-permission#android.permission.PACKAGE_USAGE_STATS
ADDED from AndroidManifest.xml:30:5
	android:name
		ADDED from AndroidManifest.xml:30:22
uses-permission#android.permission.GET_ACCOUNTS
ADDED from AndroidManifest.xml:33:5
	android:name
		ADDED from AndroidManifest.xml:33:22
uses-permission#android.permission.USE_CREDENTIALS
ADDED from AndroidManifest.xml:34:5
	android:name
		ADDED from AndroidManifest.xml:34:22
uses-permission#android.permission.GET_TASKS
ADDED from AndroidManifest.xml:37:5
	android:name
		ADDED from AndroidManifest.xml:37:22
uses-permission#android.permission.ACCESS_COARSE_LOCATION
ADDED from AndroidManifest.xml:38:5
	android:name
		ADDED from AndroidManifest.xml:38:22
uses-permission#android.permission.ACCESS_FINE_LOCATION
ADDED from AndroidManifest.xml:39:5
	android:name
		ADDED from AndroidManifest.xml:39:22
uses-permission#android.permission.READ_PHONE_STATE
ADDED from AndroidManifest.xml:40:5
	android:name
		ADDED from AndroidManifest.xml:40:22
application
ADDED from AndroidManifest.xml:45:5
MERGED from com.android.support:appcompat-v7:21.0.3:16:5
MERGED from com.android.support:support-v4:21.0.3:16:5
MERGED from com.google.android.gms:play-services:6.5.87:20:5
MERGED from com.android.support:support-v4:21.0.3:16:5
	android:label
		ADDED from AndroidManifest.xml:48:9
	android:allowBackup
		ADDED from AndroidManifest.xml:46:9
	android:icon
		ADDED from AndroidManifest.xml:47:9
	android:theme
		ADDED from AndroidManifest.xml:49:9
activity#org.swadhin.app.SplashScreen
ADDED from AndroidManifest.xml:53:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:56:13
	android:label
		ADDED from AndroidManifest.xml:55:13
	android:theme
		ADDED from AndroidManifest.xml:57:13
	android:name
		ADDED from AndroidManifest.xml:54:13
intent-filter#android.intent.action.MAIN+android.intent.category.LAUNCHER
ADDED from AndroidManifest.xml:58:13
action#android.intent.action.MAIN
ADDED from AndroidManifest.xml:59:17
	android:name
		ADDED from AndroidManifest.xml:59:25
category#android.intent.category.LAUNCHER
ADDED from AndroidManifest.xml:60:17
	android:name
		ADDED from AndroidManifest.xml:60:27
activity#org.swadhin.app.HomeActivity
ADDED from AndroidManifest.xml:63:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:66:13
	android:label
		ADDED from AndroidManifest.xml:67:13
	android:launchMode
		ADDED from AndroidManifest.xml:65:13
	android:name
		ADDED from AndroidManifest.xml:64:13
activity#org.swadhin.app.IntroActivity
ADDED from AndroidManifest.xml:69:9
	android:label
		ADDED from AndroidManifest.xml:72:13
	android:theme
		ADDED from AndroidManifest.xml:71:13
	android:name
		ADDED from AndroidManifest.xml:70:13
activity#org.swadhin.app.IntroNotificationActivity
ADDED from AndroidManifest.xml:74:9
	android:label
		ADDED from AndroidManifest.xml:77:13
	android:theme
		ADDED from AndroidManifest.xml:76:13
	android:name
		ADDED from AndroidManifest.xml:75:13
activity#org.swadhin.app.IntroResearchActivity
ADDED from AndroidManifest.xml:79:9
	android:label
		ADDED from AndroidManifest.xml:82:13
	android:theme
		ADDED from AndroidManifest.xml:81:13
	android:name
		ADDED from AndroidManifest.xml:80:13
activity#org.swadhin.app.SettingsActivity
ADDED from AndroidManifest.xml:84:10
	android:label
		ADDED from AndroidManifest.xml:87:13
	android:theme
		ADDED from AndroidManifest.xml:86:13
	android:name
		ADDED from AndroidManifest.xml:85:13
activity#org.swadhin.app.WeeklyStatsActivity
ADDED from AndroidManifest.xml:89:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:92:13
	android:label
		ADDED from AndroidManifest.xml:93:13
	android:theme
		ADDED from AndroidManifest.xml:91:13
	android:name
		ADDED from AndroidManifest.xml:90:13
activity#org.swadhin.app.fragments.InboxDetailFragmentActivity
ADDED from AndroidManifest.xml:95:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:98:13
	android:label
		ADDED from AndroidManifest.xml:99:13
	android:theme
		ADDED from AndroidManifest.xml:97:13
	android:name
		ADDED from AndroidManifest.xml:96:13
service#org.swadhin.app.service.UtilityService
ADDED from AndroidManifest.xml:104:9
	android:name
		ADDED from AndroidManifest.xml:104:18
receiver#org.swadhin.app.service.UtilityService$UtilityService_BGReceiver
ADDED from AndroidManifest.xml:106:9
	android:name
		ADDED from AndroidManifest.xml:106:19
intent-filter#android.intent.action.BOOT_COMPLETED
ADDED from AndroidManifest.xml:107:13
action#android.intent.action.BOOT_COMPLETED
ADDED from AndroidManifest.xml:108:17
	android:name
		ADDED from AndroidManifest.xml:108:25
receiver#org.swadhin.app.service.UtilityService$UtilityService_WifiReceiver
ADDED from AndroidManifest.xml:112:9
	android:name
		ADDED from AndroidManifest.xml:112:19
intent-filter#android.net.conn.CONNECTIVITY_CHANGE
ADDED from AndroidManifest.xml:113:13
action#android.net.conn.CONNECTIVITY_CHANGE
ADDED from AndroidManifest.xml:114:17
	android:name
		ADDED from AndroidManifest.xml:114:25
service#org.swadhin.sensors.appsensor.AppSensorService
ADDED from AndroidManifest.xml:121:9
	android:enabled
		ADDED from AndroidManifest.xml:124:13
	android:label
		ADDED from AndroidManifest.xml:122:13
	android:exported
		ADDED from AndroidManifest.xml:125:13
	android:name
		ADDED from AndroidManifest.xml:123:13
service#org.swadhin.sensors.appsensor.NotificationObserver
ADDED from AndroidManifest.xml:127:9
	android:permission
		ADDED from AndroidManifest.xml:129:13
	android:name
		ADDED from AndroidManifest.xml:128:13
intent-filter#android.service.notification.NotificationListenerService
ADDED from AndroidManifest.xml:130:13
action#android.service.notification.NotificationListenerService
ADDED from AndroidManifest.xml:131:17
	android:name
		ADDED from AndroidManifest.xml:131:25
meta-data#com.google.android.gms.version
ADDED from AndroidManifest.xml:136:9
MERGED from com.google.android.gms:play-services:6.5.87:21:9
	android:value
		ADDED from AndroidManifest.xml:138:13
	android:name
		ADDED from AndroidManifest.xml:137:13
